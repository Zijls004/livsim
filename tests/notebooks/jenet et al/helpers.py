import matplotlib.pyplot as plt

def merge_parameters(default,shared,specific):
    # opt. copy
    res = default
    
    for d in [shared,specific]:
        for k,v in d.items():
            res[k] = v        
        
    # days per month for conversion
    DPM = 30
    
    pmp = res['peak_milk_production']['value'] # kg/day
    ld = res['lactation_duration']['value'] # months
    pd = res['peak_milk_production_duration']['value'] # months
    
    # month after calving (month), rate (kg/month)
    res['milk_yield_potential']['value'] = [[0, pmp*DPM],
                                            [pd, pmp*DPM],
                                            [ld, 0],
                                           ]
    return res

def estimate_experimental_feeding_error(parameters,feed_mix,bw=None):
    import numpy as np
    
    if bw is None:
        bw = parameters['cycle_1']['body_weight']['value'][0][1]
    
    ME_demand = (8.3 + 0.091*bw/1.08) #ME/day
    feeding_level = parameters['feeding_level']['value']
    ME_target = ME_demand*feeding_level
    print('Target ME/day: {:}'.format(ME_target))

    amount = parameters['cycle_1']['feed_intake']['value']
    print('Amount Fed kg/day: {:}'.format(amount))
    ME_content = feed_mix.ME
    ME_supplied = amount*ME_content
    print('Feed ME/kg DM: {:}'.format(ME_content))
    print('Supplied ME/day: {:}'.format(ME_supplied))

    error = ME_supplied/ME_target
    return error


def plt_demand_supply(axes,df):

    cs0 = ['body_weight','maximum_body_weight','minimum_body_weight']
    cs1 = ['ME_demand_maintenance','ME_demand_gestation','ME_demand_lactation','ME_demand_growth',]
    cs1_ = ['ME_demand_total','ME_supply_total']

    df['body_weight'].plot(ax=axes[0],color='black')
    df['maximum_body_weight'].plot(ax=axes[0],color='black',linestyle='--')
    df['minimum_body_weight'].plot(ax=axes[0],color='black',linestyle='--')

    df[cs1_].plot(ax=axes[1])
    df[cs1].plot.area(ax=axes[1])

    cs2 = ['MP_demand_maintenance','MP_demand_gestation','MP_demand_lactation','MP_demand_growth',]
    cs2_ = ['MP_demand_total','MP_supply_total']

    df[cs2_].plot(ax=axes[2])
    df[cs2].plot.area(ax=axes[2])

    axes[0].set_ylabel('Body Weight (kg)')
    axes[1].set_ylabel('ME (MJ/month)')
    axes[2].set_ylabel('MP (g/month)')

    for ax in axes:
        ax.set_xlabel('Age (month)')
        ax.grid()

    plt.tight_layout()

def plt_bw(ax,df,parameters,title=None):

    mask1 =  (df['age'] > 30) & (df['age'] < 90)

    def myplt(ax,mask2,ls='solid', fill = False, label=None):
        ys = df.loc[mask2,'body_weight'].values.astype(float)
        xs = df.loc[mask2,'age'].values.astype(float)
        ax.plot(xs,ys,ls=ls,label=label)

        if fill:
            eps = 0.02
            ax.fill_between(xs,(1-eps)*ys,(1+eps)*ys,alpha=0.5)

    myplt(ax, df['lactation_cycle_number'] == 1, label='cycle 1')
    myplt(ax, df['lactation_cycle_number'] == 2, label='cycle 2')
    myplt(ax, mask1, ls='dotted', fill=False, label='')

    xs = df.loc[mask1,'age'].values.astype(float)
    ys = df.loc[mask1,'body_weight'].values.astype(float)
    eps = 0.02
    ax.fill_between(xs,(1-eps)*ys,(1+eps)*ys,color='black',alpha=0.1,label='2% err')

    s = (7/30)

    fmts = ['o','^']
    for i in [1,2]:
        week_bws = parameters['cycle_{:}'.format(i)]['body_weight']['value']

        x0 = df.loc[df['lactation_cycle_number'] == i].index.values[0]
        xs = [s*x[0]+x0 for x in week_bws]
        ys = [x[1] for x in week_bws]
        fmt = fmts[i-1]
        ax.errorbar(xs,ys,
                    yerr=10,
                    color='black',label='observed cycle {:}'.format(i),
                    fmt=fmts[i-1],
                    capsize=CAPSIZE)

    ax.legend()
    ax.grid(True)

    if title is not None:
        ax.set_title(title)

    ax.set_xlabel('Age (months)')
    ax.set_ylabel('Body Weight (kg)')

CAPSIZE = 3

def plt_intake(ax,df,parameters,legend=False):
    for i in [1,2]:
        ym = df.loc[df['lactation_cycle_number'] == i,'feed_intake'].mean()
        yo = parameters['cycle_{:}'.format(i)]['feed_intake']['value']
        yerr = df.loc[df['lactation_cycle_number'] == i,'feed_intake'].std()

        ax.errorbar([i],yo,yerr=0.15,color='red',label='observed',fmt='^',capsize=CAPSIZE)
        ax.errorbar([i],ym,yerr=yerr,color='black',label='simulated',fmt='o',capsize=CAPSIZE)

    ax.set_xlim(0.8,2.2)
    ax.set_xticks([1,2])
    ax.set_xlabel('cycle')
    ax.set_ylabel('feed intake (kg/day)')
    if legend:
        ax.legend(loc='lower left')

def plt_milk(axes,df,parameters,title=None,legend=True):

    varias = ['milk_yield','milk_fat_yield','milk_protein_yield']

    for ax,var in zip(axes,varias):

        for i in [1,2]:
            s = df.loc[df['lactation_cycle_number'] == i,var]
            ym = s.sum()

            obs = parameters['cycle_{:}'.format(i)][var]

            yo = obs['value']

            if i == 2:
            	label = 'simulated'
            else:
            	label = None
            ax.errorbar([i],ym,yerr=0.1*ym,color='black',label=label,fmt='o',capsize=CAPSIZE)

            if 'err' in obs:
                yerr = obs['err']
            else:
                yerr = 0

            if i == 2:
            	label = 'observed'
            else:
            	label = None
            ax.errorbar([i],yo,yerr=yerr,color='red',label=label,fmt='^',capsize=CAPSIZE)

        ax.set_xlim(0.8,2.2)

        unit = parameters['cycle_{:}'.format(i)][var]['unit']

        ax.set_xticks([1,2])
        ax.set_xlabel('cycle')
        ax.set_ylabel('{:} ({:})'.format(var,unit))

    if legend:
        ax.legend(loc='lower left')

    plt.tight_layout()

def plt_overview(df,parameters,figsize=(8,6)):

    fig = plt.figure(1,figsize=figsize)
    
    # set up subplot grid

    import matplotlib.gridspec as gridspec
    gridspec.GridSpec(2,4)

    # large subplot
    plt.subplot2grid((2,4), (0,0), colspan=4, rowspan=1)
    ax = plt.gca()

    plt_bw(ax,df,parameters)

    axes = []

    for loc in [(1,0),(1,1),(1,2),(1,3)]:

        plt.subplot2grid((2,4), loc)
        ax = plt.gca()
        axes.append(ax)

    plt_intake(axes[0],df,parameters)
    plt_milk(axes[1:],df,parameters)

    # fit subplots and save fig
    fig.tight_layout()