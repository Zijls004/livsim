import sys
import os
import logging

from file_paths import nuances_folder
from file_paths import current_folder

sys.path.append(nuances_folder)

from livsim import FeedStorage
from livsim import Cow

# Create logger
logging_file = os.path.join(current_folder, 'model_run.log')
logging.basicConfig(
    level=logging.DEBUG, 
    format='%(asctime)s %(name)-48s %(levelname)--8s %(message)s',
    datefmt='%d-%m %H:%M',
    filename=logging_file,
    filemode='w')

cow = Cow(breed='Boran x Friesian')

cow._MAX_ITER = 20

# The FeedStorage-class describes an object that handles feeding management
# for the animals in the simulation
feed_storage = FeedStorage()

# Feed sources from off-farm: here an amount of grass to feed to the animals is specified for each month of the year.
external_grass = [10, 10, 7, 7, 5, 5, 3, 5, 5, 7, 7, 10]
feed_storage.add_external_roughage(external_grass, 'Heteropogon contortus', 'default')

# Some additional concentrates to keep our cows from dying on the diet of only grass...
#feed_storage.add_external_concentrate(0, 'concentrate 1', 'default')
feed_storage.add_external_concentrate(1, 'concentrate 1', 'calf')
feed_storage.add_external_concentrate(2, 'concentrate 1', 'gestating')
feed_storage.add_external_concentrate(2, 'concentrate 1', 'lactating')


# Finally the feed storage should be associated with our cow-object to make 
# interactions between them possible.
cow.feed_supply = feed_storage

res, df = cow.run(120)
df.to_csv('test_run.csv')