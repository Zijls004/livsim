# README

Welcome to LIVSIM, an individual-based livestock production model written in Python. The basis
for LIVSIM is the livestock model by Konandreas and Anderson (1982). The main differences be-
tween this model and LIVSIM are that in LIVSIM the nutritive requirements of the animals are
calculated using the AFRC-system (AFRC, 1993), a feed intake function is included that limits
voluntary roughage intake (Conrad et al. 1966), excreta production is included, and herd level
management rules are implemented. For a description of the LIVSIM model see Rufino (2008).


# Getting Started

To get started with LIVSIM simply download or clone this repository. The examples-folder contains
example application of the model both in the form of Jupyter-notebooks and Python-scripts. The
Jupyter-notebooks are an ideal way of exploring the model and its outcomes in an interactive way.
The scripts provide a means to run the model without the need for external dependencies and make
it very easy to debug the code.


# Install Guide

LIVSIM is written in Python 3 and depends on a number of third-party libraries. The easiest way
to meet all requirements is to install the Anaconda distribution of Python. This will also provide
the user with Jupyter.

See https://anaconda.com/download/


# Contributing

Feel free to email the maintainer of this model if you wish to contribute to this project.

See the contact section below.


# License

This project is licensed under a copy-left license, see http://models.pps.wur.nl/content/licence_agreement


# Contact

The maintainer of this project is Mink Zijlstra (mink.zijlstra@wur.nl).


# References

AFRC (1993). Energy and Protein Requirements of Ruminants. An advisory manual prepared by the AFRC Technical 
Committee on Responses to Nutrients. CAB International, Wallingford, UK.

Conrad HR, Pratt AD, Hibbs JW, (1964). Regulation of feed intake in dairy cows. I. Change in importance
of physical and physiological factors with increasing digestibility. Journal of Dairy Science, Volume
47(1): 54-62.

Konandreas P, Anderson FM, (1982). Cattle herd dynamics: an integer and stochastic model for evaluating
production alternatives. ILCA Research Report No. 2.

Rufino MC (2008). Quantifying the contribution of crop-livestock integration to African farming. PhD Thesis,
Wageningen University, The Netherlands.

