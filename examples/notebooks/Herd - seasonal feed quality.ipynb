{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **Introduction**\n",
    "In this notebook we demonstrate how to simulate a herd of cows that are fed a diet that varies over the season."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys\n",
    "sys.path.append('../../..')\n",
    "\n",
    "from matplotlib import pyplot as plt\n",
    "\n",
    "from livsim import Herd\n",
    "from livsim import Cow, Bull\n",
    "from livsim import Feed, FeedStorage"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<br>\n",
    "<div style='width: 720px'>\n",
    "    <h4><b>Feed management</b></h4>\n",
    "    <p>\n",
    "        Management of feeding strategies is handled by the FeedStorage-object. There are two ways in which feed \n",
    "        can be handled by the FeedStorage-object:\n",
    "        <ol>\n",
    "            <li>\n",
    "                Through the `add_external_feed()`-method: feed that is added through this method will be fed each \n",
    "                month in the specified amount to each animal in the specified life stage. This method makes no\n",
    "                assumptions about the progeny of the feed and just assumes that the feed will be sourced from somewhere.\n",
    "                For more information on this method use: `?feed_storage.add_external_feed`\n",
    "            </li>\n",
    "            <li>\n",
    "                Through the `add_crop_residues()`-method: the amount of feed added using this method will be rationed\n",
    "                over the number of months specified in the method-call and distributed over the herd. For more information\n",
    "                on this method use: `?feed_storage.add_crop_residues`. Currently this method is intended to be used only\n",
    "                if LIVSIM is used as part of FARMSIM.\n",
    "            </li>\n",
    "        </ol>\n",
    "    </p>\n",
    "    <p>\n",
    "        The FeedStorage-object is only concerned with the ***amounts*** of feed that the animal receives. The feed quality\n",
    "        parameters are defined in `feed_library.yaml`. In the feed library quality parameters can be set to a single value,\n",
    "        to use the same quality parameter for all months in the year, or as a list of 12 values to specify the feed quality\n",
    "        of that feed for each month of the year.\n",
    "    </p>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# let us make our feed storage model-element\n",
    "feed_storage = FeedStorage()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<br>\n",
    "<div style='width: 720px'>    \n",
    "    <h4><b>Feed quality parameters</b></h4>\n",
    "    <p>\n",
    "        To show how to create a feed type with quality parameters that vary over the season, \n",
    "        the feed library contains parameters for a grass species <i>Heteropogon contortus</i>. The\n",
    "        feed quality parameters are from Rufino (2008). <i>Quantifying the contribution of crop-\n",
    "        livestock integration to African farming.</i>\n",
    "    </p>\n",
    "    <p>\n",
    "        Let us create a Feed-object using the quality parameters for <i>H. contortus</i> and inspect some\n",
    "        of its characteristics.\n",
    "    </p>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "grass = Feed(parameters='Heteropogon contortus', category='roughage')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "\n",
    "grass = Feed(parameters='Heteropogon contortus', category='roughage')\n",
    "df = pd.DataFrame({k: v['value'] for k, v in grass.parameters.items() if k != 'metadata'})\n",
    "\n",
    "time = [i for i in range(1, 13)]\n",
    "fig, axes = plt.subplots(1, 3, figsize=(20, 4))\n",
    "\n",
    "axes[0].plot(time, df['CP'], label='CP')\n",
    "axes[0].set_xlabel('time (months)')\n",
    "axes[0].set_ylabel('CP content (g/kg)')\n",
    "\n",
    "axes[1].plot(time, df['DMD'], label='DMD')\n",
    "axes[1].set_xlabel('time (months)')\n",
    "axes[1].set_ylabel('dry matter digestibility (g/kg)')\n",
    "\n",
    "axes[2].plot(time, df['FME'], label='FME')\n",
    "axes[2].set_xlabel('time (months)')\n",
    "axes[2].set_ylabel('Fermentable ME (MJ/kg)')\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **Add forage to the feed storage**\n",
    "\n",
    "Now that we have defined a feed with quality parameters that depend on the season, let's add some to the feed supply of the cattle."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Feed sources from off-farm: here an amount of grass to feed to the animals is specified for each month of the year.\n",
    "external_grass = [10, 10, 7, 7, 5, 5, 3, 5, 5, 7, 7, 10]\n",
    "feed_storage.add_external_roughage(external_grass, 'Heteropogon contortus', 'default')\n",
    "feed_storage.add_external_roughage(0, 'maize stover', 'default')\n",
    "\n",
    "# Some additional concentrates to keep our cows from dying on the diet of only grass...\n",
    "feed_storage.add_external_concentrate(0.5, 'concentrate 1', 'default')\n",
    "feed_storage.add_external_concentrate(1, 'concentrate 1', 'gestating')\n",
    "feed_storage.add_external_concentrate(1, 'concentrate 1', 'lactating')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Start with two cows; aged 48 months and 6 months.\n",
    "cow_1 = Cow(age=48, breed='Friesian x Holstein')\n",
    "cow_2 = Cow(age=6, breed='Friesian x Holstein')\n",
    "cow_1._main_variables = None\n",
    "cow_2._main_variables = None"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The two cows we specified above form our initial herd.\n",
    "initial_cattle = [cow_1, cow_2]\n",
    "herd = Herd(animals=initial_cattle)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **Herd management**\n",
    "\n",
    "At the moment there are a number of options available to manage the herd:\n",
    "1. Restrain the total allowed size of the herd; this maximum herd size is the sum of the maximum number of cows and the maximum number of bulls in the herd.\n",
    "2. Specify a maximum duration of the period a cow can remain open before she is replaced.\n",
    "3. Specify a maximum age for a cow before she is replaced.\n",
    "4. Specify a maximum number of lactations for a cow before she is replaced.\n",
    "\n",
    "In the current herd management replacement is always with an animal born on-farm. So if no calf is born on farm an animal may be kept way past its specified sell-by date."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set the maximum number of cows in our herd\n",
    "herd.parameters['max_nfemales']['value'] = 2\n",
    "herd.parameters['max_nmales']['value'] = 0\n",
    "herd.parameters['female_threshold_open']['value'] = 36       \n",
    "herd.parameters['female_threshold_age']['value'] = 96        \n",
    "herd.parameters['female_threshold_lactations']['value'] = 5"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Now let us link the feed supply to the herd; this established a two-way connection between the herd and the feed supply.\n",
    "herd.feed_supply = feed_storage"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **Running the simulation**\n",
    "\n",
    "After the herd and feed supply have been set up the simulation can be run and the output inspected."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "res, df = herd.run(months=240)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure()\n",
    "ax = fig.gca()\n",
    "\n",
    "for cow_nr in res['animal_ids']:\n",
    "\n",
    "    var_name = f'Cow_{cow_nr}_body_weight'\n",
    "    bw_data = df[var_name]\n",
    "    \n",
    "    ax.plot(bw_data, label=f'cow id: {cow_nr}')\n",
    "\n",
    "ax.legend()\n",
    "ax.set_xlabel('time (months)')\n",
    "ax.set_ylabel('body weight (kg)')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure()\n",
    "ax = fig.gca()\n",
    "\n",
    "for cow_nr in res['animal_ids']:\n",
    "\n",
    "    var_name = f'Cow_{cow_nr}_milk_yield'\n",
    "    \n",
    "    ax.plot(df[var_name], label=f'cow id: {cow_nr}')\n",
    "\n",
    "ax.legend()\n",
    "ax.set_xlabel('time (months)')\n",
    "ax.set_ylabel('milk yield (kg)')"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.16"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
