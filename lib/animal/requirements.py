import copy
from math import exp

import yaml

from ...shared_components import FarmSimBaseClass

DAYS_PER_MONTH = 30


class RequirementsCattleAFRC(FarmSimBaseClass):
    ''' This class calculates the protein and energy requirements of cattle
    according to the system of AFRC [1].

    Values for the scaling factor for the fasting
    metabolism (C1):
    ---------------------
    Sex     Bulls   Other
    ---------------------
    C1      1.15    1.00

    Values for the scaling factor for the energy
    value for liveweight gain (C2):
    -----------------------------------------------
    Maturity type   Bulls       Castrates   Heifers
    -----------------------------------------------
    Early           1.00        1.15        1.30
    Medium          0.85        1.00        1.15
    Late            0.70        0.85        1.00

    Values for the scaling factor for the net
    protein content of liveweight gain (C6):
    -----------------------------------------------
    Maturity type   Bulls       Castrates   Heifers
    -----------------------------------------------
    Early           1.00        0.90        0.80
    Medium          1.10        1.00        0.90
    Late            1.20        1.10        1.00


    References
    ----------
    [1] AFRC (1993) Energy and Protein Requirements of Ruminants

    '''
    _name = 'ReqAFRC'
    _attrs_to_keep = []

    def __init__(self, animal):

        super().__init__()

        self._animal = animal

        self._body_weight_ = None
        self._body_weight_gain_ = None

        self._Q_m_ = 1

        self.reset()

    @property
    def _prefix(self):
        if self._animal._herd is not None:
            return f'{self._name}_{self._animal.id}'
        else:
            return f'{self._name}'

    # region - Interface with animal-object

    @property
    def _animal(self):
        return self._animal_

    @_animal.setter
    def _animal(self, value):
        self._animal_ = value

    def reset(self):
        self._requirement_ME_activity = None
        self._requirement_ME_fasting = None
        self._requirement_ME_growth = None

        self._requirement_MP_maintenance = None
        self._requirement_MP_growth = None

    # region - coefficients

    @property
    def afrc_C1(self):
        ''' '''
        if self._animal is not None:
            return self._animal.parameters['AFRC']['C1']['value']
        else:
            return self.parameters['AFRC']['C1']['value']

    @property
    def afrc_C2(self):
        ''' Co-determines the energy value of weight gain, a coefficient (1).

        See [1] page 27, eq. 61.
        '''
        if self._animal is not None:
            return self._animal.parameters['AFRC']['C2']['value']
        else:
            return self.parameters['AFRC']['C2']['value']

    @property
    def afrc_C3(self):
        ''' Co-determines the energy value of weight gain, a coefficient (1).

            See [1] page 27, eq. 61.
        '''
        if self._animal is not None:
            if self._animal.level_of_feeding <= 1:
                return 0
            else:
                return 1
        else:
            return 1

    @property
    def afrc_C6(self):
        if self._animal is not None:
            return self._animal.parameters['AFRC']['C6']['value']
        else:
            return self.parameters['AFRC']['C6']['value']

    # endregion - coefficients
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - activities

    @property
    def _energy_cost_standing(self):
        if self._animal is not None:
            return self._animal.energy_cost_standing
        else:
            return self.parameters['energy_cost_standing']['value']

    @property
    def _energy_cost_body_position_change(self):
        if self._animal is not None:
            return self._animal.energy_cost_body_position_change
        else:
            return self.parameters['energy_cost_body_position_change']['value']

    @property
    def _energy_cost_horizontal_movement(self):
        if self._animal is not None:
            return self._animal.energy_cost_horizontal_movement
        else:
            return self.parameters['energy_cost_horizontal_movement']['value']

    @property
    def _energy_cost_vertical_movement(self):
        if self._animal is not None:
            return self._animal.energy_cost_vertical_movement
        else:
            return self.parameters['energy_cost_vertical_movement']['value']

    @property
    def _time_spent_standing(self):
        if self._animal is not None:
            return self._animal.time_spent_standing
        else:
            return self.parameters['time_spent_standing']['value']

    @property
    def _body_changes_per_day(self):
        if self._animal is not None:
            return self._animal.body_changes_per_day
        else:
            return self.parameters['body_changes_per_day']['value']

    @property
    def _horizontal_movement_per_day(self):
        if self._animal is not None:
            return self._animal.horizontal_movement_per_day
        else:
            return self.parameters['horizontal_movement_per_day']['value']

    @property
    def _vertical_movement_per_day(self):
        if self._animal is not None:
            return self._animal.vertical_movement_per_day
        else:
            return self.parameters['vertical_movement_per_day']['value']

    # endregion - activities
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @property
    def parameters(self):
        raise AttributeError('Parameters should be provided by sub-class.')

    @property
    def _body_weight(self):
        ''' The value returned by this property is the estimated body weight of
        the animal. This is the body weight the animal is expected to have half
        way the current month. This value is used to prevent over-, or under-
        estimating the nutritive requirements of the animal.
        '''
        if self._animal is not None:
            return self._animal._body_weight_estimate
        else:
            if self._body_weight_ is None:
                raise ValueError(
                    'Make sure that a value for body weight has been set.')
            return self._body_weight_

    @_body_weight.setter
    def _body_weight(self, value: float):
        if self._animal is None:
            self._body_weight_ = value
        else:
            raise AttributeError(
                "Attribute should not be set when object is associated with an animal.")

    @property
    def body_weight_fasted(self):
        """ The fasted body weight of the animal. This is the body weight of
        the animal before the first feed intake of the day.

        :return: Fasted body weight (kg)
        :rtype: float
        """
        return self._body_weight / 1.08

    @property
    def _body_weight_change(self):
        if self._animal is not None:
            return self._animal.body_weight_change
        else:
            if self._body_weight_gain is None:
                raise ValueError(
                    'Make sure that a value for body weight gain has been set.')
            return self._body_weight_gain

    @_body_weight_change.setter
    def _body_weight_change(self, value: float):
        if self._animal is None:
            self._body_weight_gain = value
        else:
            raise AttributeError(
                "Attribute should not be set when object is associated with an animal.")

    @property
    def _will_lose_weight(self):
        return self._animal.will_lose_weight

    @property
    def _Q_m(self):
        if self._animal is not None:
            return self._animal.Q_m
        else:
            if self._Q_m_ is None:
                m = 'Make sure that a value for Q_m has been set.'
                raise ValueError(m)

            return self._Q_m_

    @_Q_m.setter
    def _Q_m(self, value: float):
        if self._animal is None:
            self._Q_m_ = value
        else:
            m = "Attribute should not be set when object is associated with an animal."
            raise AttributeError(m)

    # endregion - Interface with animal-object
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - efficiencies

    @property
    def efficiency_ME_maintenance(self):
        """ Utilization efficiency of metabolisable energy for maintenance
        respiration. 
        
        This efficiency accounts for the loss of energy as heat during the
        processes of digestion and metabolism. 

        See also [1] page 3. Denoted "k_m".

        :return: Energy utilization efficiency for maintenance (MJ NE / MJ ME)
        :rtype: float
        """
        return 0.35 * self._Q_m + 0.503

    @property
    def efficiency_ME_growth_growing_ruminant(self):
        ''' Efficiency for growth of a "growing" ruminant (MJ ME / MJ E).

        See [1] page 3. Denoted "k_f". '''
        return 0.78 * self._Q_m + 0.006

    @property
    def efficiency_ME_growth(self):
        ''' Efficiency for growth (MJ ME / MJ E).

        See [1] page 3. '''
        raise NotImplementedError(
            "Each subclass should define its own efficiency_ME_growth.")

    @property
    def efficiency_MP_maintenance(self):
        ''' Efficiency for maintenance (g MP / g NP).

        See [1] page 34. Denoted "k_nm". '''
        return 1

    @property
    def efficiency_MP_growth(self):
        ''' Efficiency for growth (g MP / g NP).

        See [1] page 36. Denoted "k_nf". '''
        return 0.59

    # endregion - efficiencies
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - ME requirements

    # region - energy costs
    @property
    def energy_cost_activity(self):
        ''' The energy cost of activity (movement/standing) (kJ / kg / d).

        Follows from the sum of the energy cost of
            1. Standing
            2. Body position changes
            3. Horizontal movement
            4. Vertical movement
        '''

        # all per kg or per kg per m
        ecs = self._energy_cost_standing
        ecbc = self._energy_cost_body_position_change
        echm = self._energy_cost_horizontal_movement
        ecvm = self._energy_cost_vertical_movement

        hs = self._time_spent_standing
        bc = self._body_changes_per_day
        hm = self._horizontal_movement_per_day
        vm = self._vertical_movement_per_day

        # kJ / kg / day
        ec = ecs * hs + ecbc * bc + ecvm * vm + echm * hm

        return ec

    @property
    def energy_cost_weight_gain(self):
        ''' The net energy cost of gaining weight (MJ / kg gain).

        See [1] page 27, eq. 61.
        '''
        W = self._body_weight

        if self._body_weight_change < 0:
            return 0

        dW = self._body_weight_change / DAYS_PER_MONTH

        C2 = self.afrc_C2
        C3 = self.afrc_C3

        num = C2 * (4.1 + 0.0332 * W - 0.000009 * W**2)
        denum = 1 - (C3 * 0.1475 * dW)

        if denum <= 0:
            m = 'Net energy requirement for growth cannot be negative.'
            raise ValueError(m)
        else:
            res = num / denum

        return res

    # endregion - energy costs
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - net energy requirements

    @property
    def requirement_NE_maintenance(self):
        ''' The net energy requirement for maintenance (MJ/month).

        See [1] page 23 eq. 39.
        '''
        return self.requirement_NE_fasting + self.requirement_NE_activity

    @property
    def requirement_NE_fasting(self):
        ''' Calculate the net energy requirement for fasting (MJ/month).

        Is a function of the body weight.

        See [1] page 23 eq. 40.
        '''
        return DAYS_PER_MONTH * self.afrc_C1 * 0.53 * self.body_weight_fasted**0.67

    @property
    def requirement_NE_activity(self):
        ''' Calculate the net energy requirement for activity (MJ/month).

        Is a function of the body weight.

        See [1] page 24.
        '''
        c = self.energy_cost_activity

        kJ_to_MJ = 0.001

        return kJ_to_MJ * c * self._body_weight * DAYS_PER_MONTH

    @property
    def requirement_NE_growth(self):
        ''' Net energy requirement for growth (MJ/month).

        See [1] page 27, eq. 61.
        '''
        if self._body_weight_change < 0:
            return 0

        else:
            dW = self._body_weight_change
            EV_g = self.energy_cost_weight_gain

            return dW * EV_g

    # endregion - net energy requirements
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - metabolisable energy requirements

    @property
    def requirement_ME_total(self):
        ''' The ME requirement for maintenance AND production (MJ/month).

        See [1] page 9.
        '''
        return self.requirement_ME_maintenance + self.requirement_ME_growth

    @property
    def requirement_ME_non_growth(self):
        ''' The ME requirement for maintenance (MJ/month).

            See [1] page 9.
        '''
        return self.requirement_ME_maintenance

    @property
    def requirement_ME_fasting(self):

        if self._requirement_ME_fasting is None:

            f = self.requirement_NE_fasting
            km = self.efficiency_ME_maintenance

            self._requirement_ME_fasting = f / km

        return self._requirement_ME_fasting

    @property
    def requirement_ME_activity(self):
        if self._requirement_ME_activity is None:

            f = self.requirement_NE_activity
            km = self.efficiency_ME_maintenance

            self._requirement_ME_activity = f / km

        return self._requirement_ME_activity

    @property
    def requirement_ME_maintenance(self):
        ''' The ME requirement for maintenance (AFRC, 1993).

        Equals the sum of the E requirement for fasting and activity,
        converted to ME requirement via the ME use efficiency for maintenance.
        '''
        f = self.requirement_ME_fasting
        a = self.requirement_ME_activity
        
        if None in (f, a):
            return None
        else:
            return f + a

    @property
    def requirement_ME_growth(self):
        ''' The ME requirement for the monthly growth (MJ/month).

            The amount of metabolisable energy (ME) required by the animal for
            growth depends on the net energy (NE) requirements for growth and
            the efficiency with which the animal can use energy acquired from
            feed.
        '''
        if self._requirement_ME_growth is None:
            if self._body_weight_change < 0:
                self._requirement_ME_growth = 0
            else:
                self._requirement_ME_growth = self.requirement_NE_growth / self.efficiency_ME_growth

        return self._requirement_ME_growth

    # endregion - metabolisable energy requirements

    # endregion - ME requirements
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region MP requirements

    # region - protein costs
    @property
    def protein_cost_weight_gain(self):
        ''' The net protein cost of weight gain (g NP / kg weight gain).

        See [1] page 36 eq. 93.        
        '''
        if self._body_weight_change < 0:
            return 0

        W = self._body_weight  # kg
        dW = self._body_weight_change / DAYS_PER_MONTH  # g/d

        C6 = self.afrc_C6

        return C6 * (168.07 - 0.16869 * W + 0.0001633 * W**2) * (1.12 - 0.1223 * dW)
    
    # endregion - protein costs
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - net protein requirements
    
    @property
    def requirement_NP_dermal_losses(self):
        ''' The protein (P) requirement for maintaining dermal losses (g/month).

        See [1] page 35 eq. 86. '''
        return DAYS_PER_MONTH * 6.25 * 0.018 * self._body_weight**0.75

    @property
    def requirement_NP_basal_endogenous_nitrogen(self):
        ''' Calculates the protein (P) requirement for basal maintenance (g/month).

        Is a function of the body weight (first argument).

        See [1] page 35 eq. 85. '''
        return DAYS_PER_MONTH * 6.25 * 0.35 * self._body_weight**0.75

    @property
    def requirement_NP_maintenance(self):
        ''' Calculates the protein (P) requirement for overall maintenance.

        Is a function of the body weight and amounts to the sum of the P requirement
        for basal endogenous nitrogen and dermal losses.

        See [1] page 34 eq. 80. '''
        return self.requirement_NP_basal_endogenous_nitrogen + self.requirement_NP_dermal_losses

    @property
    def requirement_NP_growth(self):
        ''' The net protein requirement for growth (g NP / month).'''
        if self._body_weight_change < 0:
            return 0
        else:
            return self.protein_cost_weight_gain * self._body_weight_change
    
    # endregion - net protein requirements
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - metabolisable protein requirements
    
    @property
    def requirement_MP_total(self):
        ''' Total requirement for metabolisable protein (g / month). '''
        return self.requirement_MP_maintenance + self.requirement_MP_growth

    @property
    def requirement_MP_non_growth(self):
        return self.requirement_MP_maintenance

    @property
    def requirement_MP_maintenance(self):
        ''' The MP requirement for maintenance (g/month).

        This involves the P requirement for maintenance (which depends on body weight)
        and the efficiency of MP use for maintenance. See [1] page 34.
        '''
        if self._requirement_MP_maintenance is None:
            self._requirement_MP_maintenance = self.requirement_NP_maintenance / \
                                                self.efficiency_MP_maintenance
        return self._requirement_MP_maintenance
        
    @property
    def requirement_MP_growth(self):
        '''  The MP requirement for growth of the ruminant (g MP / month).

        Is a function of the body weight and potential body weight change.
        See [1] page 36 eq. 92.
        '''
        if self._requirement_MP_growth is None:
            if self._body_weight_change < 0:
                self._requirement_MP_growth = 0
            else:
                self._requirement_MP_growth = self.requirement_NP_growth / self.efficiency_MP_growth

        return self._requirement_MP_growth

    # endregion - metabolisable protein requirements

    # endregion MP requirements


class RequirementsCowAFRC(RequirementsCattleAFRC):

    default_parameters = yaml.load('''
        energy_cost_horizontal_movement:
            value: 0.0026
            unit: 'kJ / kg / m'
            source: '[1] page 24'
            info: 'Energy expenditure on horizontal movement.'
        energy_cost_vertical_movement:
            value: 0.028
            unit: 'kJ / kg / m'
            source: '[1] page 24'
            info: 'Energy expenditure on vertical movement.'
        energy_cost_standing:
            value: 10
            unit: 'kJ / kg / d'
            source: '[1] page 24'
            info: 'Energy expenditure on standing.'
        energy_cost_body_position_change:
            value: 0.260
            unit: 'kJ / kg'
            source: '[1] page 24'
            info: 'Energy expenditure on body changes.'
        body_changes_per_day:
            value: 10
            unit: '-'
            source: 'User defined, default based on [1] page 24--25.'
            info: 'Number of body changes per day.'
        horizontal_movement_per_day:
            value: 1000
            unit: 'meters'
            source: 'User defined, default based on [1] page 24--25.'
            info: 'Distance moved in horizontal direction per day.'
        vertical_movement_per_day:
            value: 100
            unit: 'meters'
            source: 'User defined, default based on [1] page 24--25.'
            info: 'Distance moved in vertical direction per day.'
        AFRC:
            C1:
                value: 1.0
                unit: '-'
                info: 'Key-determinant of/scales the fasting metabolism.'
                source: '[1] page 23'
            C2:
                value: 1.15
                unit: '-'
                info: 'Co-determines the energy value of liveweight gain.'
                source: '[1] page 28 Table 2.1'
            C6:
                value: 1.0
                unit: '-'
                info: 'Co-determines the protein demand for growth'
                source: '[1] page 36'
        ''', Loader=yaml.SafeLoader)

    def __init__(self, animal=None):
        super().__init__(animal)

        if self._animal is None:
            self._parameters = copy.deepcopy(self.default_parameters)
        else:
            self._parameters = None

        self._milk_yield_ = None
        self._months_into_gestation_ = None

        self._is_gestating_ = False
        self._is_lactating_ = False

    def reset(self):
        super().reset()

        self._requirement_ME_gestation = None
        self._requirement_ME_lactation = None
        self._requirement_MP_gestation = None
        self._requirement_MP_lactation = None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - Interface with animal-object

    @property
    def parameters(self):
        return self._parameters

    @parameters.setter
    def parameters(self, pars):
        self._parameters = pars

    @property
    def _is_gestating(self):
        ''' Is the animal in gestation? Before answering that question this
            property checks whether the associated animal-object has the attri-
            bute 'is_gestating'. If it doesn't the animal is a bull and 'False'
            is returned by default.
        '''
        if self._animal is not None:
            if hasattr(self._animal, 'is_gestating'):
                return self._animal.is_gestating
            else:
                return False
        else:
            if self._is_gestating_ is None:
                raise ValueError(
                    'Make sure that a value for is_gestating has been set.')
            return self._is_gestating_

    @_is_gestating.setter
    def _is_gestating(self, value: bool):
        if self._animal is not None:
            raise AttributeError(
                "Attribute should not be set when object is associated with an animal.")
        self._is_gestating_ = value

    @property
    def _is_lactating(self):
        ''' Is the animal in lactation? Before answering that question this
            property checks whether the associated animal-object has the attri-
            bute 'is_lactating'. If it doesn't the animal is a bull and 'False'
            is returned by default.
        '''
        if self._animal is not None:
            if hasattr(self._animal, 'is_lactating'):
                return self._animal.is_lactating
            else:
                return False
        else:
            if self._is_lactating_ is None:
                raise ValueError(
                    'Make sure that a value for is_lactating has been set.')
            return self._is_lactating_

    @_is_lactating.setter
    def _is_lactating(self, value: bool):
        if self._animal is not None:
            raise AttributeError(
                "Attribute should not be set when object is associated with an animal.")
        self._is_lactating_ = value

    @property
    def _months_into_gestation(self):
        if self._animal is not None:
            return self._animal.months_into_gestation
        else:
            return self._months_into_gestation_

    @_months_into_gestation.setter
    def _months_into_gestation(self, value: int):
        self._months_into_gestation_ = value

    @property
    def _calf_birth_weight(self):
        ''' Birth weight of a calf (kg). '''
        if self._animal is not None:
            return self._animal.calf_birth_weight
        else:
            return 40

    @property
    def _milk_crude_protein_content(self):
        ''' The crude protein content of milk produced by the animal
        (g CP / kg FW milk). '''
        if self._animal is not None:
            return self._animal.milk_crude_protein_content
        else:
            return 28

    @property
    def _milk_butterfat_content(self):
        ''' The butter fat content of the milk produced by the animal
        (g fat / kg FW milk). '''
        if self._animal is not None:
            return self._animal.milk_butterfat_content
        else:
            return 45

    @property
    def _milk_yield(self):
        if self._animal is not None:
            return self._animal.milk_yield
        else:
            return self._milk_yield_

    @_milk_yield.setter
    def _milk_yield(self, value: float):
        if self._animal is None:
            self._milk_yield_ = value
        else:
            raise AttributeError(
                "Attribute should not be set when object is associated with an animal.")

    # endregion - Interface with animal-object
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - efficiencies

    @property
    def efficiency_ME_gestation(self):
        ''' Efficiency for growth of the concepta/gestation (MJ E/MJ ME).

        See [1] page 4. Denoted "k_c".
        '''
        return 0.133

    @property
    def efficiency_ME_lactation(self):
        ''' Efficiency for lactation (MJ E/MJ ME).

        See [1] page 3. Denoted "k_l". '''
        return 0.35 * self._Q_m + 0.420

    @property
    def efficiency_ME_growth(self):
        ''' Efficiency for growth (MJ E/MJ ME).

        See [1] page 3. '''
        if self._is_lactating:
            return self.efficiency_ME_growth_lactating_ruminant
        else:
            return self.efficiency_ME_growth_growing_ruminant

    @property
    def efficiency_ME_growth_lactating_ruminant(self):
        ''' Efficiency for growth of a "lactating" ruminant (MJ E/MJ ME).

        See [1] page 3. Denoted "k_g". '''
        return 0.95 * self.efficiency_ME_lactation

    @property
    def efficiency_MP_lactation(self):
        ''' The efficiency of milk protein synthesis (g CP milk/g MP). '''
        return 0.68

    @property
    def efficiency_MP_gestation(self):
        ''' Efficiency for growth of the concepta/gestation (g NP/g MP).

        See [1] page 39. Denoted "k_nc".
        '''
        return 0.85

    # endregion - efficiencies
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - total requirements

    @property
    def requirement_ME_total(self):
        ''' The ME requirement for non-growth (body-weight) related processes
            (MJ / month).
        '''
        return sum([
            self.requirement_ME_maintenance,
            self.requirement_ME_gestation,
            self.requirement_ME_lactation,
            self.requirement_ME_growth
        ])

    @property
    def requirement_ME_non_growth(self):
        ''' The ME requirement for maintenance and growth (MJ/month).

            See [1] page 9.
        '''
        return sum([
            self.requirement_ME_maintenance,
            self.requirement_ME_gestation,
            self.requirement_ME_lactation
        ])

    @property
    def requirement_MP_total(self):
        ''' The MP requirement for non-growth (body-weight) related processes
            (g / month).
        '''
        return sum([
            self.requirement_MP_maintenance,
            self.requirement_MP_gestation,
            self.requirement_MP_lactation,
            self.requirement_MP_growth
        ])

    @property
    def requirement_MP_non_growth(self):
        ''' The MP requirement for non-growth (body-weight) related processes
            (g / month).
        '''
        return sum([
            self.requirement_MP_maintenance,
            self.requirement_MP_gestation,
            self.requirement_MP_lactation
        ])

    # endregion - total requirements
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - gestation requirements

    @property
    def energy_retention_calf(self):
        ''' The total energy (MJ) retained in the calf, assuming a 40kg
        birth weight.

        See [1] page 29, eq. 70. '''
        if not self._is_gestating:
            return 0
        else:
            t = DAYS_PER_MONTH * self._months_into_gestation

            return self.get_energy_retention_calf(t)

    def get_energy_retention_calf(self, t):
        ''' Function to calculate the total energy (MJ) retained in the calf,
        at time t after conception, assuming a 40kg birthweight.

        See [1] page 29, eq. 70. '''
        a = 151.665
        b = 151.64
        c = 0.0000576

        return 10**(a - b * exp(-c * t))

    @property
    def daily_energy_retention_calf(self):
        ''' The daily energy retention (MJ / d) in the growing foetus.

        See [1], page 30, eq. 71. '''
        if not self._is_gestating:
            return 0
        else:
            t = DAYS_PER_MONTH * self._months_into_gestation

            return self.get_daily_energy_retention_calf(t)

    def get_daily_energy_retention_calf(self, t):
        ''' Function to calculate the daily energy retention (MJ / d) in the
            growing foetus.

            Note, this is (we checked) the derivative of Et, the total energy
            retained in the calf, as given by "energy_retention_calf", namely:

            Et = 10**(a - b * exp(-c * t))

            dEt/dt 
            = Et * [log(10) * b * c] * exp(-c * t)
            = Et * [0.0201] * exp(-c * t)            <- evaluated with b,c
            = (Wc / Wg) * Et * 0.0201 * exp(-c * t)  <- gauge at 40 kg
            = 0.025 * Wc * Et * 0.0201 * exp(-c * t) <- final form: eq. 71 [1]

            where (a, b, c) = (151.665, 151.64, -0.0000576).

            Note, the final form takes scaling with birthweight into account by
            gauging the value at a 40 kg birthweight via the factor:
                weight_scaling = calf_birth_weight / 40

            See [1], page 30, eq. 71.
        '''
        weight_scaling = self._calf_birth_weight / 40
        energy_retention = self.get_energy_retention_calf(t)

        return weight_scaling * energy_retention * (0.0201 * exp(-0.0000576 * t))

    @property
    def requirement_NE_gestation(self):
        ''' The energy requirement (MJ / month) for the growth of the calf.

        See [1], page 30, eq. 71.

        Note, this is (we checked) the derivative of Et, the total energy
        retained in the calf, as given by "energy_retention_calf", namely:

            Et = 10**(a - b*exp(-c*t))

            dEt/dt = Et * [log(10) * b * c] * exp(-c * t)
                   = Et * 0.0201 * exp(-c * t)              <- evaluated with b,c
                   = (Wc / Wg) * Et * 0.0201 * exp(-c * t)  <- gauge at 40 kg
                   = 0.025 * Wc * Et * 0.0201 * exp(-c * t) <- final form: eq. 71 [1]

        where (a, b, c) = (151.665, 151.64, -0.0000576).

        Note, the final form takes scaling with birthweight into account by
        gauging the value at a 40 kg birthweight "Wc" via the factor
        0.025() = 1/Wg). '''
        if not self._is_gestating:
            return 0
        else:
            # in days
            t = DAYS_PER_MONTH * self._months_into_gestation

            return sum([self.get_daily_energy_retention_calf(t) for t in range(t, t + DAYS_PER_MONTH)])

    @property
    def requirement_ME_gestation(self):
        ''' The ME demand for gestation (MJ/month).

        See [1] page 6.
        '''
        if self._requirement_ME_gestation is None:

            net_energy = self.requirement_NE_gestation
            k_gestation = self.efficiency_ME_gestation

            self._requirement_ME_gestation = net_energy / k_gestation

        return self._requirement_ME_gestation

    @property
    def net_protein_concepta(self):
        ''' The net protein (NP) retained in the calf after t days of
        gestation, assuming a 40kg birthweight.

        See [1] page 39, eq. 109.
        '''
        if not self._is_gestating:
            return 0
        else:
            t = DAYS_PER_MONTH * self._months_into_gestation

            return self.get_protein_concepta(t)

    def get_protein_concepta(self, t):
        ''' Function to calculate the protein retention in the calf at t days
        after conception.

        This funcion can be used to either calculate the protein retention at a
        given moment in time, or iteratively to calculate the cumulative protein
        retention over a given period.

        See [1] page 39, eq. 109. '''
        weight_scaling = self._calf_birth_weight / 40
        tissue_protein = 10**(3.707 - 5.698 * exp(-0.00262 * t))

        return weight_scaling * tissue_protein * (34.37 * exp(-0.00262 * t))

    @property
    def requirement_NP_gestation(self):
        ''' The net protein demand (g/month) for the growth of the calf.

        See [1], page 39, eq. 110.
        '''
        if not self._is_gestating:
            return 0

        t = DAYS_PER_MONTH * self._months_into_gestation
        res = sum([self.get_protein_concepta(t)
                   for t in range(t, t + DAYS_PER_MONTH)])

        return res

    @property
    def requirement_MP_gestation(self):
        ''' The MP demand of gestation (g/month).
        '''

        if self._requirement_MP_gestation is None:

            net_protein = self.requirement_NP_gestation
            k_gestation = self.efficiency_MP_gestation

            self._requirement_MP_gestation = net_protein / k_gestation

        return self._requirement_MP_gestation

    # endregion - gestation requirements
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - lactation requirements

    @property
    def _milk_energy_content(self):
        ''' The energy content (MJ / kg FW) of the milk.

        See [1] page 26. We use eq. 54.'''
        return self._milk_energy_content_54

    @property
    def _milk_energy_content_53(self):
        ''' The energy content (MJ / kg FW) of the milk.

        See [1] page 26, eq. 55 --- s.e. 0.089.'''
        return 0.0406 * self._milk_butterfat_content + 1.509

    @property
    def _milk_energy_content_54(self):
        ''' The energy content (MJ / kg FW) of the milk.

        See [1] page 26, eq. 54  --- s.e. 0.066.'''
        milk_fat = self._milk_butterfat_content
        milk_protein = self._milk_crude_protein_content

        return 0.0376 * milk_fat + 0.0209 * milk_protein + 0.948

    @property
    def _milk_energy_content_55(self):
        ''' The energy content (MJ / kg FW) of the milk.

        See [1] page 26, eq. 55 --- s.e. 0.089.
        '''
        return 0.0406 * self._milk_butterfat_content + 1.509

    @property
    def requirement_NE_lactation(self):
        ''' The energy demand of milk production (MJ/month). 

        See [1] page 26, eq. 56. '''
        return self._milk_yield * self._milk_energy_content

    @property
    def requirement_ME_lactation(self):
        ''' The ME demand of milk production (MJ/month).

            See [1] page 26, eq. 56. 
        '''
        if self._requirement_ME_lactation is None:

            net_energy = self.requirement_NE_lactation
            k_lactation = self.efficiency_ME_lactation

            self._requirement_ME_lactation = net_energy / k_lactation

        return self._requirement_ME_lactation

    @property
    def requirement_NP_lactation(self):
        ''' The amount of protein required to sustain the current level of milk
            production (g CP / month).
        '''
        return self._milk_yield * self._milk_crude_protein_content

    @property
    def requirement_MP_lactation(self):
        ''' The MP demand of lactation (g/month). '''
        if self._requirement_MP_lactation is None:

            net_protein = self.requirement_NP_lactation
            k_lactation = self.efficiency_MP_lactation

            self._requirement_MP_lactation = net_protein / k_lactation

        return self._requirement_MP_lactation

    # endregion - lactation requirements


class RequirementsBullAFRC(RequirementsCattleAFRC):

    default_parameters = yaml.load('''
        energy_cost_horizontal_movement:
            value: 0.0026
            unit: 'kJ / kg / m'
            source: '[1] page 24'
            info: 'Energy expenditure on horizontal movement.'
        energy_cost_vertical_movement:
            value: 0.028
            unit: 'kJ / kg / m'
            source: '[1] page 24'
            info: 'Energy expenditure on vertical movement.'
        energy_cost_standing:
            value: 10
            unit: 'kJ / kg / d'
            source: '[1] page 24'
            info: 'Energy expenditure on standing.'
        energy_cost_body_position_change:
            value: 0.260
            unit: 'kJ / kg'
            source: '[1] page 24'
            info: 'Energy expenditure on body changes.'
        body_changes_per_day:
            value: 10
            unit: '-'
            source: 'User defined, default based on [1] page 24--25.'
            info: 'Number of body changes per day.'
        horizontal_movement_per_day:
            value: 1000
            unit: 'meters'
            source: 'User defined, default based on [1] page 24--25.'
            info: 'Distance moved in horizontal direction per day.'
        vertical_movement_per_day:
            value: 100
            unit: 'meters'
            source: 'User defined, default based on [1] page 24--25.'
            info: 'Distance moved in vertical direction per day.'
        C1:
            value: 1.15
            unit: '-'
            info: 'Key-determinant of/scales the fasting metabolism.'
            source: '[1] page 23'
        C2:
            value: 1.00
            unit: '-'
            info: 'Co-determines the energy value of liveweight gain.'
            source: '[1] page 28 Table 2.1'
        C6:
            value: 1.00
            unit: '-'
            info: 'Co-determines the protein demand for growth'
            source: '[1] page 36'
        ''', Loader=yaml.SafeLoader)

    def __init__(self, animal=None):
        super().__init__(animal)

        if self._animal is None:
            self._parameters = copy.deepcopy(self.default_parameters)
        else:
            self._parameters = None

    @property
    def parameters(self):
        return self._parameters

    @property
    def efficiency_ME_growth(self):
        ''' Efficiency for growth (MJ E/MJ ME).

        See [1] page 3. '''
        return self.efficiency_ME_growth_growing_ruminant