import os
import sys
import yaml
import random
import logging
from copy import deepcopy
from statistics import mean

import numpy as np
from scipy.interpolate import interp1d
import pandas as pd

from .feed_intake import FeedIntakeCattle
from .requirements import RequirementsCowAFRC, RequirementsBullAFRC
from .growth_production import GrowthProductionCow, GrowthProductionBull

from ..feed import MilkSupply

from ...shared_components import FarmSimBaseClass

logger = logging.getLogger(__name__)

# module's directory path, e.g. used to locate parameter library
path = os.path.abspath(__file__)
dir_path = os.path.dirname(path)
if getattr(sys, 'frozen', False):
    main_dir = os.path.dirname(sys.executable)
    USE_LOCAL_REPOSITORY = False
else:
    lib_dir = os.path.dirname(dir_path)
    main_dir = os.path.dirname(lib_dir)
    USE_LOCAL_REPOSITORY = True

DAYS_PER_MONTH = 30

def plain_yaml_dump(diktionary):
    ''' Function to dump the supplied dictionary of data to a yaml-file.
    
        default_flow_style=False:
        -------------------------
        Makes sure that the nested dictionaries are dumped to file in blocks of
        data, rather than in flow style. 
        
        Dumper=noalias_dumper:
        ----------------------
        Aliases are ignored, meaning that recurring blocks of data are dumped
        as repeated blocks of data, rather than as a reference to the first
        occurrence of that block of data.
    '''
    noalias_dumper = yaml.dumper.SafeDumper
    noalias_dumper.ignore_aliases = lambda self, data: True

    return yaml.dump(diktionary, default_flow_style=False, Dumper=noalias_dumper)


def read_yaml(path):
    ''' Simple function to facilitate reading data from disk.
    '''
    with open(path) as f:
        res = yaml.load(f, Loader=yaml.SafeLoader)

    return res


def interpolate(xys):
    ''' Takes xy pairs xys [[x0,y0],...[xn,yn]] and returns the associated
    piece-wise linear function y(x). '''
    xs = [xy[0] for xy in xys]
    ys = [xy[1] for xy in xys]

    return interp1d(xs, ys, fill_value='extrapolate')


class ProtoCattle(FarmSimBaseClass):
    ''' Class that defines the humble beginnings of the Cow- and Bull-classes.

        This class defines methods and attributes that are not directly related
        to the conceptualization of the cattle in LIVSIM as such, but that are
        needed to interact with input-data, generate output, etc.
        TODO: complement this list
    '''
    _name = 'ProtoCattle'
    _default_preset = 'Friesian x Holstein'
    _counter = 0
    _breed_library = {}
    
    if USE_LOCAL_REPOSITORY:
        _library_path = os.path.join(main_dir, 'data', 'breed_library.yaml')
        _default_library = read_yaml(_library_path)

    # region - class-methods to write breed to file and read breed from file
    
    def to_library(self, key=None, path=None):
        ''' This method allows the object to write its parameters to a breed
            library. 

            The library is stored at the location indicated by the parameter
            'path'. If that breed library already exists the current set of
            parameters will be appended to the existing library and stored
            under the key indicated by the parameter 'key'. If 'key' is al-
            ready present in the library the existing set will be overwritten.

            If no path is provided the default path is used to store the
            library to. If no key is provided the set of parameters can not be
            stored and the method returns the set of parameters as they would
            be written to a library, allowing the user to inspect the result.

            parameters:
            -----------
            key [str] : key under which the set of parameters is stored in the
                        library.
            path [str]: file path to the location where the breed library is,
                        or will be, located. Path should include the name of
                        the file.
        '''
        if path is None:
            path = self._library_path

        data = {'parameters': self.parameters}

        if key is None:
            return data
        else:
            try:
                old_lib = read_yaml(path)
                new_lib = deepcopy(old_lib)
            except FileNotFoundError:
                new_lib = {}

            with open(path, 'w') as file:
                new_lib[key] = data
                file.write(plain_yaml_dump(new_lib))

            return data

    def from_library(self, key, path=None):
        ''' This method allows the object to parameterize itself from a breed-
            library.

            parameters:
            -----------
            key [str] : key under which the set of parameters is stored in the
                        library.
            path [str]: file path to the location where the breed library is
                        located.
        '''
        library = self._breed_library

        if key in library:
            breed_data = library[key]
        else:
            print('Breed preset not available in library...')
            print(f'Using the preset: {self._default_preset}')
            breed_data = library[self._default_preset]

        if isinstance(self, Cow):
            par_key = 'cow'
        elif isinstance(self, Bull):
            par_key = 'bull'
        else:
            raise ValueError("Please make sure that the animal is either a lady or a gentleman.")

        self.parameters = breed_data[par_key]

    # endregion - class-methods to write breed to file and read breed from file
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def __init__(self, herd=None):

        super().__init__()

        self.id = self._counter
        self._herd = herd
    
    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, value):
        self._id = value
        ProtoCattle._counter += 1

    @property
    def _herd(self):
        """ Return a reference to the herd-object to which this animal belongs,
        if applicable, or None. 

        :return: Reference to the encapsulating herd-object.
        :rtype: Herd or None
        """
        return self._herd_

    @_herd.setter
    def _herd(self, value):
        self._herd_ = value

    @property
    def _prefix(self):
        if self._herd is not None:
            return f'{self._name}_{self.id}'
        else:
            return f'{self._name}'


class Cattle(ProtoCattle):
    ''' The Cattle-class inherits from the ProtoCattle-class and adds all
        characteristics that are common to both cows and bulls. This is the
        base-class modelling cattle in LivSim (see reference [2]).

        The time-resolution of the model is a (30-day) month. Rates are expres-
        sed accordingly e.g. body weight loss/gain is expressed in kg / month.

        Growth (body weight) and production (milk, manure, urine) are a result
        of the balance between the intake of energy and protein (through feed)
        and the expenditure of energy and protein on processes such as the fast-
        ing metabolism, activity, gestation and lactation. The requirements for
        these processes, in terms of energy and protein, are calculated using
        the system by the AFRC (see reference [1]). Amounts of P and K in urine
        and faeces are derived from [3].

        References
        ----------
        [1] AFRC (1993) Energy and Protein Requirements of Ruminants
        [2] Rufino MC, 2008. Quantifying the contribution of crop-livestock
            integration to African farming.
        [3] Efde SL, 1996. Quantified and integrated crop and livestock produc-
            tion analysis at the farm level.

        Input
        -----
        In running the model: feed --- see the "Feed" class.

        In parametrizing the model: breed/activity specific parameters e.g.
            - energy_cost_body_position_change
            - body_changes_per_day
            - horizontal_movement_per_day
            - vertical_movement_per_day
            - maximum_body_weight
            - minimum_body_weight
            - maximum_body_weight_gain
            - maximum_body_weight_loss
            - ...
        --- see the variable "parameters", how to access this is shown in the examples.

        Output
        ------
        The value of all the state/rate variables, most importantly the
            - body weight
            - body weight change
            - energy supply/demand
            - protein supply/demand
            - production rates
                - milk --- if it concerns a cow
                - manure (amount, N,P,K)
                - urine (amount, N,P,K)

        Examples
        --------

        Instantiating a default cow (default parameters/instantiation)

        >>> c = Cow()

        Inspecting it's properties (for the moment listed A--Z)

        >>> c
        Object: Cow

        Property                                 Value     Unit
        --------------------------------------------------------------
        E_demand_activity                        270.0000  MJ/month
        E_demand_fasting                         971.2099  MJ/month
        E_demand_gestation                       32.6455   ?
        _E_demand_gestation_A                     32.6455   ?
        _E_demand_gestation_B                     32.6432   ?
        E_demand_growth                          0.0000    MJ/month
        E_demand_growth_potential                0.0000    MJ/month
        ... + 80 more

        Setting the supplied feed (instances of the "Feed" class):

        >>> c.supply_roughage = some_supply_of_roughage
        >>> c.supply_concentrate = some_supply_of_concentrate

        The cow will "respond" accordingly i.e. its variable values will adjust
        accordingly.

        Updating a single time-step (a month):

        >>> c = c.update()

        Running the model, for a duration of N months, outputting the results to a
        DataFrame (spreadsheet data-structure):

        >>> df = c.run(duration=N)

        Notes
        -----
        Although the model is designed for a monthly time resolution, currently,
        the model allows for arbitrary time-step size in the updating of the conti-
        nuous variables. However, updating of the boolean variables does not. We
        might consider making this possible as well such that the whole model
        allows for arbitrary time-step size.
    '''
    _attrs_to_keep = None
    _name = 'Cattle'
    _version = '5-04-2018+'
    _sex = 'i'

    # the maximum number of iterations in the binary search for the weight
    # loss/gain
    _MAX_ITER = 20

    # the Qm (ME to GE ratio) used to estimate metabolism during fasting
    _FASTING_Q_m = 0.3

    # the allowed tolerance in the search for the actual weight loss/gain
    _TOLERANCE = 0.01

    # whether calf sex, natural death, etc. is
    # deterministic
    _DETERMINISTIC = True

    def __init__(self, library=None, age=0, breed=None, bw=None, deterministic=None, 
                 use_growth_curve=False, limit_output=False, herd=None):
    
        super().__init__(herd=herd)
    
        self._logger = logging.getLogger(f"{__name__}: {self._prefix}")

        if library is not None:
            self._breed_library = library
        else:
            self._breed_library = self._default_library

        # animal identifier
        self._breed = breed
        self._dead = False
        self._age = age
        self._months_in_simulation = 0
        self._dt = 1

        self._limit_output = limit_output
        if not limit_output:
            self._main_variables = None

        self.set_parameters(breed)
        self.set_parametrized_functions()

        self._deterministic = deterministic if deterministic is not None else self._DETERMINISTIC
        self.use_growth_curve = use_growth_curve

        self.set_body_weight(bw)
        self._cant_lose_enough_weight = False
        self._will_grow = True

        self._supply_milk = self.get_supply_milk()

        self._feed_intake = self.get_feed_intake()
        
        self._fraction_external_roughage = 0

        self._has_been_updated = False

        # References to [feed/roughage/concentrate]_supply-objects.
        self.feed_supply = None
        self.supply_roughage = None
        self.supply_concentrate = None

    @property
    def has_been_updated(self):
        return self._has_been_updated

    # region - initialization

    @property
    def growth_and_production(self):
        raise NotImplementedError(
            'Sub-classes should define sex-specific GrowthProduction-objects.')

    @property
    def requirements(self):
        raise NotImplementedError('Sub-classes should define sex-specific Requirements-objects.')

    @property
    def feed_intake(self):
        ''' Returns a reference to the FeedIntake-object. '''
        return self._feed_intake

    # endregion - initialization
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - interface with GrowthProduction-object

    @property
    def body_weight_change(self):
        ''' The body weight change in the current month (kg / month).

            The meaning of this property depends on the production level:
                - potential: the body weight change gives the potential growth
                  possible given the diet quality and growth potential of the
                  animal.
                
                - actual: body weight change may either be the actual growth
                  possible given the balance between ME and MP intake and
                  requirements, or the weight loss necessary to mobilize
                  sufficient energy and/or protein from tissues to meet nutri-
                  tive requirements.
        '''
        return self.growth_and_production._body_weight_change

    @property
    def _body_weight_change_estimate(self):
        return self.growth_and_production._body_weight_change_estimate

    @property
    def _ME_supply_weight_loss(self):
        return self.growth_and_production.ME_supply_weight_loss

    @property
    def _MP_supply_weight_loss(self):
        return self.growth_and_production.MP_supply_weight_loss

    # endregion - interface with GrowthProduction-object
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - interface with Requirements-object

    @property
    def _requirement_ME_maintenance(self):
        ''' The amount of ME required for maintenance (fasting metabolism +
            activity allowance) of the the animal.
        '''
        try:
            return self.requirements.requirement_ME_maintenance
        except AttributeError:
            raise AttributeError('requirement_ME_maintenance not set.') from None

    @property
    def ME_required(self):
        ''' The amount of ME required by the animal given its current state:
                - maintenance requirements 
                - growth requirements
                - gestation requirements
                - lactation requirements
            
            Growth-, gestation-, and lactation requirements are only added when
            applicable.
        '''        
        return self.requirements.requirement_ME_total

    @property
    def ME_required_non_growth(self):
        return self.requirements.requirement_ME_non_growth

    @property
    def MP_required(self):
        ''' The amount of MP required by the animal given its current state:
                - maintenance requirements 
                - growth requirements
                - gestation requirements
                - lactation requirements
            
            Growth-, gestation-, and lactation requirements are only added when
            applicable.
        '''
        return self.requirements.requirement_MP_total

    @property
    def MP_required_non_growth(self):
        return self.requirements.requirement_MP_non_growth

    # endregion - interface with Requirements-object
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - interface with FeedIntake-object

    @property
    def Q_m(self):
        ''' The metabolisability of the feed, expressed as the ratio between
            the ME-content and the GE-content of the feed. 
        '''
        return self.feed_intake._Q_m

    @property
    def feed_milk(self):
        return self.feed_intake.feed_milk
    
    @property
    def feed_concentrate(self):
        return self.feed_intake.feed_concentrate
    
    @property
    def feed_roughage(self):
        return self.feed_intake.feed_roughage

    @property
    def intake_feed_ME(self):
        return self.feed_intake._intake_feed_ME

    @property
    def intake_feed_MP(self):
        return self.feed_intake._intake_feed_MP

    @property
    def feed_intake_N(self):
        ''' The total amount of nitrogen consumed by the animal through its
            diet (g / month).
        '''
        return self.feed_intake.intake_feed_N * 1000

    @property
    def feed_intake_K(self):
        ''' The total amount of potassium consumed by the animal through its
            diet (g / month).
        '''
        return self.feed_intake._feed_intake_K

    # endregion - interface with FeedIntake-object
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - id related

    @property
    def dead(self):
        ''' Boolean indicating whether this animal is still alive. 
        '''
        return self._dead

    @property
    def breed(self):
        """ The name of the breed of this animal.

        :return: The name of the breed of this animal.
        :rtype: string
        """
        return self._breed

    @property
    def dt(self):
        """ The time step used in the Euler integration of the differential
        equations.

        :return: Time step of integration.
        :rtype: float
        """
        return self._dt

    @property
    def months_in_simulation(self):
        """ This property is used to determine in which month of the year the
        animal is during the simulation. This information is used by the Feed-
        class to select the right value for feed quality parameters that vary
        over the year.

        :return: The number of months that have passed in the simulation.
        :rtype: int
        """
        if self._herd is not None:
            return self._herd.months_in_simulation
        else:
            return self._months_in_simulation

    # endregion - id related
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - activities

    @property
    def energy_cost_standing(self):
        return self.parameters['activity_allowance']['energy_cost_standing']['value']

    @property
    def energy_cost_body_position_change(self):
        return self.parameters['activity_allowance']['energy_cost_body_position_change']['value']

    @property
    def energy_cost_horizontal_movement(self):
        return self.parameters['activity_allowance']['energy_cost_horizontal_movement']['value']

    @property
    def energy_cost_vertical_movement(self):
        return self.parameters['activity_allowance']['energy_cost_vertical_movement']['value']

    @property
    def time_spent_standing(self):
        return self.parameters['activity_allowance']['time_spent_standing']['value']

    @property
    def body_changes_per_day(self):
        return self.parameters['activity_allowance']['body_changes_per_day']['value']

    @property
    def horizontal_movement_per_day(self):
        return self.parameters['activity_allowance']['horizontal_movement_per_day']['value']

    @property
    def vertical_movement_per_day(self):
        return self.parameters['activity_allowance']['vertical_movement_per_day']['value']
    
    # endregion - activities
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - state

    @property
    def age(self):
        ''' The age of the animal in months.
        '''
        return self._age

    @property
    def body_weight(self):
        ''' Body weight of the animal in kilograms.
        '''
        return self._body_weight

    @property
    def will_grow(self):
        return self._will_grow

    @body_weight.setter
    def body_weight(self, value):
        ''' Set the new value for the body weight of the animals. Raises an
        exception when the new value is not feasible given the growth curves
        of the animal.
        '''
        min_weight = self.minimum_body_weight
        max_weight = self.maximum_body_weight
        if not min_weight <= value <= max_weight:
            if not self.will_lose_weight:
                raise ValueError(f'body weight {value:.2f}kg is outside the bounds of the ' +
                                  f'growth curves [{min_weight:.2f}, {max_weight:.2f}]')
        self._body_weight = value

    @property
    def _body_weight_estimate(self):
        return self.growth_and_production.body_weight_estimate

    @staticmethod
    def protein_to_N(var):
        ''' Protein contains, on average, about 16% nitrogen. Amounts of pro-
        tein are, therefore, divided by 6.25 to convert them into amounts of
        nitrogen.
        '''
        return var / 6.25

    # endregion - state
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - parameter related

    @property
    def energy_content_tissue(self):
        ''' ME supply through weight loss (MJ ME/kg loss).

            See [1] page 31, eq. 75.        
        '''
        # TODO: get rid of hard-coding
        return 19

    @property
    def protein_content_tissue(self):
        ''' MP supply through weight loss (g/kg loss).

            See [1] page 40, eq. 115.
        '''
        # TODO: get rid of hard-coding
        return 138

    @property
    def weaning_age(self):
        ''' Age (months) at which the animal is weaned. '''
        return self.parameters['milk_intake']['weaning_age']['value']

    @property
    def use_growth_curve(self):
        return self._use_growth_curve

    @use_growth_curve.setter
    def use_growth_curve(self, use_curve):
        ''' If the user indicates that an equation should be used to determine
            the maximum and minimum growth curves, the model checks whether a
            set of parameters is provided for the equation.
        '''
        if use_curve:
            if 'growth_curve_parameters' not in self.parameters['growth']:
                print('No parameters found for growth curve, using interpolation instead.')
                self._use_growth_curve = False
            else:
                self._use_growth_curve = use_curve
        else:
            self._use_growth_curve = use_curve

    def set_parameters(self, breed):
        ''' The behaviour of this method depends on the type of the breed
            parameter.

            - breed is None:
                loads the default breed from the breed library and selects the
                right set of sex-specific parameters

            - breed is an instance of str:
                loads the specified breed from the breed library and selects
                the right set of sex-specific parameters

            - breed is an instance of dict:
                selects the sex-specific set of parameters from the set of
                parameters provided
        '''
        if breed is None:
            self.from_library(self._default_preset)

        elif isinstance(breed, str):
            self.from_library(breed)

        elif isinstance(breed, dict):
            par_key = 'cow' if self._sex == 'f' else 'bull'  # TODO: make class attribute?
            self.parameters = breed[par_key]

        else:
            raise NotImplementedError(f'{breed} is not a valid breed identifier.')

    def get_parameterized_function(self, region, key):
        ''' Generates a function to perform a piece-wise linear interpolation
            on the data in the parameters-dictionary specified by the parameter
            'key'.
        '''
        xys = self.parameters[region][key]['value']

        return interpolate(xys)

    def set_parametrized_functions(self):
        ''' Sets the functions to perform linear interpolations. 
        '''
        f = self.get_parameterized_function

        self._milk_allowance_function = f(region='milk_intake', key='milk_allowance')
        
        self._maximum_body_weight_function = f(region='growth', key='maximum_body_weight')
        
        self._minimum_body_weight_function = f(region='growth', key='minimum_body_weight')
        
        self._mortality_probability_function = f(region='miscellaneous', key='mortality_probability')

        self._compensatory_gain_function = f(region='growth', key='compensatory_gain')

    # endregion - parameter related
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - body weight related

    def set_body_weight(self, body_weight):
        ''' If the animal is initialized with no initial body weight specified,
            the body weight will be set at the mean of the minimum and maximum
            body weights possible for the given age of the animal.
        '''
        if body_weight is None:
            res = mean([self.maximum_body_weight, self.minimum_body_weight])
        else:
            if body_weight < self.minimum_body_weight:
                self._logger.warning(
                    "Initial body weight too low; setting body weight to " +
                    f"{self.minimum_body_weight:.2f}"
                )
                res = self.minimum_body_weight

            elif body_weight > self.maximum_body_weight:
                self._logger.warning(
                    "Initial body weight too high; setting body weight to " + 
                    f"{self.maximum_body_weight:.2f}"
                )
                res = self.maximum_body_weight

            else:
                res = body_weight

        self.body_weight = res

    def growth_curve(self, K, x0, r):
        ''' A modified Gompertz function has been fitted to a wide range of
            body weight - age combinations. The parameters for the maximum and
            minimum growth curves have been derived from these fits.
        '''
        return K * np.exp(np.log(x0 / K) * np.exp(-r * self.age))

    @property
    def minimum_body_weight(self):
        ''' The minimum body weight an animal can have at any given age is
            either calculated using a growth curve [1] or derived from data
            using a linear interpolation [2].
        '''
        # [1]
        if self.use_growth_curve:
            pars = self.parameters['growth']['growth_curve_parameters']['value'][0]
            res = self.growth_curve(*pars)
        # [2]
        else:
            res = float(self._minimum_body_weight_function(self.age))

        return res

    @property
    def maximum_body_weight(self):
        ''' The maximum body weight an animal can have at any given age is
            either calculated using a growth curve [1] or derived from data
            using a linear interpolation [2].
        '''
        # [1]
        if self.use_growth_curve:
            pars = self.parameters['growth']['growth_curve_parameters']['value'][1]
            res = self.growth_curve(*pars)
        # [2]
        else:
            res = float(self._maximum_body_weight_function(self.age))

        return res

    @property
    def condition_index(self):
        ''' The weight relative to the age-dependent bounds.

            I.e. max. weight = 1, min. weight = 0.
        '''

        num = self.body_weight - self.minimum_body_weight
        denum = self.maximum_body_weight - self.minimum_body_weight

        res = num / denum

        if res < 0:
           if not self.will_die_by_starvation:
               self._logger.warning(f"Animal {self.id} is below minimum weight.")
           res = 0

        return res

    @property
    def rel_body_weight_change(self):
        ''' The relative body weight change (%/month).
        '''
        if self.dead:
            return 0
        else:
            return 100 * (self.body_weight_change / self.body_weight)

    @property
    def body_weight_change_per_day(self):
        ''' The body weight change estimate (g/day).
        '''
        return 1000 * self.body_weight_change / DAYS_PER_MONTH

    @property
    def maximum_body_weight_loss(self):
        ''' The maximum body weight loss as a fraction of the animals body
            weight.
        '''
        return self.parameters['growth']['maximum_body_weight_loss']['value'] / 100

    # endregion - body weight related
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - feed related

    @property
    def ME_supply_total(self):
        ''' The total supply of ME in a given month is the sum of the ME in the
            diet of the animal and the ME supplied through weight loss (if the
            animal loses weight).
        '''
        return self.intake_feed_ME + self._ME_supply_weight_loss

    @property
    def MP_supply_total(self):
        ''' The total supply of MP in a given month is the sum of the MP in the
            diet of the animal and the MP supplied through weight loss (if the
            animal loses weight).
        '''
        return self.intake_feed_MP + self._MP_supply_weight_loss

    @property
    def milk_amount(self):
        if self.feed_milk is not None:
            return self.feed_milk.amount
        else:
            return 0

    @property
    def concentrate_amount(self):
        if self.feed_concentrate is not None:
            return self.feed_concentrate.amount
        else:
            return 0

    @property
    def roughage_amount(self):
        if self.feed_roughage is not None:
            return self.feed_roughage.amount
        else:
            return 0

    @property
    def feed_supply(self):
        ''' Returns a reference to the feed storage if this animal is asso-
            ciated with a feed storage object.
        '''
        if self._herd is None:
            if self._feed_supply is None:
                raise AttributeError('No feed supply set for this animal.')
            return self._feed_supply
        else:
            return self._herd.feed_supply

    @feed_supply.setter
    def feed_supply(self, value):
        ''' Set a reference to a feed storage object for this animal. '''
        self._feed_supply = value
        if value is not None:
            self._feed_supply._animal = self

    @property
    def fraction_external_roughage(self):
        ''' The fraction of roughage fed to the animal that has been produced
            off-farm. This value is updated by the FeedStorage-object.
        '''
        return self._fraction_external_roughage

    @fraction_external_roughage.setter
    def fraction_external_roughage(self, value):
        self._fraction_external_roughage = value

    @property
    def potential_milk_intake(self):
        ''' The potential intake of milk (kg FW / month) as defined by the milk
            allowance for this breed and the age of the calf.
        '''
        if self.age >= self.weaning_age:
            return 0.0
        else:
            return float(self._milk_allowance_function(self.age))

    @property
    def supply_milk(self):
        return self._supply_milk

    @property
    def supply_concentrate(self):
        ''' The concentrate supply (a feed-object). '''
        if self.feed_supply is None:
            if self._supply_concentrate is None:
                raise AttributeError("No concentrate supply set for this animal.")
            return self._supply_concentrate
        else:
            if self._supply_concentrate is None:
               self.supply_concentrate = self.feed_supply.concentrate_to_give(self)
            return self._supply_concentrate
            
    @supply_concentrate.setter
    def supply_concentrate(self, value):
        ''' The concentrate supply (a feed-object) setter. '''
        self._supply_concentrate = value

    @property
    def supply_roughage(self):
        ''' The roughage supply (a feed-object). '''
        if self.feed_supply is None:
            if self._supply_roughage is None:
                raise AttributeError("No roughage supply set for this animal.")
            return self._supply_roughage
        else:
            if self._supply_roughage is None:
               self.supply_roughage = self.feed_supply.roughage_to_give(self)
            return self._supply_roughage
            
    @supply_roughage.setter
    def supply_roughage(self, value):
        ''' The roughage supply (a feed-object) setter. '''
        self._supply_roughage = value

    def get_supply_milk(self):
        ''' Creates and returns a MilkSupply-object, using breed specific
            parameters for the quality parameters of the milk.
        '''
        dm = self.parameters['milk_intake']['milk_DM_content']['value'] / 1000
        me = self.parameters['milk_intake']['milk_ME_content']['value']
        cp = self.parameters['milk_intake']['milk_CP_content']['value'] * 10

        return MilkSupply(dm=dm, me=me, cp=cp)

    def get_feed_intake(self):
        ''' Creates and returns a FeedIntake-object. 
        
            The FeedIntake-object handles both the intake of the amounts of 
            feed (milk, concentrates, roughages) as well as the supply of
            energy and protein from that feed.
        '''
        feed_intake = FeedIntakeCattle(animal=self)

        return feed_intake

    @property
    def level_of_feeding(self):
        ''' The level of feeding; is defined as the multiple of ME in the feed
        relative to the ME requirement for maintenance.

        See [1] page 16: Typically 1 <= L <= 3 (from maintenance to gestating).
        '''
        if self.dead:
            return 0
        else:
            return self.intake_feed_ME / self._requirement_ME_maintenance

    @property
    def correction_factor_C_L(self):
        ''' A correction factor co-determining the ME actually available to the
            animal.

            High levels of feeding decrease the efficiency with which the ME in
            the feed can be utilized by the animal. This is taken into account
            via this correction factor which is a function of the level of
            feeding.

            See [1] page 4 eq. 12: "The ME actually available to the animal
            [...] reduced significantly at high levels of feeding due to
            the increased outflow rate from the rumen [...]."

            TODO: implement correction factor in Requirements-class.
        '''
        return 1 + 0.018 * (self.level_of_feeding - 1)

    @property
    def sex_specific_N_excretion(self):
        ''' Part of the utilization of the nitrogen animals take in through
        feed differs between cows and bulls. 
        
        For bulls no additional excretions of N are distinguished, but for
        cows the amounts of nitrogen in their calf (when in gestation) or
        the milk (when in lactation) must also be accounted for.
        '''
        return 0

    # endregion - feed related
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - mortality related

    @property
    def mortality_probability(self):
        ''' The mortality probability in the coming month (1/month). '''
        return float(self._mortality_probability_function(self.age))

    @property
    def will_die_by_starvation(self):
        ''' Whether the animal will die in the coming month (bool). '''
        return self.body_weight < self.minimum_body_weight or self._cant_lose_enough_weight

    @property
    def will_die_by_chance(self):
        ''' Whether the animal will die in the coming month (bool). '''
        if self._DETERMINISTIC:
            return False
        else:
            r = random.random()
            P = self.mortality_probability

            return r < P

    @property
    def will_die(self):
        ''' Whether the animal will die in the coming month (bool). '''
        return self.will_die_by_starvation or self.will_die_by_chance

    def is_dead(self):
        instance_attrs = [i for i in dir(self) if i not in dir(type(self))]

        for attr in instance_attrs:

            value = getattr(self, attr)

            if isinstance(value, (int, float, bool)):

                if attr not in self._attrs_to_keep:

                    setattr(self, attr, 0)

            elif isinstance(value, FarmSimBaseClass):

                sub_attrs = [i for i in dir(value) if i not in dir(type(value))]

                for sub_attr in sub_attrs:

                    sub_value = getattr(value, sub_attr)

                    if isinstance(sub_value, (int, float, bool)):

                        if sub_attr not in value._attrs_to_keep:

                            setattr(value, sub_attr, 0)

        self._dead = True

    # endregion - mortality related
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - update state

    @property
    def compensatory_gain(self):
        """[summary]
        
        :return: The compensatory gain for this animal.
        :rtype: float
        """
        return self._compensatory_gain_function(self.Q_m) * DAYS_PER_MONTH

    @property
    def ME_shortage(self):
        ''' The difference in total ME demand and supply (g). '''
        return max(0, self.ME_required - self.ME_supply_total)

    @property
    def MP_shortage(self):
        ''' The difference in total MP demand and supply (g). '''
        return max(0, self.MP_required - self.MP_supply_total)

    def is_potential_production_possible(self):
        ''' If both ME and MP are available in excess over the requirements for
            potential production the animal can realize its full genetic poten-
            tial.
        '''
        self._will_grow = True

        abundant_ME = self.ME_shortage == 0 
        abundant_MP = self.MP_shortage == 0

        return (abundant_ME and abundant_MP)

    def set_will_grow(self):

        enough_ME = (self.ME_required_non_growth - self.ME_supply_total) < 1E-6 
        enough_MP = (self.MP_required_non_growth - self.MP_supply_total) < 1E-6

        self._will_grow = bool(enough_ME and enough_MP)

    @property
    def will_lose_weight(self):
        if self._body_weight_change_estimate < 0:
            return True
        else:
            return False

    def reset_requirements(self):
        """ This wrapper around the reset-method of the requirements-object is
        used by the growth_and_production-object when a change in the estimate
        for the body weight and/or body weight change necessitates an updated
        value for the nutritive requirements of the animal.
        """
        self.requirements.reset()

    def update(self):
        ''' Update the state of the animal based on the changes during the cur-
            rent timestep.

            The update process involves a number of steps:
                1. Reset the state of the production-object and update the feed
                   intake
                2. Determine whether the animal has died
                3. Determine whether the potential production level is fea-
                   sible; if needed, calculate the actual (i.e. reduced) level
                   of production.
                4. Update the state of the animal.
        '''
        # [1]
        self.supply_roughage = None
        self.supply_concentrate = None
        self.growth_and_production.reset()
        self.requirements.reset()
        self.feed_intake.reset()

        # [2]
        if self.dead:
            return self
        if self.will_die:            
            self.is_dead()
            return self
    
        self.feed_intake.set_feed_supplies(feeding_level='potential')
        
        self.set_will_grow()

        self.production_level = 'potential'

        self.feed_intake.set_feed_supplies(feeding_level='actual')

        potential_possible = self.is_potential_production_possible()
        
        # [3]
        if not potential_possible:

            self.production_level = 'actual'
            
        self.set_will_grow()

        # [4]
        self.body_weight += self.body_weight_change * self.dt
        
        self._age += 1  # self.dt
        self._has_been_updated = True

        return self
    
    def run(self, duration=1):
        ''' Run the model for a duration, returns a DataFrame. 
        '''
        if not isinstance(duration, int):
            raise ValueError

        if duration < 1:
            raise ValueError

        res = {}
        
        for i in range(duration):

            self.update()

            res = self.to_dict(obj=self, d=res, time_step=i, no_of_timesteps=duration)

            self._months_in_simulation += self.dt

        df = pd.DataFrame(res)

        return res, df

    # endregion - update state
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #


class Cow(Cattle):

    _attrs_to_keep = [
        '_age',
        '_id',
        '_months_in_simulation',
        'has_given_birth',
        'lactation_cycle_number',
        'will_die_by_chance',
        '_use_growth_curve'
    ]
    _main_variables = [
        'age',
        'body_weight',
        'will_die',
        'milk_yield',
        '_sex',
        'feed_intake'
    ]
    _name = 'Cow'
    _sex = 'f'

    def __init__(self, library=None, age=0, bw=None, breed=None, calvind=0, damid=None, 
                    gestating=0, lactating=0, lactind=0, pregind=0, deterministic=None,
                    use_growth_curve=False, limit_output=False, herd=None):

        super().__init__(library=library, age=age, breed=breed, bw=bw, deterministic=deterministic,
                            use_growth_curve=use_growth_curve, limit_output=limit_output, herd=herd)

        self._requirements = RequirementsCowAFRC(animal=self)
        self._growth_and_production = GrowthProductionCow(animal=self)

        self.set_parametrized_functions()

        # Gestation / lactation related
        self.is_lactating = lactating
        self.is_gestating = gestating
    
        self.previous_calf_sex = None
        if self.is_gestating:
            self.calf_sex = self.get_calf_sex()

        self._lactation_has_stopped = None
        self.is_bull_present = 1

        self.months_into_gestation = pregind
        self.months_into_lactation = lactind
        self.months_after_calving = calvind
        self.has_given_birth = 0

        self._can_conceive_given_management = 1

        self._milk_yield = 0
        self._lactation_cycle_number = 0
        
    # region - initialization

    @property
    def requirements(self):
        return self._requirements

    @property
    def growth_and_production(self):
        return self._growth_and_production

    # endregion - initialization
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - interface with the Requirements-object
    
    @property
    def _requirement_NP_gestation(self):
        ''' The amount of net protein (NP) required for gestation; is part of
            the total N retention by the animal.
        ''' 
        return self.requirements.requirement_NP_gestation

    # endregion - interface with Requirements-object
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - parameters

    @property
    def calf_birth_weight(self):
        return self.parameters['reproduction']['calf_birthweight']['value']

    def set_parametrized_functions(self):
        ''' A number of the parmeterized functions are unique to either sex of
            the breed. These function are set in addition to the generic ones.
        '''
        super().set_parametrized_functions()

        f = self.get_parameterized_function

        self._potential_milk_yield_function = f(region='lactation', key='milk_yield_potential')
        
        self._milk_body_condition_effect_function = f(region='lactation', key='milk_body_condition_effect')
        
        self._milk_age_effect_function = f(region='lactation', key='milk_age_effect')
        
        self._threshold_lactation_function = f(region='lactation', key='body_weight_loss_threshold_lactation_stop')

        self._gestation_feasibility_curve_function = f(region='reproduction', key='gestation_feasibility_curve')
        
        self._gestation_given_postpartum_function = f(region='reproduction', key='gestation_given_postpartum')
        
        self._gestation_given_condition_index_function = f(region='reproduction', key='gestation_given_condition_index')

        self._gestation_given_age_function = f(region='reproduction', key='gestation_given_age')

    # endregion - parameters
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - milk related

    @property
    def milk_butterfat_content(self):
        ''' A property of the milk, set by user via the parameters (g/kg).
        '''
        return self.parameters['lactation']['milk_butterfat_content']['value']

    @property
    def milk_energy_content(self):
        return self.requirements._milk_energy_content

    @property
    def milk_crude_protein_content(self):
        return self.parameters['lactation']['milk_crude_protein_content']['value']
    
    @property
    def _efficiency_ME_lactation(self):
        return self.requirements.efficiency_ME_lactation

    @property
    def _efficiency_MP_lactation(self):
        return self.requirements.efficiency_MP_lactation

    # endregion - milk related
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - initiate/terminate processes

    def start_gestation(self):
        ''' Starts gestation. '''
        self.is_gestating = True
        self.months_into_gestation = 0
        self.calf_sex = self.get_calf_sex()

    def stop_gestation(self):
        ''' Stops gestation.

            Note: implicitly linked to parturition and start of lactation.
        '''
        self.is_gestating = False
        self.months_after_calving = 0
        self.months_into_gestation = 0

    def start_lactation(self):
        ''' Starts lactation. '''
        self.is_lactating = True
        self.months_into_lactation = 0
        self._lactation_has_stopped = False

    def stop_lactation(self):
        ''' Stops lactation. '''
        self.is_lactating = False
        self.months_into_lactation = None
        self._lactation_cycle_number += 1
        self._lactation_has_stopped = True

    # endregion - initiate/terminate processes
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - booleans driving behaviour

    @property
    def will_lactation_start(self):
        ''' Whether lactation will start (bool) in the current time-step. '''
        return self.will_give_birth

    @property
    def will_gestation_start(self):
        ''' Whether gestation will start (bool) in the current time-step. '''

        # gestating -> nothing to start
        if self.is_gestating:
            return False

        if not self._DETERMINISTIC:
            r = random.random()
            p = self.conception_probability

            return r < p
        else:
            return self.deterministic_conception

    @property
    def will_gestation_stop(self):
        ''' Whether gestation will stop (bool) in the current time-step. '''
        return self.will_gestation_stop_given_duration

    @property
    def will_gestation_stop_given_duration(self):
        ''' Whether gestation will stop (bool) in the current time-step
            given the duration of the gestation cycle (completion).
        '''
        if self.is_gestating:
            threshold = self.parameters['reproduction']['gestation_duration']['value']
            return (self.months_into_gestation + self.dt) > threshold
        # not gestating -> nothing to stop
        else:
            return 0

    @property
    def will_give_birth(self):
        ''' Whether the cow will calf (bool) in the current time-step.
        '''
        if self.is_gestating:
            threshold = self.parameters['reproduction']['gestation_duration']['value']
            return (self.months_into_gestation + self.dt) > threshold
        # not gestating -> nothing to stop
        else:
            return 0

    @property
    def will_lactation_stop(self):
        ''' Whether lactation will stop (bool) in the current time-step. '''
        return self.will_lactation_stop_given_duration or self.will_lactation_stop_given_starvation

    @property
    def will_lactation_stop_given_starvation(self):
        ''' Whether lactation will stop (bool) in the current time-step
            given the body weight. '''
        return self.body_weight < self.minimum_body_weight

    @property
    def will_lactation_stop_given_duration(self):
        ''' Whether lactation will stop (bool) in the current time-step
            given the duration of lactation. '''

        # not lactating -> nothing to stop
        if not self.is_lactating:
            return 0
        else:
            threshold = self.parameters['lactation']['lactation_duration']['value']
            return (self.months_into_lactation + self.dt) > threshold

    @property
    def lactation_has_stopped(self):
        ''' Boolean to indicate whether lactation has stopped. This information
        is used for herd management. '''
        return self._lactation_has_stopped

    # endregion - booleans driving behaviour
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - conception related
    
    @property
    def minimum_body_weight_reproduction(self):
        """Animals of a given age need to exceed a certain body weight in order
        to be considered sexually mature. This is to simulate the effect that
        well-nourished animal mature more quickly than poorly nourished 
        animals.

        :return: Minimum body weight to allow conception.
        :rtype: float
        """
        return float(self._gestation_feasibility_curve_function(self.age))

    @property
    def conception_after_calving(self):
        return self.parameters['reproduction']['conception_after_calving']['value']

    @property
    def deterministic_conception(self):
        ''' Boolean indicating wether this animal will conceive based on the
            specified interval between calving and conception and dependent on
            the status of the animal.

            It is assumed here that conception will take place if this is 
            possible; even when the animal is at the lowest end of the range of
            body condition scores at which conception is even remotely likely
            conception will take place.
        '''
        c1 = self.can_conceive_given_maturity
        c2 = self.can_conceive_given_postpartum > 0
        c3 = self.p_conception_modifier_body_index > 0
        c4 = self.p_conception_modifier_age > 0
        c5 = self.will_conceive_given_interval
        c6 = self.can_conceive_given_management

        return int(c1 and c2 and c3 and c4 and c5 and c6)

    @property
    def conception_probability(self):
        ''' The probability of conception (1/month).

        See [2] A2.2, eq. 3 and 4.
        '''
        pot = self.potential_conception_probability
        c1 = self.can_conceive_given_maturity
        c2 = self.can_conceive_given_postpartum
        c3 = self.can_conceive_given_bull
        c4 = self.p_conception_modifier_body_index
        c5 = self.p_conception_modifier_age
        c6 = self.can_conceive_given_management

        return pot * c1 * c2 * c3 * c4 * c5 * c6

    @property
    def potential_conception_probability(self):
        ''' The potential probability of conception (1/month).

        See [2] A2.2, eq. 3 and 4.
        '''
        ar = self.parameters['reproduction']['potential_annual_calving_rate']['value']
        return 1 - (1 - ar)**(1 / 12)

    @property
    def can_conceive_given_maturity(self):
        ''' Indicates (bool) whether the cow can concieve given the body weight and age.

        See [2] A2.2, eq. 3 and 4.
        '''
        if self.body_weight >= self.minimum_body_weight_reproduction:
            return 1
        else:
            return 0

    @property
    def can_conceive_given_postpartum(self):
        ''' Indicates (0--1) whether the cow can concieve given the postpartum.

        See [2] A2.2, eq. 3 and 4.
        '''

        # if has not calved
        if self.months_after_calving is None:
            return 1
        else:
            return float(self._gestation_given_postpartum_function(self.months_after_calving))

    @property
    def p_conception_modifier_body_index(self):
        ''' Modifier for the probablity of conception given the body condition
            of the animal.

            The data for this relationship is based on Wagenaar and Kontrohr
            (1986).
        '''
        return float(self._gestation_given_condition_index_function(self.condition_index))

    @property
    def p_conception_modifier_age(self):
        ''' Modifier for the probablity of conception given the age of the 
            animal.

            After Rufino (2008)
        '''
        return float(self._gestation_given_age_function(self.age))


    @property
    def can_conceive_given_bull(self):
        ''' Indicates (bool) whether the cow can concieve given the presence of
            a bull.

            See [2] A2.2, eq. 3 and 4.
        '''
        return int(self.is_bull_present)

    @property
    def can_conceive_given_management(self):
        return self._can_conceive_given_management

    @can_conceive_given_management.setter
    def can_conceive_given_management(self, value):
        self._can_conceive_given_management = value

    @property
    def will_conceive_given_interval(self):
        ''' Indicates (bool) whether the animal will conceive given the time
            that has passed since the moment of last calving.
        '''
        if self.months_after_calving is None:
            return 1
        else:
            return int(self.months_after_calving >= self.conception_after_calving)

    # endregion - conception related
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - gestation related

    def get_calf_sex(self):

        if self._DETERMINISTIC:
            if self.previous_calf_sex == 'f':
                return 'm'
            else:
                return 'f'
        else:
            r = random.random()
            P_female = self.parameters['reproduction']['probability_female_calf']['value']

            if r <= P_female:
                return 'f'
            else:
                return 'm'

    def get_calf(self):
        ''' This method creates, and returns, a new object of either type Cow,
            or type Bull. 
            
            The type of the return value depends on the value of the attribute
            self.calf_sex.
        '''
        if self.calf_sex == 'f':
            res = Cow(
                bw=self.calf_birthweight,
                library=self._breed_library,
                breed=self.breed, 
                limit_output=self._limit_output, 
                herd=self._herd
            )            
            self.previous_calf_sex = 'f'            
        
        else:
            res = Bull(
                bw=self.calf_birthweight,
                library=self._breed_library,
                breed=self.breed, 
                limit_output=self._limit_output, 
                herd=self._herd
            )
            self.previous_calf_sex = 'm'
        
        return res
    
    @property
    def calf_birthweight(self):
        ''' The calf birthweight (kg). '''
        return self.parameters['reproduction']['calf_birthweight']['value']

    # endregion - gestation related
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - lactation related

    @property
    def lactation_cycle_number(self):
        ''' The number of lactations this animal has (partially) completed.
        '''
        return self._lactation_cycle_number

    @property
    def milk_yield_potential(self):
        ''' The potential milk yield (kg / month), given the lactation curve
            for this breed.
        ''' 
        if self.months_into_lactation is None:
            return 0
        else:
            t = self.months_into_lactation
            return float(self._potential_milk_yield_function(t))

    @property
    def milk_yield_corrected(self):
        ''' The milk yield (kg/month), corrected for the condition index and
            age of the animal.
        '''
        if self.is_lactating:

            c1 = self.milk_condition_factor
            c2 = self.milk_age_effect_factor

            pot = self.milk_yield_potential

            return max(0, c1 * c2 * pot)
        else:
            return 0

    @property
    def milk_condition_factor(self):
        return float(self._milk_body_condition_effect_function(self.condition_index))

    @property
    def milk_age_effect_factor(self):
        return float(self._milk_age_effect_function(self.age))

    @property
    def milk_yield(self):
        if not self.is_lactating:
            return 0
        else:
            return self._milk_yield

    @property
    def threshold_lactation(self):
        ''' The maximum amount of body weight (kg / month) an animal can sacri-
            fice before lactation is either limited or stopped.
        '''
        if self.is_lactating:
            return float(self._threshold_lactation_function(self.months_into_lactation))
        else:
            return float('inf')

    @property
    def milk_fat_yield(self):
        ''' The milk fat production (g/month).
        '''
        if self.is_lactating:            
            return self.milk_yield * self.milk_butterfat_content
        else:
            return 0

    @property
    def milk_protein_yield(self):
        ''' The milk protein production (g/month).
        '''
        if self.is_lactating:
            return self.milk_yield * self.milk_crude_protein_content
        else:
            return 0

    @property
    def sex_specific_N_excretion(self):
        ''' Part of the nitrogen consumed by the cow ends up in either the calf
            (if the cow is gestating) or in the milk (if the cow is lactating).
        '''        
        N_out_calf = self.protein_to_N(self._requirement_NP_gestation)  # (g/month)
        N_out_milk = self.protein_to_N(self.milk_protein_yield)                     # (g/month)

        return N_out_calf + N_out_milk

    # endregion - lactation related
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def update(self):

        if self.dead:
            return self

        if self.will_gestation_start:
            self.start_gestation()

        if self.will_lactation_start:
            self.start_lactation()

        if self.will_gestation_stop:
            self.stop_gestation()

        if self.will_lactation_stop:
            self.stop_lactation()

        if self.is_gestating:
            self.months_into_gestation += self.dt

        if self.is_lactating:
            self.months_into_lactation += self.dt

        if self.months_after_calving is not None:
            self.months_after_calving += self.dt

        if self.will_give_birth:
            self.has_given_birth = 1
        else:
            self.has_given_birth = 0

        self._milk_yield = self.milk_yield_corrected

        super().update()


class Bull(Cattle):
    
    _name = 'Bull'
    _sex = 'm'
    _attrs_to_keep = [
        '_age',
        '_id',
        '_months_in_simulation',
        'will_die_by_chance',
        '_use_growth_curve'    
    ]
    _main_variables = [
        'body_weight',
        'will_die',
        '_sex',
        'feed_intake'
    ]

    def __init__(self, library=None, age=0, breed=None, bw=None, deterministic=None, 
                 use_growth_curve=False, limit_output=False, herd=None):

        super().__init__(library=library, age=age, breed=breed, bw=bw, deterministic=deterministic, 
                            use_growth_curve=use_growth_curve, limit_output=limit_output, herd=herd)

        self._requirements = RequirementsBullAFRC(animal=self)
        self._growth_and_production = GrowthProductionBull(animal=self)

    @property
    def requirements(self):
        return self._requirements

    @property
    def growth_and_production(self):
        return self._growth_and_production
