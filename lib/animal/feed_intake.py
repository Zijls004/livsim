from ...shared_components import FarmSimBaseClass

DAYS_PER_MONTH = 30


class FeedIntakeCattle(FarmSimBaseClass):

    _name = 'FI'
    _attrs_to_keep = []

    def __init__(self, animal):

        super().__init__()

        self._animal = animal

    @property
    def _prefix(self):
        if self._animal._herd is not None:
            return f'{self._name}_{self._animal.id}'
        else:
            return f'{self._name}'

    @property
    def max_fraction_concentrate(self):
        return 0.4

    def reset(self):

        self._fraction_milk_consumed = None
        self._fraction_concentrate_consumed = None
        self._fraction_roughage_consumed = None

        self._max_voluntary_roughage_intake = None
        self._required_roughage_intake_ME = None
        self._required_roughage_intake_MP = None

    # region - interface with animal-object

    @property
    def _animal(self):
        return self._animal_

    @_animal.setter
    def _animal(self, value):
        self._animal_ = value

    @property
    def _potential_milk_intake(self):
        ''' The amount of milk potentially available to the animal (kg FW).
        '''  
        return self._animal.potential_milk_intake

    @property
    def _supply_milk(self):
        ''' Reference to the MilkSupply-object that models the supply of milk
            (as feed) to the animal. This supply of milk is determined in the
            parameters for the current breed.
        '''
        return self._animal.supply_milk

    @property
    def supply_roughage(self):
        ''' The potential supply of roughage to the animal, as determined by
            availability of feed and the feeding strategy. 
        '''
        return self._animal.supply_roughage

    @property
    def _supply_concentrate(self):
        ''' The potential supply of concentrate to the animal, as determined by
            availability of feed and the feeding strategy.
        '''
        return self._animal.supply_concentrate

    # endregion - interface with animal-object
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - references to feed-objects

    @property
    def feed_milk(self):
        ''' Returns either the potential supply of milk or the milk actually
            consumed by the animal, depending on the value of the production
            level.
        '''
        return self._feed_milk

    @feed_milk.setter
    def feed_milk(self, feed_milk):
        self._feed_milk = feed_milk

    @property
    def feed_concentrate(self):
        """ Returns either the potential supply of concentrate or the concen-
        trate actually consumed by the animal, depending on the value of the
        production level.
        """
        return self._feed_concentrate

    @feed_concentrate.setter
    def feed_concentrate(self, feed_concentrate):
        self._feed_concentrate = feed_concentrate

    @property
    def feed_roughage(self):
        """ Returns either the potential supply of roughage or the roughage
        actually consumed by the animal, depending on the value of the 
        production level.
        """
        if self._animal.dead:
            return None
        else:
            return self._feed_roughage

    @feed_roughage.setter
    def feed_roughage(self, feed_roughage):
        self._feed_roughage = feed_roughage

    # endregion - references to feed-objects
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @property
    def roughage_DMD(self):
        if self.supply_roughage is not None:
            return self.supply_roughage.DMD
        else:
            return 0

    @property
    def _Q_m(self):
        ''' The metabolisability of the feed, expressed as the ratio between
            the ME-content and the GE-content of the feed.
        '''
        roughage = self.feed_roughage
        concentrate = self.feed_concentrate

        if roughage is None and concentrate is None:
            res = self._animal._FASTING_Q_m

        elif roughage is None and concentrate is not None:
            res = concentrate.Q_m

        elif roughage is not None and concentrate is None:
            res = roughage.Q_m

        else:
            # Weighted average
            Qr = roughage.Q_m
            Qc = concentrate.Q_m

            wr = roughage.amount
            wc = concentrate.amount

            res = (wr*Qr + wc*Qc) / (wr+wc)

        return res

    @property
    def max_voluntary_roughage_intake(self):
        ''' The potential roughage intake (kg DM/month).

            Makes use of the model of Conrad (1964) J. Dairy Sci. 47: 54-60:

                Ip = 0.0107*BW/(1-DMD)

            where Ip is intake (kg DM/day), BW is the bodyweight (kg) and
            DMD is the dry-matter digestibility of the feed.

            See also [2], A2.3.
        '''
        if self._max_voluntary_roughage_intake is None:
            bw = self._animal.body_weight
            dmd = self.roughage_DMD
            max_intake = DAYS_PER_MONTH * 0.0107 * bw / (1 - dmd / 1000)

            self._max_voluntary_roughage_intake = max_intake

        return self._max_voluntary_roughage_intake

    @property
    def fraction_milk_required(self):
        ''' TODO: what determines milk intake: energy or protein? 

            | MZ: Currently there is a mis-match between the energy content and
            | the protein content of the milk; when energy requirement deter-
            | mines milk intake the animal does not get enough protein and
            | dies. I've 'solved' this by making milk intake depend on the
            | scarcest resource. 
        '''
        if self._fraction_milk_consumed is None:
            ME_demand = self._animal.ME_required
            ME_supply = self.feed_milk.ME_total

            if ME_supply == 0:
                return 0

            MP_demand = self._animal.MP_required
            MP_supply = self.feed_milk.MP_total

            fraction_ME = ME_demand / ME_supply
            fraction_MP = MP_demand / MP_supply

            if (ME_supply or MP_supply) == 0:
                self._fraction_milk_consumed = 0
            else:
                self._fraction_milk_consumed = max(min(1, max(fraction_ME, fraction_MP)), 0)

        return self._fraction_milk_consumed

    @property
    def fraction_concentrate_required(self):
        ''' The fraction of the supplied concentrate that is actually consumed
            by the animal.

            Milk (if applicable) is consumed preferentially. If the nutritive
            requirements are not met after that an amount of concentrates
            is consumed that is either equal to the amount required to meet
            requirements, equal to the amount on offer, or equal to the amount
            the animal will voluntarily consume.
        '''
        if self._fraction_concentrate_consumed is None:
            
            m = self.feed_milk
            c = self._supply_concentrate

            consumed = m.ME_total if m is not None else 0
            supplied = c.ME_total if c is not None else 0
            required = self._animal.ME_required - consumed
            
            if supplied == 0:
                self._fraction_concentrate_consumed = 0
            else:
                self._fraction_concentrate_consumed = max(min(1, required / supplied), 0)

        return self._fraction_concentrate_consumed

    @property
    def fraction_roughage_required(self):
        ''' The fraction of the supplied roughage that is actually consumed by
            the animal.

            Milk (if applicable) and concentrates are consumed preferentially.
            If the nutritive requirements are not met after that an amount of
            roughage is consumed that is either equal to the amount required to
            meet requirements, equal to the amount on offer, or equal to the
            amount the animal will voluntarily consume.
        '''
        if self._fraction_roughage_consumed is None:

            m = self.feed_milk
            c = self.feed_concentrate
            r = self.supply_roughage

            milk_ME = m.ME_total if m is not None else 0
            concentrate_ME = c.ME_total if c is not None else 0
            
            consumed = milk_ME + concentrate_ME
            supplied = r.ME_total if r is not None else 0
            required = self._animal.ME_required - consumed

            if supplied == 0:
                self._fraction_roughage_consumed = 0
            else:
                self._fraction_roughage_consumed = max(min(1, required / supplied), 0)

        return self._fraction_roughage_consumed

    @property
    def actual_milk_consumed(self):

        if self._supply_milk is None:
            return None

        if self._animal.age >= self._animal.weaning_age:
            f = 0
        else:
            f = self.fraction_milk_required

        if f < 0:
            raise ValueError("The fraction of milk that is consumed is negative...")
        else:
            res = self._supply_milk.__copy__()
            res.amount *= f

        return res

    @property
    def actual_concentrate_consumed(self):
        ''' The concentrate actually consumed by the animal. 

            Makes use of the fraction of feed to be eaten (accepted) to derive
            the associated accepted feed (an object).
        '''
        if self._supply_concentrate is None:
            return None

        f = self.fraction_concentrate_required
        
        if f > 0:
            res = self._supply_concentrate.__copy__()
            res.amount *= f

        elif f == 0:
            res = None
        else:
            raise ValueError("The fraction of concentrate that is consumed is negative...")

        return res

    @property
    def actual_roughage_consumed(self):
        ''' The roughage actually consumed by the animal. 

            This property returns a feed-object that represents the weighted
            feed quality of the diet, with the right amount (DM) of feed con-
            sumed by the animal.
        '''
        if self.supply_roughage is None:
            return None

        f = self.fraction_roughage_required

        if f >= 0:
            res = self.supply_roughage.__copy__()

            max_intake = self.max_voluntary_roughage_intake
            required_intake = res.amount * f

            res.amount = min(max_intake, required_intake)
        elif f == 0:
            res = None
        else:
            raise ValueError("The fraction of roughage that is consumed is negative...")

        return res

    @property
    def roughage_refused(self):
        ''' Returns an object detailing the roughage offered to, but not eaten
            by, the animal. This can give insight in the efficiency of the cur-
            rent feeding management.
        '''
        if None in (self.supply_roughage, self.feed_roughage):
            return None

        amount_refused = self.supply_roughage.amount - self.feed_roughage.amount

        if amount_refused > 0:
            res = self.supply_roughage.__copy__()
            res.amount = amount_refused
        
        elif amount_refused == 0:
            return None

        else:
            raise ValueError("The fraction of roughage that is refused is negative...")

        return res

    @property
    def fraction_concentrate_in_diet(self):
        ''' The fraction of the diet that consists of concentrates, expressed
            as the fraction of the total dry matter intake.
        '''
        roughage = self.intake_roughage_DM
        concentrate = self.intake_concentrate_DM

        if roughage == concentrate == 0:
            return 0
        else:
            return concentrate / (roughage + concentrate)

    def reduce_concentrate_intake(self):
        ''' Concentrate intake is limited to avoid rumen acidosis. The maxi-
            mum allowed fraction of concentrates in the diet is here hard-coded
            at 40% of total dry matter intake.

            If the diet contains more than 40% concentrates the intake of con-
            centrates is reduced.

            If the animal has not been weaned yet, the roughage intake is still
            quite low. For these animals, therefore, concentrate intake is not
            limited by roughage intake.
        '''
        if self._animal.age < self._animal.weaning_age:
            return

        total_ME = self._intake_feed_ME
        milk_ME = self.intake_milk_ME

        f_c = self.max_fraction_concentrate
        f_r = 1 - f_c

        concentrate = self.feed_concentrate
        roughage = self.feed_roughage

        # Maximum amounts of concentrate and roughage available to the animal.
        max_concentrate = self._supply_concentrate.amount
        max_roughage = min(self.supply_roughage.amount, self.max_voluntary_roughage_intake)

        average_ME = f_c * concentrate.ME + f_r * roughage.ME

        intake_required = (total_ME - milk_ME) / average_ME

        amount_roughage = 0.6 * intake_required
        if amount_roughage > max_roughage:
            amount_roughage = max_roughage
            amount_concentrate = (f_c / f_r) * amount_roughage
        else:
            amount_concentrate = f_c * intake_required

        if amount_concentrate > max_concentrate:
            amount_concentrate = max_concentrate

        roughage.amount = amount_roughage
        concentrate.amount = amount_concentrate

    def set_feed_supplies(self, feeding_level='potential'):
        ''' Set the supplies of milk, concentrates and roughages to the animal.

            Behaviour of the method depends on the value of the parameter
            'feeding_level':

            - potential: assign feed-objects for milk, concentrate, and rough-
                age, using the amounts potentially available.

            - actual: assign feed-objects for milk, concentrate, and roughage, 
                using the amounts actually consumed by the animal. These
                amounts depend both on the availability of the feed and on the
                energy requirements of the animal.

            The order in which the feed-objects are assigned is important here,
            since the feeding preference of the animal dictates that first milk
            will be consumed, followed by concentrates and, finally, roughages.
        '''
        assert feeding_level in ['actual', 'potential']

        if feeding_level == 'potential':
            # Set right amount of milk
            self._supply_milk.amount = self._potential_milk_intake

            self.feed_milk = self._supply_milk.__copy__()
            
            if self._supply_concentrate is not None:
                self.feed_concentrate = self._supply_concentrate.__copy__()
            else:
                self.feed_concentrate = None
            
            if self.supply_roughage is not None:
                self.feed_roughage = self.supply_roughage.__copy__()
    
                if self.feed_roughage.amount > self.max_voluntary_roughage_intake:
                    self.feed_roughage.amount = self.max_voluntary_roughage_intake
            else:
                self.feed_roughage = None

            if self.fraction_concentrate_in_diet > self.max_fraction_concentrate:
                self.reduce_concentrate_intake()

        elif feeding_level == 'actual':

            self.feed_milk = self.actual_milk_consumed
            self.feed_concentrate = self.actual_concentrate_consumed
            self.feed_roughage = self.actual_roughage_consumed

            if self.fraction_concentrate_in_diet > self.max_fraction_concentrate:
                self.reduce_concentrate_intake()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - intake of ME and MP

    @property
    def intake_milk_ME(self):
        ''' The amount of metabolizable energy (ME) in the source of milk
            returned by self.feed_milk.
        '''
        if self.feed_milk is None:
            return 0
        else:
            return self.feed_milk.ME_total

    @property
    def intake_roughage_ME(self):
        ''' The amount of metabolizable energy (ME) in the source of roughage
            returned by self.feed_roughage.
        '''
        if self.feed_roughage is None:
            return 0
        else:
            return self.feed_roughage.ME_total

    @property
    def intake_concentrate_ME(self):
        ''' The amount of metabolizable energy (ME) in the source of concentrate
            returned by self.feed_concentrate.
        '''
        if self.feed_concentrate is None:
            return 0
        else:
            return self.feed_concentrate.ME_total

    @property
    def _intake_feed_ME(self):
        ''' The total amount of metabolizable energy (ME) in the feed. 
        '''
        return self.intake_milk_ME + self.intake_concentrate_ME + self.intake_roughage_ME

    @property
    def intake_milk_MP(self):
        ''' The amount of metabolizable protein (MP) in the source of milk
            returned by self.feed_milk.
        '''
        if self.feed_milk is None:
            return 0
        else:
            return self.feed_milk.MP_total

    @property
    def intake_roughage_MP(self):
        ''' The amount of metabolizable protein (MP) in the source of roughage
            returned by self.feed_roughage.
        '''
        if self.feed_roughage is None:
            return 0
        else:
            return self.feed_roughage.MP_total

    @property
    def intake_concentrate_MP(self):
        ''' The amount of metabolizable protein (MP) in the source of concen-
            trate returned by self.feed_concentrate.
        '''
        if self.feed_concentrate is None:
            return 0
        else:
            return self.feed_concentrate.MP_total

    @property
    def _intake_feed_MP(self):
        ''' The total amount of metabolizable protein (MP) in the feed. 
        '''
        return self.intake_milk_MP + self.intake_concentrate_MP + self.intake_roughage_MP

    # endregion - intake of ME and MP
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - feed intake in terms of amount DM and N

    @property
    def intake_feed_DM(self):
        ''' The dry matter intake from the feed (kg / month). 
        '''
        sources = [
            self.feed_concentrate,
            self.feed_roughage,
            self.feed_milk
        ]
        # N content of feed : "x.N" in g/kg DM
        # amount of feed    : "x.amount" in kg
        intake_feed_DM = sum([x.amount for x in sources if x is not None])

        return intake_feed_DM

    @property
    def intake_feed_DM_per_day(self):
        ''' The feed intake (kg DM/day). 
        '''
        return self.intake_feed_DM / DAYS_PER_MONTH

    @property
    def intake_feed_N(self):
        ''' The nitrogen intake from the feed (kg / month). 
        '''
        sources = [
            self.feed_concentrate,
            self.feed_roughage,
            self.feed_milk
        ]
        # N content of feed : "x.N" in g/kg DM
        # amount of feed    : "x.amount" in kg
        N_in_feed = sum([x.amount * x.N for x in sources if x is not None])

        return N_in_feed / 1000

    @property
    def intake_milk_DM(self):
        """ Intake of milk (kg DM / month).

        :return: The amount of milk consumed by the animal.
        :rtype: float
        """
        x = self.feed_milk

        if x is not None:
            return x.amount
        else:
            return 0

    @property
    def intake_roughage_DM(self):
        ''' Intake of DM from roughage (kg DM / month). 
        '''
        x = self.feed_roughage

        if x is not None:
            return x.amount
        else:
            return 0

    @property
    def intake_roughage_N(self):
        ''' Intake of N from roughage (kg N / month). 
        '''
        x = self.feed_roughage

        if x is not None:
            return (x.amount * x.N) / 1000
        else:
            return 0

    @property
    def intake_external_roughage_DM(self):
        ''' Intake of DM from external roughage (kg DM / month).
        '''
        x = self.feed_roughage

        if x is not None:
            return x.amount * self._animal.fraction_external_roughage
        else:
            return 0

    @property
    def intake_external_roughage_N(self):
        ''' Intake of N from external roughage (kg N / month). '''
        x = self.feed_roughage

        if x is not None:
            return (x.amount * self._animal.fraction_external_roughage * x.N) / 1000
        else:
            return 0

    @property
    def intake_concentrate_DM(self):
        ''' Intake of DM from concentrate (kg DM / month). '''
        x = self.feed_concentrate

        if x is not None:
            return x.amount
        else:
            return 0

    @property
    def intake_concentrate_N(self):
        ''' Intake of N from concentrate (kg N / month). '''
        x = self.feed_concentrate

        if x is not None:
            return (x.amount * x.N) / 1000
        else:
            return 0

    @property
    def _feed_intake_K(self):
        ''' The potassium intake from the feed (g/month). '''
        sources = [self.feed_concentrate, self.feed_roughage]

        return sum([x.amount * x.K for x in sources if x is not None])

    # endregion - feed intake in terms of amount DM and N
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - required feed intake

    @property
    def required_roughage_intake_ME(self):
        """The amount of roughage the animal needs to take in to meet the
        amount of ME required in the current timestep.

        Part of the ME requirement is satisfied by the intake of milk (for 
        animals at a pre-weaning age) and concentrates. The remainder of the
        ME requirement needs to be met by the intake of roughage.
        """
        if self._required_roughage_intake_ME is None:
            a = self._animal.ME_required
            b = self.intake_milk_ME
            c = self.intake_concentrate_ME
            d = self.actual_roughage_consumed.ME

            self._required_roughage_intake_ME = (a - b - c) / d
        
        return self._required_roughage_intake_ME
    
    @property
    def required_roughage_intake_MP(self):
        """The amount of roughage the animal needs to take in to meet the
        amount of MP required in the current timestep.
        """
        if self._required_roughage_intake_MP is None:
            a = self._animal.MP_required
            b = self.intake_milk_MP
            c = self.intake_concentrate_MP
            d = self.actual_roughage_consumed.MP

            self._required_roughage_intake_MP = (a - b - c) / d

        return self._required_roughage_intake_MP
    
    # endregion - required feed intake