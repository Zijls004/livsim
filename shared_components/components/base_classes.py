import sys


class FarmSimBaseClass(object):
    """Base class from which all classes in FARMSIM inherit.

    This class defines basic functionality that all classes in FARMSIM share. 
    Furthermore, the recursive algorithm that writes the values of an objects 
    attributes to the output dictionary uses the data-type of FarmSimBaseClass
    to know when to break the recursive loop.
    """
    def __init__(self):

        self._is_base_class = True
        self._output_variables = None

    @staticmethod
    def to_dict(obj, d: dict, time_step: int, no_of_timesteps: int) -> dict:
        """Returns a dict [d] of the instance variables of the object [obj]
        passed as the first argument. 
        
        If the object specifies which variables should be included in the 
        output, in the objects ._main_variables-attribute, only those variables
        will be written to the dict. Otherwise all non-private instance 
        variables will be included.

        To make sure that no nested public objects of type FarmSimBaseClass are
        excluded this method checks for the presence of any such objects even 
        if obj.main_variables is not set to None.

        Nested objects and lists are dealt with by calling this method re-
        cursively.

        :param obj: The object from which we'd like to extract output.
        :type obj: var
        :param time_step: The time step for which output is stored.
        :type time_step: int
        :param initialize: Boolean indicating whether this method is called to
            initialize the output-dictionary.
        :type initialize: bool
        :param no_of_timesteps: The total number of timesteps in the 
            simulation.
        :type no_of_timesteps: int
        :param d: The dictionary in which output is stored.
        :type d: dict
        :returns: The dictionary in which output is stored.
        :rtype: dict
        """
        if obj._output_variables is None:
            farmsim_objects = []
            public_attributes = [i for i in dir(obj) if not i.startswith('_')]

            # Limit output to a specified set of attributes if such a set is 
            # specified on any given object.
            if hasattr(obj, '_main_variables') and obj._main_variables is not None:
                output_attributes = obj._main_variables

                # If there are nested objects of class FarmSimBaseClass that are 
                # not already part of the requested output we add them here.
                for attr in public_attributes:

                    # To avoid duplicating output from a nested object we only wish
                    # to add an object to the list of nested objects if it is not 
                    # already included in the list of output attributes.
                    if attr not in output_attributes:
                        a = getattr(obj, attr)
                    
                        # Include output from objects that:
                        #   - are of class FarmSimBaseClass
                        #   - are not private
                        if hasattr(a, "_is_base_class") \
                            and a._is_base_class \
                                and not attr.startswith('_'):

                            # Some objects may have an attribute that indicates 
                            # their attributes should not be included in the output.
                            if not hasattr(a, '_write_output'):
                                farmsim_objects.append(attr)
                            elif a._write_output:
                                farmsim_objects.append(attr)

            else:
                output_attributes = public_attributes

            obj._output_variables = farmsim_objects + output_attributes

        for key in obj._output_variables:
            try:
                value = getattr(obj, key)

                # If the current attribute is of class FarmSimBaseClass call 
                # the to_dict-method recursively.
                if hasattr(value, "_is_base_class") and value._is_base_class:
                    d = obj.to_dict(obj=value, 
                                    d=d,
                                    time_step=time_step, 
                                    no_of_timesteps=no_of_timesteps)

                # If the current attribute is a a list, check whether the
                # elements of that list are of class FarmSimBaseClass, and if
                # so call the to_dict-method recursively.
                elif isinstance(value, list):
                    
                    for i, v in enumerate(value):
                        if hasattr(v, "_is_base_class") and v._is_base_class:
                            d = obj.to_dict(obj=v, 
                                            d=d, 
                                            time_step=time_step,
                                            no_of_timesteps=no_of_timesteps)

                # If the current attribute is an actual value of some kind it 
                # is stored in the dictionary. Space is pre-allocated in the
                # dictionary if the attribute is not yet part of the output.
                elif isinstance(value, (int, float, str, bool)):
                    k = f'{obj._prefix}_{key}'
                    if k not in d:
                        d[k] = [None] * no_of_timesteps
                    d[k][time_step] = value

                else:
                    pass

            except Exception as e:
                print(f'key: {key}  --> causes: {sys.exc_info()[1]} on object {obj._name}')
                
        return d
