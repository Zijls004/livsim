#!/usr/bin/env python

########
# README
########

''' Provides the AFRC Feed model.

Developed for Python 3.
'''

##################
# Import Libraries
##################

import os
import numpy as np
import yaml
import sys


from copy import deepcopy

if getattr(sys, 'frozen', False):
    main_dir = os.path.dirname(sys.executable)
    USE_LOCAL_REPOSITORY = False
else:
    path = os.path.abspath(__file__)
    dir_path = os.path.dirname(path)
    lib_dir = os.path.dirname(dir_path)
    main_dir = os.path.dirname(lib_dir)
    USE_LOCAL_REPOSITORY = True

noalias_dumper = yaml.dumper.SafeDumper
noalias_dumper.ignore_aliases = lambda self, data: True

def plain_yaml_dump(diktionary):
    noalias_dumper = yaml.dumper.SafeDumper
    noalias_dumper.ignore_aliases = lambda self, data: True
    return yaml.dump(diktionary, default_flow_style=False, Dumper=noalias_dumper)

def read_yaml(path):

    with open(path) as f:
        res = yaml.load(f, Loader=yaml.SafeLoader)
    return res

class Feed(object):
    ''' Nutritional info on the feed quality.

    Primarily in terms of metabolisable energy (ME, MJ/kg DM)
    and metabolisable protein (MP, g/kg DM).

    First of all we estimate the size of 3-types of potential protein sources:
    - Quickly Digestible Protein (QDP)    - digested mainly in rumen.
    - Slowly Digestible Protein (SDP)     - digested mainly in rumen.
    - Undegraded Digestible Protein (UDP) - digested in lower tract, not in rumen.

    Then we estimate using the ME available for rumen microbial activity (FME)
    the actual protein got out of the QDP and SDP to eventually get to the
    digestible metabolisble true protein (DMTP).

    Finally the UDP and DMTP combined give us the estimate for the MP, the
    protein actually available for the ruminant to do something with.

    For a somewhat nice overview of the derivation process
    see [1] page 20.

    Parameters
    ----------
    ME
        Metabolisable Energy (MJ/kg)
    FME
        Fermentable Metabolisable Energy (MJ/kg DM).

        The ME from the non-fats, and the non-fermentation acids.
        The ME potentially available for the rumen microbes.
        See [1] page 3.


    References
    ----------
    [1] AFRC(1993) Energy and Protein Requirements of Ruminants
    [2] https://extension.psu.edu/determining-forage-quality-understanding-feed-analysis.
    [3] Efde SL, 1996. Quantified and integrated crop and livestock production
        analysis at the farm level. 
    '''

    default_parameters = yaml.load('''
    kind_of_feed:
        unit : '-'
        value: roughage
    ADIN:
        unit: g/kg DM
        value: 1.5
    CP:
        unit: g/kg DM
        value: 54
    DM:
        unit: g DM/kg FM
        value: 850
    DMD:
        unit: g/kg DM
        value: 538
    FME:
        unit: MJ/kg DM
        value: 6.8
    GE:
        unit: MJ/kg DM
        value: 18.4
    K:
        unit: g/kg DM
        value: 0.014
    ME:
        unit: MJ/kg DM
        value: 7.6
    NDF:
        unit: g/kg DM
        value: 841
    P:
        unit: g/kg DM
        value: 0.002
    a:
        unit: 1
        value: 0.3
    b:
        unit: 1
        value: 0.5
    c:
        unit: 1
        value: 0.07
    metadata:
        compiler: Willem Hekman
        version: 12-9-18
        info: Parameter values copied from the education version.
    ''', Loader=yaml.SafeLoader)

    units = yaml.load('''
        ADIN: 'g/kg DM'
        CP: 'g/kg DM'
        DM: 'g DM/kg FM'
        DMD: 'g/kg DM'
        DMTP: 'g/kg DM'
        DUP: 'g/kg DM'
        ERDP: 'g/kg DM'
        FME: 'MJ/kg DM'
        GE: 'MJ/kg DM'
        K: 'kg/kg DM'
        L: '1'
        MCP: 'g/kg DM'
        ME: 'MJ/kg DM'
        ME_total: 'MJ'
        MP_total: 'g'
        MP: 'g/kg DM'
        MTP: 'g/kg DM'
        NDF: 'g/kg DM'
        P: 'kg/kg DM'
        QDP: 'g/kg DM'
        RDP: 'g/kg DM'
        SDP: 'g/kg DM'
        UDP: 'g/kg DM'
        a: '1'
        amount: 'kg DM'
        b: '1'
        c: '1'
        r: '1/hr'
        y: '1'
        faecal_N: 'g N/kg DM'
        faecal_P: 'g P/kg DM'
        faecal_K: 'g K/kg DM'
    ''', Loader=yaml.SafeLoader)

    _name = 'Feed'

    if USE_LOCAL_REPOSITORY:
        _library_path = os.path.join(main_dir, 'data', 'feed_library.yaml')
        library = read_yaml(_library_path)

    def to_library(self, key=None, path=None):

        if path is None:
            path = self._library_path

        data = self.parameters

        if key is None:
            return data
        else:
            with open(path, 'r') as file:
                old_lib = yaml.load(file.read(), Loader=yaml.SafeLoader)

            if old_lib is None:
                new_lib = {}
            else:
                new_lib = deepcopy(old_lib)

            with open(path, 'w') as file:
                new_lib[key] = data
                file.write(yaml.dump(new_lib, default_flow_style=False, Dumper=noalias_dumper))

            return data

    def __init__(self, library=None, parameters=None, category=None, amount=1,
                    animal=None, crop_residues=False):

        if library is not None:
            self.library = library
            
        self.amount = amount

        if parameters is None:
            # note, the deepcopy is very important here
            # otherwise A.parameters == B.parameters == x.default_parameters
            # for different feeds A,B and Feed class-object x...: un-expected behaviour
            self.parameters = deepcopy(self.default_parameters)
            print('Warning: Using default parameters: maize stover')
        else:
            if isinstance(parameters, str):
                
                if crop_residues:
                    try:
                        feed_name = self.library['cr_to_feed'][parameters]
                    except KeyError:
                        raise KeyError(f"No feed name specified for residues of crop: {parameters}")
                else:
                    feed_name = parameters

                if category is not None:
                    assert category in ('roughage', 'concentrate'), \
                        "Please make sure that the feed category is either 'roughage' or 'concentrate'."
                    
                    if feed_name in self.list_of_pastures:
                        self.parameters = deepcopy(self.library['pasture'][feed_name])
                    
                    elif feed_name in self.list_of_forages:
                        self.parameters = deepcopy(self.library['forage'][feed_name])
                    
                    elif feed_name in self.list_of_concentrates:
                        self.parameters = deepcopy(self.library['concentrate'][feed_name])

                    else:
                        raise KeyError(f'No parameters found for feed: {feed_name}')

                    self.name = feed_name
                
                else:
                    raise ValueError("Please provide a value for the category-parameter.")

            else:
                self.parameters = parameters
                self.name = 'custom'

        check_parameters(self)

        # Ruminant interface:
        # Reference to ruminant to possibly fetch level of feeding
        self._animal = animal
        self._month_of_year = 0
        self._requirement_ME = 0
        self._requirement_MP = 0

        # Dummy value of level of feeding
        # --- actual level of feeding is used if linked to a ruminant
        self._L = 1


    @property
    def list_of_forages(self):
        return list(self.library['forage'])

    @property
    def list_of_pastures(self):
        return list(self.library['pasture'])

    @property
    def list_of_concentrates(self):
        return list(self.library['concentrate'])

    @staticmethod
    def protein_to_N(var):
        ''' Protein contains, on average, about 16% nitrogen. Amounts of pro-
            tein are, therefore, divided by 6.25 to convert them into amounts
            of nitrogen.
        '''
        return var / 6.25

    def set_amount(self, value):
        self.amount = value

    def __copy__(self):
        ''' The copy method: returns a copy of this object by making a new identical one. '''
        return Feed(
            amount = self.amount,
            parameters = deepcopy(self.parameters),
            animal=self._animal
        )

    def set_parameters(self, parameters):
        ''' Allows one to set the parameters given a reduced (no metadata) form:

        >>> parameters = {ME: 8, CP: 50, ...} # reduced (no metadata) form
        >>> feed = feed.set_parameters(parameters)

        '''
        for k,v in parameters.items():
            self.parameters[k]['value'] = v

    ###########################################################################
    # region - interface
    @property
    def _animal(self):
        return self._animal_

    @_animal.setter
    def _animal(self, value):
        self._animal_ = value

    @property
    def requirement_ME(self):
        ''' The ME-requirement that should be satisfied by the feed.
        
        This can be used to match supply and demand. '''
        if self._animal is not None:
            return self._animal.ME_required
        else: 
            return self._requirement_ME

    @requirement_ME.setter
    def requirement_ME(self, value):
        if self._animal is not None:
            raise AttributeError("Attribute should not be set when object is associated with an animal.")
        self._requirement_ME = value

    @property
    def requirement_MP(self):
        ''' The MP-requirement that should be satisfied by the feed.
        
        This can be used to match supply and demand. '''
        if self._animal is not None:
            return self._animal.MP_required
        else: 
            return self._requirement_MP

    @requirement_MP.setter
    def requirement_MP(self, value):
        if self._animal is not None:
            raise AttributeError("Attribute should not be set when object is associated with an animal.")
        self._requirement_MP = value


    @property
    def month_of_year(self):
        ''' If the user has specified that certain feed quality parameters are
            variable throughout the year this variable is used to extract the
            right value for each of the variable parameters based on the number
            of months that have elapsed since the start of the simulation.
            
            Since indexing in Python is zero-based we subtract 1 from the num-
            ber of months that have passed in the simulation.
        '''        
        if self._animal is not None:
            return (self._animal.months_in_simulation - 1) % 12
        else:
            return self._month_of_year

    @month_of_year.setter
    def month_of_year(self, value):
        self._month_of_year = value

    # endregion - interface
    ###########################################################################

    @property
    def N(self):
        ''' Nitrogen content of the feed (g/kg DM).
        '''
        return self.protein_to_N(self.CP)

    @property
    def r(self):
        ''' Rumen digesta fractional outflow rate per hour (1/hr), depends on
            the cow.

            See [1] page 11, eq. 25:
            "Retention time is highly correlated with the level of feeding.
            [...] values in the range 0.02 to 0.08/hour [...] of total rumen
            content would leave the rumen each hour. ARC (1984) gave estimates
            [...] as follows

            1. Animals fed at low level of feeding, about 1 x maintenance: 
               0.02 / h
            2. Calves, low yielding dairy cows [...] less than 2x maintance:
               0.05 / h
            3. High yielding dairy cows (15kg milk / day), greater than 2x
               maintance: 0.08 / h

            Although no relationship between level of feeding (L) and retention
            time (r) has been established experimentally, AFRC (1992) proposed
            [...] as follows:"

                r = -0.024 + 0.179*(1-exp(-.0278*L))
        '''
        res = -0.024 + 0.179 * (1 - np.exp(-0.278 * self.L))

        # 0.02 <= r <= 0.08
        r = 0.02 if res < 0.02 else 0.08 if res > 0.08 else res

        return r

    @property
    def y(self):
        ''' The Microbial Yield (g MCP/MJ FME).

        The protein yield as produced by the microbes as an emperical function
        of the level of feeding (L).

        Rationale (!?): High (low) L; much (little) feed passes through
        the digestive system, microbial yield is (is not) relatively limited by
        crude protein availability.

        See [1] page 16, eq. 34. :

        L = 1 --> y = 9g MCP/MJ FME .
        L = 3 --> y = 11g MCP/MJ FME . '''

        return float(7.0 + 6.0 * (1 - np.exp(-0.35 * self.L)))

    @property
    def L(self):
        ''' Level of feeding (1), cow relative.

            Actually the specific cow and diet determine the cows r.
        '''
        if self._animal is None:
            return self._L
        else:
            return self._animal.level_of_feeding

    @property
    def QDP(self):
        ''' Fraction of Quickly Digestible Protein (DM g/kg) in the feed.

            The fraction of protein in the feed that is quickly digested in the
            rumen (near water-soluble).
        '''
        return self.a * self.CP

    @property
    def RDP(self):
        ''' Fraction of Rumen Digestible Protein (DM g/kg) in the feed.

            Simply the sum of quickly and slowly digestible protein in the 
            feed.
        '''
        return min(self.CP, self.QDP + self.SDP)

    @property
    def UDP(self):
        ''' Fraction of Undegradable Protein (DM g/kg) in the feed.
        '''
        res = self.CP - self.RDP

        return max(0, res)

    @property
    def SDP(self):
        ''' Slowly Degradable Protein: the fraction of crude protein in the
            feed, other than QDP, that is degraded in the rumen (g SDP / 
            kg DM).

            The proportionality constant relating SDP to total crude protein
            content of the feed (here called 'alpha') describes which fraction
            of the non-water soluble rumen degradable protein (b) becomes part
            of the SDP as a function of the relative degradation rate of feed 
            N (c) and the rumen outflow rate (r) using a rectangular hyperbola.
        '''
        b = self.b
        c = self.c
        r = self.r

        alpha = b * (c / (c + r))

        return alpha * self.CP

    @property
    def DUP(self):
        ''' The fraction of Digestible Undegraded Protein (DM g/kg) in the
            feed.
        '''
        res = 0.9 * self.UDP - 6.25 * self.ADIN

        return max(0, res)

    @property
    def ERDP(self):
        ''' The fraction of Effective Rumen Degradable Protein (DM g/kg) in the
            feed.

            An upper limit on the Microbial Crude Protein; the effective amount
            of protein in the rumen which can be, at most, digested. 
        '''
        return 0.8 * self.QDP + self.SDP

    @property
    def MCP(self):
        ''' The fraction of Microbial Crude Protein (g/kg DM) in the feed.

        The protein the microbes can get out of the feed. Depends on the FME 
        available for this process. Typically the availability of FME will
        limit microbial protein degradation in feeds high in protein, whereas
        in protein-poor feeds the fraction of erdp itself will limit microbial
        protein degradation.
        '''
        return min(self.y * self.FME, self.ERDP)

    @property
    def MTP(self):
        ''' The fraction of Microbial True Protein (g/kg DM) in the feed.

            The proteins the ruminant could get out of the MCP (amino-acids).
            See [1] page 18: Is estimated to be 70--80% of the MCP, 75% is used
            as a best estimate. 
        '''
        return 0.75 * self.MCP

    @property
    def DMTP(self):
        ''' The fraction of Digestible Microbial True Protein (g/kg DM) in the
            feed.

            The proteins the ruminant is expected to get out of the MCP (amino-
            acids). See [1] page 18: Estimated to be a constant 85% of the MTP. 
        '''
        return 0.85 * self.MTP

    @property
    def MP(self):
        ''' The fraction of Metabolisable Protein (g MP / kg DM) in the feed.
        '''
        return self.DMTP + self.DUP

    @property
    def Q_m(self):
        ''' The ME to GE ratio of the feed (Q_m); a measure of the metabolisibility.

        See [1] page 2 eq. 4. '''
        if self.GE == 0:
            raise ValueError
        else:
            return self.ME/self.GE

    @property
    def faecal_N(self):
        ''' The associated faecal N concentration (g/kg).

        NOTE: is DMTP correct in this calculation?

        See [1] page 20, the flow diagram. '''

        return self.protein_to_N((0.1*self.UDP + 0.25*self.MCP + 0.15*self.DMTP)) + self.ADIN

    @property
    def faecal_P(self):
        ''' The associated faecal P concentration (g/kg).

        ISSUE: The value of 0.5 as the fraction of ingested P that ends up in
        manure does not correspond to the numbers given in the cited source.

        SOLUTION: The reported value of 0.63 is used instead.

        See [3] page 131-132 Table 9.4. '''
        
        return 0.63 * 1000 * self.P

    @property
    def faecal_K(self):
        ''' The associated faecal K concentration (g/kg).

        Of the total amount of potassium in the feed about 2.5% is excreted
        through faeces.        

        See [3] page 133-4 Table 9.5. '''
        
        return 0.025 * 1000 * self.K

    @property
    def faecal_DM(self):
        ''' The associated faecal DM concentration (kg/kg).

        Here DMD is a measure of the digestibility of the dry-matter
        in the feed (g/kg).

        See [2] (Marianas Thesis) page 237 eq. 7. '''
        
        return 1 - 0.001 * self.DMD

    def get_portion(self, nportions):
        ''' Returns one out of n equal portions of the feed.
        '''
        o = self.__copy__()

        return o.set_amount(o.amount / nportions)

    def split(self, amounts=None):
        ''' Divide self in a number of portions of amounts "amounts".

            Example: Consider 3 kg of feed and amounts a list of amounts
            (kg DM) e.g. [1, 1, 1]. Calling this method divides 3 kg of feed
            into three times 1 kg of feed.

            feed = RuminantFeed(amount=3,**feed_quality)
            feed1, feed2, feed3 = feed.split([1,1,1])
        '''

        total_amount = sum(amounts)

        if amounts is None or total_amount != self.amount:
            raise ValueError('Pass an appropriate list of floats "amounts", see docstring.')

        to_return = []

        for amount in amounts:

            a_copy = self.__copy__()
            a_copy.amount = amount

            to_return.append(a_copy)

        return to_return

    ###########################################################################
    # region - totals

    @property
    def ME_total(self):
        ''' ME (MJ). '''

        return self.ME * self.amount

    @property
    def GE_total(self):
        ''' GE (MJ). ''' 
        return self.GE * self.amount

    @property
    def MP_total(self):
        ''' MP (g) .'''

        return self.MP * self.amount
    
    @property
    def dry_matter_intake_ME(self):
        ''' The amount of feed an animal would have to eat to satisfy the
        specified ME-requirement, given the composition of the current feed.
        
        The feed may be either a single feed-type (e.g. napier grass), or a
        composite feed as calculated by the mix()-function from the feed-
        module. '''

        return self.requirement_ME / self.ME

    @property
    def dry_matter_intake_MP(self):
        ''' The amount of feed an animal would have to eat to satisfy the
        specified MP-requirement, given the composition of the current feed.
        
        The feed may be either a single feed-type (e.g. napier grass), or a
        composite feed as calculated by the mix()-function from the feed-
        module. '''

        return self.requirement_MP / self.MP

    # endregion - totals
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - Parameters
 
    @property
    def is_concentrate(self):
        return self.parameters['kind_of_feed']['value'] == 'concentrate'

    @property
    def is_roughage(self):
        return self.parameters['kind_of_feed']['value'] == 'roughage'

    @property
    def ADIN(self):
        _val = self.parameters['ADIN']['value']

        if isinstance(_val, list):
            if len(_val) == 1:
                return _val[0]
            else:
                return _val[self.month_of_year]
        else:
            return _val

    @property
    def CP(self):        
        _val = self.parameters['CP']['value']

        if isinstance(_val, list):
            if len(_val) == 1:
                return _val[0]
            else:
                return _val[self.month_of_year]
        else:
            return _val

    @property
    def DM(self):
        _val = self.parameters['DM']['value']

        if isinstance(_val, list):
            if len(_val) == 1:
                return _val[0]
            else:
                return _val[self.month_of_year]
        else:
            return _val

    @property
    def DMD(self):
        _val = self.parameters['DMD']['value']

        if isinstance(_val, list):
            if len(_val) == 1:
                return _val[0]
            else:
                return _val[self.month_of_year]
        else:
            return _val

    @property
    def FME(self):
        _val = self.parameters['FME']['value']

        if isinstance(_val, list):
            if len(_val) == 1:
                return _val[0]
            else:
                return _val[self.month_of_year]
        else:
            return _val

    @property
    def GE(self):
        ''' Gross Energy content of the feed (MJ / kg DM). '''
        _val = self.parameters['GE']['value']

        if isinstance(_val, list):
            if len(_val) == 1:
                return _val[0]
            else:
                return _val[self.month_of_year]
        else:
            return _val

    @property
    def K(self):
        ''' Potassium content of the feed (g / kg DM). '''
        _val = self.parameters['K']['value']

        if isinstance(_val, list):
            if len(_val) == 1:
                return _val[0]
            else:
                return _val[self.month_of_year]
        else:
            return _val

    @property
    def ME(self):
        _val = self.parameters['ME']['value']

        if isinstance(_val, list):
            if len(_val) == 1:
                return _val[0]
            else:                
                return _val[self.month_of_year]
        else:
            return _val

    @property
    def NDF(self):
        _val = self.parameters['NDF']['value']

        if isinstance(_val, list):
            if len(_val) == 1:
                return _val[0]
            else:
                return _val[self.month_of_year]
        else:
            return _val

    @property
    def P(self):
        _val = self.parameters['P']['value']

        if isinstance(_val, list):
            if len(_val) == 1:
                return _val[0]
            else:
                return _val[self.month_of_year]
        else:
            return _val

    @property
    def a(self):
        ''' Proportionality constant (-) relating feed total crude protein
        content (g CP / kg DM) to quickly degradable protein content
        (g QDP / kg DM). '''
        _val = self.parameters['a']['value']

        if isinstance(_val, list):
            if len(_val) == 1:
                return _val[0]
            else:
                return _val[self.month_of_year]
        else:
            return _val

    @property
    def b(self):
        _val = self.parameters['b']['value']

        if isinstance(_val, list):
            if len(_val) == 1:
                return _val[0]
            else:
                return _val[self.month_of_year]
        else:
            return _val

    @property
    def c(self):
        _val = self.parameters['c']['value']

        if isinstance(_val, list):
            if len(_val) == 1:
                return _val[0]
            else:
                return _val[self.month_of_year]
        else:
            return _val

    # endregion

###################
# Utility functions
###################
def check_parameters(feed):
    for key in feed.parameters   :
        if key == 'metadata':
            continue
        
        _var = feed.parameters[key]['value']
    
        if isinstance(_var, list):
            if len(_var) not in (1, 12):
                raise ValueError(f"List should contain either 1 or 12 values; check {key}")


def mix(feeds, animal=None):
    ''' Mix multiple feeds.

    Returns feed with the weighted mean feed quality and appropriate amount.
    '''

    all_feeds = [x for x in feeds if x is not None]
    feeds = [feed for feed in all_feeds if feed.amount > 0]

    kind_of_feed = set([feed.parameters['kind_of_feed']['value'] for feed in all_feeds])
    
    if len(all_feeds) == 0:
        return None

    if len(all_feeds) == 1 or len(feeds) == 0:
        # one feed, simple
        source = all_feeds[0]
        feed = source.__copy__()
        feed._animal = animal
        return feed
    
    if len(kind_of_feed) > 1:
        raise ValueError("More than one type of feed is being mixed, please separate roughages from concentrates.")

    mean_parameters = {}
    total_amount = sum([feed.amount for feed in feeds])

    for feed in feeds:
        rel_amount = feed.amount/total_amount
        parameters = feed.parameters

        non_numeric_parameters = ['metadata', 'kind_of_feed']
        parameters_ = [x for x in parameters if x not in non_numeric_parameters]

        for k in parameters_:
            if k in mean_parameters:
                value = parameters[k]['value']
                if isinstance(value, list):
                    value = value[feed.month_of_year]
                mean_parameters[k]['value'] += rel_amount * value
            else:
                value = parameters[k]['value']
                if isinstance(value, list):
                    value = value[feed.month_of_year]
                if isinstance(value, (float,int)):
                    mean_parameters[k] = {'unit': parameters[k]['unit'],
                                          'value': value}  # deepcopy(parameters[k])

                    mean_parameters[k]['value'] *= rel_amount

    mean_parameters['metadata'] = {}
    mean_parameters['kind_of_feed'] = {}
    mean_parameters['metadata']['info'] = 'Made by mixing feeds... TODO'
    mean_parameters['kind_of_feed']['value'] = kind_of_feed.pop()
        
    return Feed(amount=total_amount, parameters=mean_parameters, animal=animal)


class MilkSupply(object):
    ''' The supply of milk as feed for calves.

        dm: dry matter content of the milk
        me: metabolizable energy content of the milk (MJ / kg DM)
        cp: crude protein content of the milk (MJ / kg FW)
    '''
    def __init__(self, dm, me, cp):

        self.DM_content = dm
        self.ME_content = me
        self.CP_content = cp

        self._amount = 0

    def __copy__(self):
        ''' The copy method: returns a copy of this object by making a new
            identical one. 
        '''
        milk_supply = MilkSupply(dm=self.DM_content, me=self.ME_content, cp=self.CP_content)
        milk_supply.amount = self.amount

        return milk_supply
                    
    @staticmethod
    def protein_to_N(var):
        ''' Protein contains, on average, about 16% nitrogen. Amounts of pro-
            tein are, therefore, divided by 6.25 to convert them into amounts
            of nitrogen.
        '''
        return var / 6.25

    @property
    def amount(self):
        return self._amount

    @amount.setter
    def amount(self, value):
        self._amount = value

    @property
    def ME_total(self):
        return self.amount * self.DM_content * self.ME_content

    @property
    def MP_total(self):
        return self.amount * self.CP_content

    @property
    def N(self):
        return self.protein_to_N(self.CP_content)
