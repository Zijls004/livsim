from ...shared_components import FarmSimBaseClass


class CowManagement(FarmSimBaseClass):

    _name = 'CowManagement'

    units = {}

    def __init__(self, cow, parameters):

        super().__init__()

        self._cow = cow

        self._next_in_line = False
        self._to_be_replaced = False

        self._threshold_lactations = parameters['female_threshold_lactations']['value']
        self._threshold_period_open = parameters['female_threshold_open']['value']
        self._threshold_age = parameters['female_threshold_age']['value']

    @property
    def cow(self):
        ''' Returns a reference to the cow-object managed here.
        '''
        return self._cow

    @property
    def next_in_line(self):
        ''' Boolean indicating whether this animal has been flagged for 
            replacement, but kept in the herd to finish current gestation
            and/or lactation.
        '''
        return self._next_in_line

    @next_in_line.setter
    def next_in_line(self, value):
        self._next_in_line = value
        self.cow.can_conceive_given_management = 0

    @property
    def to_be_replaced(self):
        ''' Boolean indicating whether this animal will be taken from the herd
            the next time the herd is updated.
        '''
        return self._to_be_replaced

    ############################
    # Herd management parameters
    ############################
    @property
    def threshold_age(self):
        return self._threshold_age

    @property
    def threshold_lactations(self):
        return self._threshold_lactations

    @property
    def threshold_period_open(self):
        return self._threshold_period_open

    ########################
    # Animal characteristics
    ########################
    @property
    def age(self):
        return self.cow.age

    @property
    def nr_of_lactations(self):
        return self.cow.lactation_cycle_number

    @property
    def period_open(self):
        return self.cow.months_after_calving

    @property
    def _is_gestating(self):
        return self.cow.is_gestating

    @property
    def _is_lactating(self):
        return self.cow.is_lactating

    @property
    def _lactation_has_stopped(self):
        ''' Boolean indicating whether lactation has finished. '''
        return self.cow.lactation_has_stopped

    @property
    def gestation_period(self):
        return self.cow.parameters['gestation_duration']['value']

    ####################
    # Decision variables
    ####################
    @property
    def not_handled_yet(self):
        return not self.next_in_line and not self.to_be_replaced

    @property
    def old(self):
        return self.age >= self.threshold_age

    @property
    def used_up(self):
        return self.nr_of_lactations >= self.threshold_lactations

    @property
    def unproductive(self):
        # period_open is None when animal has not given birth yet
        if self.period_open is not None:
            return self.period_open >= self.threshold_period_open
        else:
            return False

    @property
    def keep_past_retirement_age(self):
        ''' An animal may meet the set criteria for replacement, but if said
        animal is still productive (i.e. in gestation or lactation) at the
        moment a management decision has to be made, replacement of said 
        animal is deferred to the time at which the animal has completed its
        swan song.
        '''
        return self.meets_criteria_for_replacement and not self.is_eligible_for_replacement

    @property
    def almost_used_up(self):
        return self.nr_of_lactations >= (self.threshold_lactations - 1) and self._is_lactating

    @property
    def meets_criteria_for_replacement(self):
        ''' Determine whether an animal can, potentially, be replaced if a
        new calf is born on farm, based on a set of criteria. 

        Currently the criteria for replacement are:
        - a set maximum age for animals in the herd
        - a set maximum number of lactations for animals in the herd
        - a set maximum period open for animals in the herd

        If any of these threshold values are met, or exceeded, an animal is
        deemed eligible for replacement.
        '''
        reasons_for_replacement = [self.old, self.used_up, self.unproductive]
        if any(reasons_for_replacement) and self.not_handled_yet:
            return True
        else:
            return False

    @property
    def is_eligible_for_replacement(self):
        ''' Any animal that is currently in gestation or lactation is kept in
        the herd, even if that animal is eligible for replacement on the basis
        of the criteria specified in the herd management.
        '''
        if self._is_gestating or self._is_lactating:
            return False
        else:
            return self.meets_criteria_for_replacement

    @property
    def is_almost_eligible_for_replacement(self):
        ''' At the time a calf is born some animals in the herd may not meet
        the criteria for replacement just yet.
        '''
        reasons_for_future_replacement = [
            self.almost_used_up, self.keep_past_retirement_age
        ]
        if any(reasons_for_future_replacement) and self.not_handled_yet:
            return True
        else:
            return False

    def mark_as_to_be_replaced(self):
        ''' Method to switch from next_in_line to to_be_replaced.

        If an animal has been flagged for future replacement herd management
        will wait for parturition and/or lactation to finish before removing
        the animal from the herd.
        '''
        if self.next_in_line and not self._is_gestating and self._lactation_has_stopped:
            self._to_be_replaced = True

    def update(self):

        self.mark_as_to_be_replaced()


class BullManagement(FarmSimBaseClass):

    _name = 'BullManagement'

    units = {}

    def __init__(self, bull, parameters):

        super().__init__()

        self._bull = bull

        self._to_be_replaced = False

        self._threshold_age = parameters['male_threshold_age']['value']

    @property
    def bull(self):
        ''' Returns a reference to the cow-object managed here. '''
        return self._bull

    ############################
    # Herd management parameters
    ############################
    @property
    def threshold_age(self):
        return self._threshold_age

    ####################
    # Decision variables
    ####################
    @property
    def to_be_replaced(self):
        ''' Boolean indicating whether this animal will be taken from the herd the
        next time the herd is updated. '''
        return self._to_be_replaced

    @property
    def is_eligible_for_replacement(self):
        ''' Determine whether an animal can, potentially, be replaced if a
        new calf is born on farm. '''
        if self.bull.age >= self.threshold_age:
            return True
        else:
            return False

    def update(self):

        pass
