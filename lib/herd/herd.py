
from copy import deepcopy
from statistics import mean
import yaml

import numpy as np
import pandas as pd

from ...shared_components import FarmSimBaseClass

from ..feed import Feed
from ..feed import mix

from ..animal import Cow
from ..animal import Bull

from .management import CowManagement
from .management import BullManagement


class Herd(FarmSimBaseClass):
    """The Herd-class simulates the dynamics of a herd of animals.
    
    :param FarmSimBaseClass: [description]
    :type FarmSimBaseClass: [type]
    :raises ValueError: Calves in the model need to be either male or female.
    :raises ValueError: At any one time the status of either the animals or the
                        herd should be updated.
    """
    default_parameters = yaml.load('''
    max_nfemales:
        value: 4
        unit: '-'
        info: 'Imposed maximum number of female animals to have in the herd at any time.'
        source: 'User defined.'
    max_nmales:
        value: 1
        unit: '-'
        info: 'Imposed maximum number of female animals to have in the herd at any time.'
        source: 'User defined.'
    male_threshold_age:
        value: 96
        unit: 'months'
        info: 'Age above which a bull is replaced by a bull calf.'
        source: 'User defined.'
    female_threshold_lactations:
        value: 4
        unit: 'months'
        info: 'Number of lactations above which a cow is replaced by a cow calf.'
        source: 'User defined.'
    female_threshold_age:
        value: 96
        unit: 'months'
        info: 'Age above which a cow is replaced by a cow calf.'
        source: 'User defined.'
    female_threshold_open:
        value: 36
        unit: 'months'
        info: 'Maximum period a cow is allowed to remain open, before it is replaced.'
        source: 'User defined.'
    ''', Loader=yaml.SafeLoader)

    _name = 'Herd'
    _version = '1.5-w-04.09.2018'

    units = {}

    def __init__(self, parameters=None, animals=None, farm=None, grazing_rate_monthly=0,
                 grazing_feed=None):

        super().__init__()
        
        if parameters is None:
            self.parameters = deepcopy(self.default_parameters)
        else:
            self.parameters = parameters

        self.max_id = 0

        self._animal_management = {}

        if animals is None:
            self._animals_ = []
        else:
            self._animals_ = animals
            
            self.create_animal_management()

            for animal in self._animals:
                animal._herd = self

        self._farm = farm

        self.feed_supply = None
        self._is_bull_present = True

        self.grazing_rate_monthly = grazing_rate_monthly

        self.grazing_feed = grazing_feed

        self.number_of_cows_born = 0
        self.number_of_bulls_born = 0

        self._number_of_calves_born = 0
        self._number_of_calves_sold = 0
        self._number_of_cattle_sold = 0
        self._number_of_animals_died = 0
        self._bws_of_animals_sold = 0

        self._months_in_simulation = 1

    def set_animals(self, animals: list):
        
        self._animals = animals

        self.create_animal_management()

    @property
    def _prefix(self):
        return f'{self._name}'

    @property
    def _farm(self):
        return self._farm_

    @_farm.setter
    def _farm(self, value):
        self._farm_ = value

    @property
    def months_in_simulation(self):
        ''' Returns the number of months that have elapsed in the simulation. '''
        return self._months_in_simulation

    # region - herd characteristics

    @property
    def _animals(self):
        return self._animals_

    @_animals.setter
    def _animals(self, values):

        for animal in values:
            self.max_id += 1
            animal._id = self.max_id

        self._animals_ = values

    @property
    def nanimals(self):
        return len(self._animals)

    @property
    def ncows(self):
        return len(self._females)

    @property
    def nbulls(self):
        return len(self._males)

    @property
    def max_nanimals(self):
        return self.parameters['max_nfemales']['value'] + self.parameters['max_nmales']['value']

    @property
    def highest_id(self):
        if self._animals:
            return max([x._id for x in self._animals])
        else:
            return None

    @property
    def _females(self):
        return [x for x in self._animals if x._sex == 'f']

    @property
    def _males(self):
        return [x for x in self._animals if x._sex == 'm']

    @property
    def updated_animals(self):
        """Returns a list of animals that have been through at least one 
        update-cycle.
            
        Herd level output can only be based on the characteristics of animals
        that have been through at least one full update-cycle. Animals that 
        have been added to the herd in the current timestep have not been 
        updated yet and, therefore, do not yet feature all required 
        information.

        :return: List of all animals that have been updated.
        :rtype: list
        """
        return [a for a in self._animals if a.has_been_updated]

    @property
    def is_bull_present(self):

        if self._is_bull_present is None:

            for animal in self._animals:
                if animal._sex == 'm':
                    return True
            return False

        else:
            return self._is_bull_present

    def set_presence_bull(self):
        for animal in self._animals:
            animal.is_bull_present = self.is_bull_present

    # endregion - herd characteristics
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - herd level output
    
    @property
    def mean_condition_index(self):
        N = self.nanimals
        return sum([x.condition_index/N for x in self.updated_animals])

    @property
    def mean_age(self):
        N = self.nanimals
        return sum([x.age/N for x in self.updated_animals])

    @property
    def total_milk_yield(self):
        return sum([x.milk_yield for x in self._females])
    
    @property
    def total_faecal_DM(self):
        ''' (kg/month). '''
        return sum([x.growth_and_production.faecal_DM for x in self.updated_animals])

    @property
    def total_faecal_C(self):
        ''' (kg/month). '''
        return sum([x.growth_and_production.faecal_C for x in self.updated_animals])

    @property
    def total_faecal_N(self):
        ''' (kg/month). '''
        return 0.001*sum([x.growth_and_production.faecal_N for x in self.updated_animals])

    @property
    def total_faecal_P(self):
        ''' (kg/month). '''
        return 0.001*sum([x.growth_and_production.faecal_P for x in self.updated_animals])

    @property
    def total_faecal_K(self):
        ''' (kg/month). '''
        return 0.001*sum([x.growth_and_production.faecal_K for x in self.updated_animals])

    @property
    def total_faecal_DM_CNPK(self):
        ''' (kg/month). '''
        return np.array([
            self.total_faecal_DM,
            self.total_faecal_C,
            self.total_faecal_N,
            self.total_faecal_P,
            self.total_faecal_K]
        )

    @property
    def total_urinary_N(self):
        ''' (kg/month). '''
        return 0.001*sum([x.growth_and_production.urinary_N for x in self.updated_animals])

    @property
    def total_urinary_P(self):
        ''' (kg/month). '''
        return 0.001*sum([x.growth_and_production.urinary_P for x in self.updated_animals])

    @property
    def total_urinary_K(self):
        ''' (kg/month). '''
        return 0.001*sum([x.growth_and_production.urinary_K for x in self.updated_animals])

    @property
    def total_urinary_NPK(self):
        ''' (kg/month). '''
        return np.array([self.total_urinary_N,
                         self.total_urinary_P,
                         self.total_urinary_K])

    @property
    def number_of_calves_born(self):
        return self._number_of_calves_born

    @property
    def number_of_calves_sold(self):
        return self._number_of_calves_sold

    @property
    def number_of_animals_died(self):
        return self._number_of_animals_died

    @property
    def number_of_cattle_sold(self):
        return self._number_of_cattle_sold

    @property
    def bw_of_animals_sold(self):
        return self._bws_of_animals_sold

    @property
    def total_roughage_offered(self):
        return sum([x.supply_roughage.amount if x.supply_roughage is not None else 0 for x in self.updated_animals])

    @property
    def total_intake_ME(self):
        return sum([x.intake_feed_ME for x in self.updated_animals])

    @property
    def total_intake_roughage(self):
        return sum([x.feed_intake.intake_roughage_DM for x in self.updated_animals])

    @property
    def total_intake_roughage_N(self):
        return sum([c.feed_intake.intake_roughage_N for c in self.updated_animals])

    @property
    def total_intake_external_roughage_N(self):
        return sum([c.feed_intake.intake_external_roughage_N for c in self.updated_animals])

    @property
    def total_intake_concentrate(self):
        return sum([x.feed_intake.intake_concentrate_DM for x in self.updated_animals])

    @property
    def total_intake_concentrate_N(self):
        return sum([x.feed_intake.intake_concentrate_N for x in self.updated_animals])

    @property
    def total_intake_feed_N(self):
        return self.total_intake_roughage_N + self.total_intake_concentrate_N

    @property
    def total_intake_external_feed_N(self):
        return self.total_intake_external_roughage_N + self.total_intake_concentrate_N

    # endregion - herd level output
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - feed related

    @property
    def feed_supply(self):
        if self._farm is None:
            return self._feed_supply
        else:
            return self._farm.feed_storage

    @feed_supply.setter
    def feed_supply(self, value):
        self._feed_supply = value
        self.couple_feed_to_herd()

    def couple_feed_to_herd(self):
        if self.feed_supply:
            self.feed_supply.herd = self

    def set_supply_roughage(self):

        if self.grazing_feed:
            grazed = self.grazing_feed.set_amount(self.grazing_rate_monthly)
        else:
            grazed = None

        for animal in self._animals:

            if self.feed_supply:
                fed = self.feed_supply.roughage_to_give(animal)
            else:
                fed = None

            available = mix([grazed, fed])
            animal.supply_roughage = available

    def set_supply_concentrate(self):

        for animal in self._animals:

            if self.feed_supply:
                fed = self.feed_supply.concentrate_to_give(animal)
            else:
                fed = None

            available = fed
            animal.supply_concentrate = available

    def set_feed_supply(self):
        self.set_supply_roughage()
        self.set_supply_concentrate()

    # endregion - feed related
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - herd management

    @property
    def animal_management(self):
        ''' The animal_management-dictionary contains the management objects
            for all animals in the herd. These objects handle replacement
            decisions. 
        '''
        return self._animal_management

    def create_animal_management(self):
        ''' Creates the dictionary containing the management objects for all
            animals in the herd.
        '''
        self._animal_management = {}

        for animal in self._animals:
            self.add_to_animal_management(animal)

    def add_to_animal_management(self, animal):
        ''' Add a single animal to the herd management.
        '''
        self.animal_management[str(animal.id)] = self.create_animal_management_object(
            animal, self.parameters)

    @staticmethod
    def create_animal_management_object(animal, parameters):
        ''' Creates the actual animal management object based on the sex of
            the new animal.
        '''
        if animal._sex == 'f':
            return CowManagement(animal, parameters)
        elif animal._sex == 'm':
            return BullManagement(animal, parameters)

    def _get_eligible_females(self):
        ''' Get the cows in the herd that meet one or more of the criteria for
            replacement with a new-born calf.
        '''
        return [x for x in self._females if self.animal_management[str(x.id)].is_eligible_for_replacement]

    def _get_eligible_males(self):
        ''' Get the bulls in the herd that meet one or more of the criteria for
            replacement with a new-born calf.
        '''
        return [x for x in self._males if self.animal_management[str(x.id)].is_eligible_for_replacement]

    def _get_almost_eligible_females(self):
        ''' Get the cows in the herd that have some unfinished business, but
            that are ready to be replaced after they've completed their
            business.
        '''
        return [x for x in self._females if self.animal_management[str(x.id)].is_almost_eligible_for_replacement]

    def _get_calves(self):
        ''' Returns a list of the calves that are born in the current time
            step.

            For each of these calves herd management will decide whether they are
            added to the herd or sold off.
        '''
        calves = [x.get_calf() for x in self._females if x.has_given_birth]

        for animal in calves:
            self.max_id += 1
            animal._id = self.max_id

        return calves

    def try_add_animal(self, calf):

        self._number_of_calves_born += 1

        if calf._sex == 'f':
            self.number_of_cows_born += 1
            self.try_add_female(calf)
        elif calf._sex == 'm':
            self.number_of_bulls_born += 1
            self.try_add_male(calf)
        else:
            raise ValueError("Calf is neither female, nor male.")

    def try_add_male(self, calf):

        nmales = len(self._males)
        max_nmales = self.parameters['max_nmales']['value']

        if max_nmales == 0:
            # reject calf
            self._number_of_calves_sold += 1
            return

        elif nmales >= max_nmales:
            # accept/reject new calf?
            candidates = self._get_eligible_males()

            if len(candidates) == 0:
                self._number_of_calves_sold += 1
                return

            candidates_sorted = sorted(candidates, key=lambda x: x.age, reverse=True)
            candidate = candidates_sorted[0]

            # reject old boy, accept new boy
            self.animal_management[str(candidate.id)]._to_be_replaced = True
            self.add_animal_to_herd(calf)
        else:
            # accept new calf
            self.add_animal_to_herd(calf)

    def try_add_female(self, calf):
        ''' Whether or not a female calf is kept in the herd depends on the
            herd management parameters specified by the user:

            - max_nfemales: if the maximum allowed number of cows in the herd
              has not been reached yet, the calf will be kept
            - age
            - lactations 
        '''
        nfemales = len(self._females)
        max_nfemales = self.parameters['max_nfemales']['value']

        if max_nfemales == 0:
            # reject calf
            self._number_of_calves_sold += 1
            return

        elif nfemales >= max_nfemales:
            # accept/reject new calf?

            candidates = self._get_eligible_females()

            if len(candidates) == 0:
                candidates = self._get_almost_eligible_females()

                if len(candidates) == 0:
                    self._number_of_calves_sold += 1
                    return

                candidates_sorted = sorted(candidates, key=lambda x: x.age, reverse=True)
                candidate = candidates_sorted[0]
                self.animal_management[str(candidate.id)].next_in_line = True

                self.add_animal_to_herd(calf)
            else:
                candidates_sorted = sorted(
                    candidates, key=lambda x: x.age, reverse=True)
                candidate = candidates_sorted[0]
                self.animal_management[str(
                    candidate.id)]._to_be_replaced = True

                self.add_animal_to_herd(calf)
        else:
            # accept new calf
            self.add_animal_to_herd(calf)

    def add_animal_to_herd(self, animal):
        """ Adds an animal to the herd and initiates a management object for
        the current animal.
        
        :param animal: Object representing the animal to add to the herd.
        :type animal: Cow, Bull
        """
        animal._herd = self
        self._animals.append(animal)
        self.add_to_animal_management(animal)

    def take_animals_from_herd(self):
        """ Take animals that are marked as ready for replacement or that have
        come to a regrettable and untimely demise from the herd.    
        """
        for animal in self._animals:
            replace_animal = self.animal_management[str(
                animal.id)].to_be_replaced

            if replace_animal or animal.will_die:

                if replace_animal:
                    self._number_of_cattle_sold += 1
                    self._bws_of_animals_sold += animal.body_weight

                elif animal.will_die:
                    self._number_of_animals_died += 1

                animal.has_been_replaced = True
                self._animals.remove(animal)

    # endregion - herd management
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - dynamic

    def reset_herd_counters(self):
        self._number_of_calves_born = 0
        self._number_of_calves_sold = 0

    def reset_animal_counters(self):
        self._number_of_animals_died = 0
        self._number_of_cattle_sold = 0
        self._bws_of_animals_sold = 0

    def update(self):
        """Updates the status of the animals in the herd and of the herd 
        itself.
        
        :return: None is returned if there are no animals to update.
        :rtype: NoneType
        """
        self.reset_animal_counters()
        self.reset_herd_counters()

        # Remove animals from the herd that are to be replaced
        self.take_animals_from_herd()

        # No animals in the herd: nothing to do
        if self.nanimals == 0:
            return None

        # Add free grazing to roughage supply if applicable
        self.set_feed_supply()
        self.set_presence_bull()

        for animal in self._animals:
            if not animal.will_die:
                animal.update()

        new_calves = self._get_calves()

        for calf in new_calves:
            self.try_add_animal(calf)

        for animal in self._animals:
            self.animal_management[str(animal.id)].update()

    def run(self, months=120):

        res = {}
        animal_ids = []

        for i in range(months):

            self.update()

            for a in self.updated_animals:
                if a.id not in animal_ids:
                    animal_ids.append(a.id)

            res = self.to_dict(obj=self, d=res, time_step=i, no_of_timesteps=months)

            self._months_in_simulation += 1

            if self._farm is None:
                self.feed_supply.update()

        df = pd.DataFrame(res)

        res['animal_ids'] = animal_ids

        return res, df

    # endregion - dynamic
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
