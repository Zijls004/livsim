from os import path

# To build the right relative file path we need to add the folder containing
# the different parts of the NUANCES-FARMSIM model to the python-path.
current_folder = path.dirname(path.abspath(__file__))

examples_folder = path.dirname(current_folder)

field_folder = path.dirname(examples_folder)

nuances_folder = path.dirname(field_folder)