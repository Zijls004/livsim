{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### __The Jupyter-notebook__\n",
    "\n",
    "To facilitate the use of, and communication about, LIVSIM example applications of the model are provided in the shape of Jupyter notebooks. A Jupyter-notebook is a document that combines live code, narrative text, equations, and visualizations in a series of cells."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\">\n",
    "    To run a cell, simply select that cell and hit either <b>shift+enter</b> or <b>ctrl+enter</b>.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import display, HTML\n",
    "\n",
    "# With these two lines of code the display width can be set as a percentage\n",
    "# of the total width of the screen.\n",
    "width = 65\n",
    "display(HTML(f\"<style>.container {{ width:{width}% !important; }} </style>\"))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%load_ext autoreload\n",
    "%autoreload 2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### **Setting the scene**\n",
    "\n",
    "Before we can simulate a cow we should make sure we import the model components we need."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys\n",
    "\n",
    "# We need to append the folder containing the model components to the \n",
    "# search path before we can import anything from it.\n",
    "sys.path.append('../../..')\n",
    "from livsim import Cow\n",
    "from livsim import Bull\n",
    "from livsim import FeedStorage\n",
    "\n",
    "# To make the murky waters of LIVSIM slightly less treacherous to navigate\n",
    "# a number of simple functions have been provided to aid the user.\n",
    "from helpers import get_list_of_breeds\n",
    "from helpers import get_list_of_feeds\n",
    "from helpers import list_of_animal_vars\n",
    "from helpers import get_full_var_name\n",
    "from helpers import display_function_parameters\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### __Some formatting of the plots__\n",
    "To make the plots a bit easier to read we set some of the parameters for the plots in this workbook to user defined values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.rcParams.update({\n",
    "    'font.size': 16,\n",
    "    'legend.fontsize': 10,\n",
    "    'legend.handlelength': 2\n",
    "})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### __Introduction to the current version of LIVSIM__\n",
    "\n",
    "In the current version of LIVSIM every element in a simulation is described by a class. Each class describes what that element looks like and what it can do. Most of the classes that form the codebase for LIVSIM are not meant to be used directly by the end-user. These classes are used by the limited number of classes that the user interacts with to build and run a simulation. The classes a user is likely to use are:\n",
    "\n",
    "- `Cow` / `Bull`\n",
    "- `FeedStorage`\n",
    "- `Herd`\n",
    "\n",
    "In this part of the tutorial we will only focus on simulating a single animal. Creating and running a simulation with a herd is the topic for a separate tutorial.\n",
    "\n",
    "\n",
    "#### __Doing some shopping__\n",
    "Before we can build our simulation we need to know which ingredients we have at our disposal. To get a quick overview of the cattle breeds and the types of feed that are currently available in our breed,- and feed-libraries we can use two of the functions we imported earlier.\n",
    "\n",
    "The function `get_list_of_breeds` gives a list of all breeds of cattle available."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "get_list_of_breeds()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To get a list of available kinds of feed we can use the function `get_list_of_feeds`. The list printed by this function distinguishes between pasture grasses, forages, and concentrates. \n",
    "\n",
    "Pasture grasses and forages are both roughages and are treated in the same way. The concentrates, however, are concentrates (nomen est omen...). In LIVSIM an animal will eat all concentrates it receives, unless this is more than a fixed fraction (0.4) of the total dry matter intake. Roughages are either consumed untill energy and protein requirements are fulfilled or up to a maximum amount depending on body weight of the animal and the dry matter digestibility of the feed according to an emperical relationship by [Conrad _et al._ (1964)](https://doi.org/10.3168/jds.S0022-0302(64)88581-7):\n",
    "\n",
    "$$\n",
    "\\begin{align}\n",
    "I_p &= \\frac{0.0107 \\cdot \\mathrm{BW}}{(1-\\mathrm{DMD})}\n",
    "\\end{align}\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "get_list_of_feeds()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "How we can create animals using the sets of breed parameters available and how to do the catering will be explained in the following cells."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### __Cow / Bull__\n",
    "\n",
    "The `Cow`- and `Bull`-classes describe a cow and bull, respectively. Whenever we wish to simulate an animal we should start by instantiating an object of either class Cow, or class Bull. The following example shows how to create an animal of breed 'Holstein x Friesian', that is 2 years (i.e. 24 months) old, and weighs 400 kgs.\n",
    "\n",
    "``` Python\n",
    "cow_1 = Cow(breed='Friesian x Holstein', age=24, bw=400)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Enter code here to create cow_1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this example we've named the object that we created 'cow_1'. If we wish to know anything about the state of the object we can access the object attributes (e.g. the body weight of the animal) in the following way:\n",
    "\n",
    "``` Python\n",
    "cow_1.body_weight\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we would like to give our animal some company we need to create a new object using a different name.\n",
    "\n",
    "``` Python\n",
    "cow_2 = Cow(breed='Friesian x Holstein', age=24, bw=400)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Enter code here to create cow_2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class='alert alert-warning'>\n",
    "    Note that, even though the second animal we created is identical to the first, we cannot create the second animal by simply stating: <code>cow_2 = cow_1</code>. This statement does not create a new object that is a copy of the first, it simply gives an additional name to the already existing object.\n",
    "</div>  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Making our animal exhibit certain of its behaviours is not unlike accessing an attribute. Functions defined as part of a class are called methods and are called by typing the name of the method followed by parentheses:\n",
    "```\n",
    "cow_1.sing_and_dance()\n",
    "```\n",
    "Any arguments that the method might require should be entered between the parentheses.\n",
    "\n",
    "<div class='alert alert-warning'>\n",
    "    Please note that the <code>sing_and_dance</code>-method has not yet been implemented in the current version of LIVSIM and is not on the roadmap for any future release.\n",
    "</div> "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### __FeedStorage__\n",
    "\n",
    "The `FeedStorage`-class describes an object that is used to store information on feed management and that handles distribution of feed to an individual animal or over the herd. A new, empty, object of type `FeedStorage` is created in the following way:\n",
    "\n",
    "```Python\n",
    "feed_storage = FeedStorage()\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Enter code here to create feed_storage"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### __Adding feed to the feed storage__\n",
    "The two methods a LIVSIM-user is most likely to interact with are `add_external_roughage` and `add_external_concentrate`. These methods add either an amount of roughage, or an amount of concentrate to the feed storage.\n",
    "\n",
    "<div class=\"alert alert-info\">\n",
    "    <p> \n",
    "        If we would like to learn, or remind ourselves, how a method should be called we \n",
    "        can use one of the provided helper-functions: <code>display_function_parameters</code>. \n",
    "        This function prints a list of the parameters, in the order in which those parameters\n",
    "        should be provided with arguments, of any function or method we provide it with .\n",
    "    </p>\n",
    "    <br>\n",
    "    <p> \n",
    "        To get the parameters of the method <code>add_external_roughage</code> of the class\n",
    "        <code>FeedStorage</code>, for example, we type:\n",
    "        <br>\n",
    "        <span style=\"padding-left: 2em;\">\n",
    "            <code>display_function_parameters(FeedStorage.add_external_roughage)</code>\n",
    "        </span>\n",
    "    </p>\n",
    "    <br>\n",
    "    <p>\n",
    "        The table displayed by <code>display_function_parameters</code> also indicates the\n",
    "        default value and expected data type of each parameter when applicable. \n",
    "    </p>\n",
    "    <p>\n",
    "        If a parameter has been defined with a default value providing the function with an argument\n",
    "        for this parameter is optional. If the user chooses not to give an argument for said\n",
    "        parameter the function will use the default value. \n",
    "    </p>\n",
    "    <p>\n",
    "        The expected data type of the argument to a parameter is given as a so-called 'type hint'.\n",
    "        Providing an argument with a different data type will not generate an error, but it can\n",
    "        lead to unexpected behaviour. If an expected data type is part of the function definition,\n",
    "        therefore, the user is strongly advised to provide arguments of the correct type.\n",
    "    </p>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Enter code here to print the list of arguments for 'add_external_roughage'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### __Adding roughages__\n",
    "To give a fixed amount of a given roughage to the animal, irrespective of the animals' life stage, one can use the following code:\n",
    "\n",
    "```Python\n",
    "feed_storage.add_external_roughage(3, 'maize stover')\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Enter code here to add roughage to the feed storage"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this case we do not provide an argument for the parameter `life_stage` so the default value `default` is used. The amount of feed should be entered in kg fresh weight day<sup>-1</sup>. If we run the simulation with these settings, the animal will be fed 3 kgs of maize stover per day throughout the entire simulation.\n",
    "\n",
    "When the availability of a type of feed is not constant throughout the year the user may enter specific amounts for each month of the year; in this case the amounts should be entered as a list containing 12 elements.\n",
    "\n",
    "```Python\n",
    "external_grass = [8, 8, 6, 5, 4, 3, 3, 4, 5, 6, 7, 8]\n",
    "feed_storage.add_external_roughage(external_grass, 'Heteropogon contortus')\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Enter code here to add more roughage to the feed storage"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once an animal is offered more than one kind of roughage the feed actually consumed by the animal will be a mixture of the individual kinds of feed offered. The composition of that mixture will be a weighted average of that of the constituent components."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### __Adding concentrates__\n",
    "Some feeds are too precious to waste on just any old cow. To specify certain types of feed (e.g. concentrates) that should only be given to animals during certain periods of their life we can pass a value to the optional parameter `life_stage` of the `add_external_[roughage/concentrate]`-methods:\n",
    "\n",
    "```Python\n",
    "feed_storage.add_external_concentrate(1, 'concentrate 1', 'calf')\n",
    "feed_storage.add_external_concentrate(2, 'concentrate 1', 'gestating')\n",
    "feed_storage.add_external_concentrate(2, 'concentrate 1', 'lactating')\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Enter code here to add concentrates for only certain life stages"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These lines add an aptly named concentrate ('concentrate 1') to the diets of animals that are considered calves (i.e. animals that are at a pre-weaning age) and of animals that are in gestation or are lactating. \n",
    "\n",
    "Animals that are simultaneously both in gestation _and_ lactating can be given an extra special diet:\n",
    "\n",
    "```Python\n",
    "feed_storage.add_external_concentrate(2.5, 'concentrate 1', 'lactating and gestating')\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Enter code here to add the extra special diet to the feed storage"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If no such diet is defined the model will first check whether a rule exists for animals that are lactating, then it will look for a rule for animals that are in gestation, and finally it will default to 'default'.\n",
    "\n",
    "<div class=\"alert alert-info\">\n",
    "    <p>\n",
    "        To get an overview of the life stages distinguished at the moment in the model, \n",
    "        one can ask for help on the <i>get_life_stage_of_animal</i>-method of the FeedStorage-class.\n",
    "    </p>\n",
    "    <p>\n",
    "        <span style=\"padding-left: 2em;\">\n",
    "            <code>?FeedStorage.get_life_stage_of_animal</code>\n",
    "        </span>\n",
    "    </p>    \n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Enter code here to ask for help on the get_life_stage_of_animal-method"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### **Creating a cow and feeding it**\n",
    "After the right classes have been imported we create a __Cow__-object using a specific set of breed parameters ( ___Boran x Friesian___ ). \n",
    "\n",
    "To manage the feeding regime for this animal we create a __FeedStorage__-object and add roughage and concentrate to it. As can be seen we define the feeding regime for the roughage as 'default', this means that all animals will receive the specified amount of that particular kind of roughage, unless they fall into another one of the specified categories and a feeding regime for that category has been defined. The amount of a type of feed offered can be given as a single number, or as a list of 12 numbers. In the first case the same amount of feed will be offerend in each month, whereas in the latter case a different amount will be offered in each month.\n",
    "\n",
    "In this example animals that fall into the 'default' category do not receive concentrates. The concentrates are reserved for animals that are calves or that are in gestation or lactation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a cow\n",
    "cow = Cow(breed='Boran x Friesian')\n",
    "\n",
    "cow._MAX_ITER = 20\n",
    "\n",
    "# Create empty feed storage\n",
    "feed_storage = FeedStorage()\n",
    "\n",
    "# Feed sources from off-farm: here an amount of grass to feed to the animals is specified \n",
    "# for each month of the year.\n",
    "external_grass = [10, 10, 7, 7, 5, 5, 3, 5, 5, 7, 7, 10]\n",
    "feed_storage.add_external_roughage(external_grass, 'Heteropogon contortus', 'default')\n",
    "\n",
    "# Some additional concentrates to keep our cows from dying on the diet of only grass...\n",
    "feed_storage.add_external_concentrate(1, 'concentrate 1', 'calf')\n",
    "feed_storage.add_external_concentrate(2, 'concentrate 1', 'gestating')\n",
    "feed_storage.add_external_concentrate(2, 'concentrate 1', 'lactating')\n",
    "\n",
    "# Finally the feed storage should be associated with our cow-object to make interactions \n",
    "# between them possible.\n",
    "cow.feed_supply = feed_storage"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### **Running the model**\n",
    "\n",
    "To run the model we simply call the run-method on our cow-object. The argument provided to the run-method is the duration of the simulation in months. The run-method returns the results both as a Pandas DataFrame (`df`) and as a dictionary (`res`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "res, df = cow.run(120)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### __Inspecting the results - 1__\n",
    "\n",
    "The quickest way to get a glimpse of the results from the simulation is to ask for the so-called head of the DataFrame. This is done by simply calling the method called `head` on the DataFrame-object called `df`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### **Inspecting the results - 2**\n",
    "\n",
    "To get an overview of all variables available in the DataFrame we can use the list()-function and pass the DataFrame as argument.\n",
    "\n",
    "In the resulting list the variable names are preceeded by a prefix. This prefix indicates from which part of the cow-object the information is taken:\n",
    "\n",
    "1. __Cow__: the actual Cow-object itself, this forms the nervous center for our simulation\n",
    "2. __FI__: the FeedIntake-object, this object handles intake of milk, concentrates, and roughage\n",
    "3. __GrPr__: the GrowthProduction-object, growth, weight loss and milk production are calculated here\n",
    "4. __ReqAFRC__: the RequirementsAFRC-object, in LIVSIM we use the AFRC-system to calculate the requirements of livestock in terms of energy and protein; this object knows all about that\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "list(df)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### __Inspecting the results - 3__\n",
    "\n",
    "To make finding a specific variable, or remembering the exact name of a variable, slightly easier a custom made function called `list_of_animal_vars` is provided. This function accepts the DataFrame as its argument and print an alphabetically sorted list of all variables stored in the DataFrame, omitting the prefix of the objects."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "list_of_animal_vars(df)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once we've found the variable we're looking for, or reacquainted ourselves with the FARMSIM nomenclature, we can ask for the full variable name (including the prefix) using another custom function: `get_full_var_name`. This full variable name is needed if we want to extract information from DataFrame. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "get_full_var_name(df, 'body_weight_change')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### __Inspecting the results - 4__\n",
    "\n",
    "Once we know how to find the information we want it is very simple to make some quick visualizations of the results. Whenever we extract a single variable from a DataFrame a so-called `Series`-object is generated. A Series-object is essentially a DataFrame that contains only a single variable (in fact, a DataFrame may be regarded as a collection of Series-objects). Both the DataFrame `df`, and any Series-objects we may extract from it, have a `plot`-method, making it very simple to make some quick graphs.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df['Cow_body_weight_change'].plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### __Inspecting the results - 5__\n",
    "\n",
    "Using the package `matplotlib` it is very easy to make slightly more sophisticated graphs of our results."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create empty figure and get a referene to the currect axis.\n",
    "fig = plt.figure()\n",
    "ax = fig.gca()\n",
    "\n",
    "# Plot the results and direct the output to the current axis of the\n",
    "# empty figure.\n",
    "df['Cow_body_weight_change'].plot(ax=ax)\n",
    "\n",
    "# Set axis labels and create a grid.\n",
    "ax.set_xlabel('time (months)')\n",
    "ax.set_ylabel('$\\Delta$ body weight (kg / month)')\n",
    "ax.grid()\n",
    "\n",
    "# Show the plot.\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### __Inspecting the results - 6__\n",
    "\n",
    "Once we're a bit more familiar with `Python` and `matplotlib` it becomes quite easy to make far more intricate plots, showing a lot more information in a single plot."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get data on the gestation feasibility curve\n",
    "bw = df['Cow_body_weight']\n",
    "age = df['Cow_age']\n",
    "max_bw = df['Cow_maximum_body_weight']\n",
    "min_bw = df['Cow_minimum_body_weight']\n",
    "feas_bw = df['Cow_minimum_body_weight_reproduction']\n",
    "\n",
    "min_bw_repr = [i if i <= j else None for i, j in zip(feas_bw, max_bw)]\n",
    "\n",
    "# Create plot\n",
    "f, axes = plt.subplots(1, 2, figsize=(12, 6))\n",
    "\n",
    "# Min and max body weight\n",
    "max_bw.plot(ax=axes[0], \n",
    "            color='green', \n",
    "            linestyle='--', \n",
    "            alpha=0.85, \n",
    "            label='max body weight')\n",
    "min_bw.plot(ax=axes[0], \n",
    "            color='red', \n",
    "            linestyle='--', \n",
    "            alpha=0.85, \n",
    "            label='min body weight')\n",
    "\n",
    "# Plot gestation feasibility curve\n",
    "axes[0].plot(age, \n",
    "             min_bw_repr, \n",
    "             linestyle='--', \n",
    "             alpha=0.85, \n",
    "             color='darkorange', \n",
    "             label='feasible set for reproduction')\n",
    "\n",
    "bw.plot(ax=axes[0],\n",
    "        label='body weight')\n",
    "\n",
    "# Compensatory gain\n",
    "df['Cow_compensatory_gain'].plot(ax=axes[1], \n",
    "                                 alpha=0.85, \n",
    "                                 linestyle='--', \n",
    "                                 label='compensatory gain')\n",
    "\n",
    "df['Cow_body_weight_change'].plot(ax=axes[1],\n",
    "                                  label='$\\Delta$ body weight')\n",
    "\n",
    "# Set limits on the y-axis and create horizontal line to indicate x-axis at y=0\n",
    "axes[1].set_ylim([1.1*min(df['Cow_body_weight_change']), \n",
    "                  1.1*max(df['Cow_compensatory_gain'])])\n",
    "axes[1].axhline(linewidth=2, color='k')\n",
    "\n",
    "# Set labels\n",
    "axes[0].set_ylabel('Body Weight (kg)')\n",
    "axes[1].set_ylabel('Body Weight Change $\\mathrm{(kg \\; month^{-1}})$')\n",
    "\n",
    "for ax in axes:\n",
    "    ax.set_xlabel('Age (months)')\n",
    "    ax.grid()\n",
    "    ax.legend()\n",
    "\n",
    "plt.tight_layout()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### __Dumping results to Excel__\n",
    "If one insits on using Excel to get an overview of the results, the following cell demonstrates how to write the DataFrame to an Excel-file. Dumping to a csv-file works in much the same way, except that one should use the `to_csv`-method and specify `.csv` as the file extension."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.to_excel('some results.xlsx')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\">\n",
    "    Occassionally it happens that the Excel-file, or csv-file, is written to a location that may not be immediately obvious to the casual observed. In such cases that following code can be used to ask the system what working directory it is using, and, therefore, where the data went.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "\n",
    "\n",
    "os.getcwd()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.16"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
