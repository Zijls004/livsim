from statistics import mean

from ...shared_components import FarmSimBaseClass


class GrowthProductionCattle(FarmSimBaseClass):

    _name = 'GrPr'
    _attrs_to_keep = []

    def __init__(self, animal):

        super().__init__()
        
        self._animal = animal

        self.reset()

    @property
    def _prefix(self):
        if self._animal._herd is not None:
            return f'{self._name}_{self._animal.id}'
        else:
            return f'{self._name}'

    @property
    def _animal(self):
        return self._animal_

    @_animal.setter
    def _animal(self, value):
        self._animal_ = value

    # region - interface with animal-object

    @property
    def _will_grow(self):
        return self._animal.will_grow

    @property
    def _dead(self):
        return self._animal.dead

    @property
    def _intake_feed_ME(self):
        return self._animal.intake_feed_ME

    @property
    def _intake_feed_MP(self):
        return self._animal.intake_feed_MP

    def get_body_weight_estimate(self, change_estimate):
        ''' An estimate for the updated body weight at a time half-way the
            current month.

            This value is used to calculate the nutritive requirements of
            the animal. Using the fully updated value would result in an
            overestimation of the requirements.
        '''
        f = self.bw_change_modifier
        return self._animal.body_weight + 0.5 * f * change_estimate

    # endregion - interface with animal-object

    @property
    def ME_supply(self):
        """ The supply of ME to the animal is equal to the ME extracted from
        the feed if the animal is growing. If the animal loses weight the ME
        mobilized from body tissues is added to the supply from feed. 

        :return: The supply of ME to the animal
        :rtype: float
        """
        if self._will_grow:
            return self._intake_feed_ME
        else:
            return self._ME_supply

    @property
    def MP_supply(self):
        if self._will_grow:
            return self._intake_feed_MP
        else:
            return self._MP_supply

    @property
    def _ME_required(self):
        return self._ME_required_

    @property
    def _MP_required(self):
        return self._MP_required_

    @property
    def ME_supply_weight_loss(self):
        return self._ME_supply_weight_loss

    @property
    def MP_supply_weight_loss(self):
        return self._MP_supply_weight_loss

    @staticmethod
    def protein_to_N(var):
        ''' Protein contains, on average, about 16% nitrogen. Amounts of pro-
            tein are, therefore, divided by 6.25 to convert them into amounts
            of nitrogen.
        '''
        return var / 6.25

    def reset(self):
        ''' The values for body weight gain and weight loss are reset to None
            at the start of each timestep. This triggers these values to be re-
            calculated the first time these properties are requested.
         '''
        self._ME_supply_weight_loss = 0
        self._MP_supply_weight_loss = 0

        self._ME_required_ = 0
        self._MP_required_ = 0

        self._body_weight_estimate = self._animal.body_weight
        self._body_weight_change_estimate = 0

        self._potential_body_weight_gain_set = False
        self._actual_body_weight_gain_set = False
        self._actual_body_weight_loss_set = False

    @property
    def bw_change_modifier(self):
        ''' This modifier will switch between positive and negative body weight
            change, depending on whether the animal is currently growing or
            losing weight.
        '''
        if self._will_grow:
            res = 1
        else:
            res = -1

        return res

    @property
    def body_weight_estimate(self):
        return self._body_weight_estimate

    @property
    def body_weight_change_estimate(self):
        return self._body_weight_change_estimate

    @property
    def _body_weight_change(self):
        ''' The body weight change of the animal in the current month (kg /
        month).

        Whether this change means growth or weight loss is indicated by the
        associated animal-object. In the case of growth a further distinction
        is made between potential and actual growth.

        At the start of each update-cycle the values for actual weight gain and
        weight loss are reset to False. This triggers the current object to
        to recalculate and set the values for weight gain or loss. Once
        these values have been set the calculated values are returned. This
        prevents unnecessary calls to the computationally expensive binary
        search algorithms that calculate the actual weight gain and weight
        loss.
        '''
        if self._animal.production_level == 'potential':

            if not self._potential_body_weight_gain_set:

                self._potential_body_weight_gain_set = True
                
                bw_gain, bw_estimate = self.get_potential_body_weight_gain()

                self._potential_body_weight_gain = bw_gain
                
                self.set_requirements(bw=bw_estimate, bw_change=bw_gain)

            return self._body_weight_change_estimate

        elif self._animal.production_level == 'actual':

            if self._will_grow:

                if not self._actual_body_weight_gain_set:

                    self._actual_body_weight_gain_set = True

                    bw_gain_estimate, bw_estimate = self.get_body_weight_gain()
                    
                    self.update_supply_and_requirements(bw_estimate=bw_estimate, 
                                                        bw_change_estimate=bw_gain_estimate)

                return self._body_weight_change_estimate

            else:

                if not self._actual_body_weight_loss_set:

                    self._actual_body_weight_loss_set = True

                    bw_loss_estimate, bw_estimate = self.get_body_weight_loss()

                    self.update_supply_and_requirements(bw_estimate=bw_estimate, 
                                                        bw_change_estimate=bw_loss_estimate,
                                                        bw_loss=True)

                return self._body_weight_change_estimate

        else:
            raise ValueError('value for production_level unknown or not set')

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - potential production

    @property
    def potential_body_weight_gain(self):
        return self._potential_body_weight_gain

    def get_potential_body_weight_gain(self):
        ''' The potential body weight gain of the animal is determined by the
            metabolisability of the feed on the one hand (compensatory gain)
            and the distance to the potential growth curve on the other hand.
        '''
        bw = self._animal.body_weight
        max_weight = self._animal.maximum_body_weight

        comp_gain = self._animal.compensatory_gain

        if (bw + comp_gain) > max_weight:
            bw_gain = max_weight - bw
        else:
            bw_gain = comp_gain

        bw_estimate = self.get_body_weight_estimate(bw_gain)

        return bw_gain, bw_estimate

    # endregion - potential production
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - actual body weight change
    
    def get_body_weight_loss(self):
        ''' The body weight loss (kg/month): >= 0.

            Involves finding the weight gain which
            balances the ME/MP supply and demand.
        '''        
        weight_loss_max = self._animal.maximum_body_weight_loss * self._animal.body_weight
        weight_loss_min = 0

        # binary search the right weight loss
        for i in range(self._animal._MAX_ITER):
            # kg/month
            bw_loss_estimate = mean([weight_loss_max, weight_loss_min])
            bw_estimate = self.get_body_weight_estimate(bw_loss_estimate)

            self.update_supply_and_requirements(bw_estimate=bw_estimate,
                                                bw_change_estimate=bw_loss_estimate,
                                                bw_loss=True)

            res = self.check_balance()

            if res == 'shortage':  # continue search in upper half of the domain
                weight_loss_min = bw_loss_estimate
            elif res == 'abundance':  # continue search in lower half of the domain
                weight_loss_max = bw_loss_estimate
            elif res == 'balance':
                break
            else:
                raise ValueError('No proper balance could be found between supply and requirement.')

        res = self.check_balance()
        if res == 'shortage':
            self._animal._cant_lose_enough_weight = True

        return bw_loss_estimate, bw_estimate

    def get_body_weight_gain(self):
        ''' The body weight gain estimate (kg/month).

        Involves finding the weight gain which
        balances the ME/MP supply and demand.
        '''
        weight_gain_max = self.potential_body_weight_gain
        weight_gain_min = 0

        # binary search the right weight gain
        for i in range(self._animal._MAX_ITER):

            # kg/month
            bw_gain_estimate = mean([weight_gain_max, weight_gain_min])
            bw_estimate = self.get_body_weight_estimate(bw_gain_estimate)

            self.update_supply_and_requirements(bw_estimate=bw_estimate,
                                                bw_change_estimate=bw_gain_estimate,
                                                bw_loss=False)

            res = self.check_balance()

            if res == 'abundance':  # continue search in upper half of the domain
                weight_gain_min = bw_gain_estimate
            elif res == 'shortage':  # continue search in lower half of the domain
                weight_gain_max = bw_gain_estimate
            elif res == 'balance':
                break
            else:
                raise ValueError('No proper balance could be found between supply and requirement.')

        return bw_gain_estimate, bw_estimate

    def check_balance(self):
        ''' This method checks whether ME and MP are available in abundance,
        or whether there is a shortage of ME and/or MP.

        The behaviour triggered by the return value of this method depends on
        the value returned:
            - 'abundance': there is room for more growth or less weight loss is
                needed 
            - 'shortage': growth should be reduced or more weight needs to be
                lost
            - 'balance': supply and requirements are in balance and the right
                value for growth or weight loss has been found  
        '''
        tolerance_ME = self._animal._TOLERANCE * self._ME_required
        tolerance_MP = self._animal._TOLERANCE * self._MP_required

        diff_ME = self.ME_supply - self._ME_required
        diff_MP = self.MP_supply - self._MP_required

        abundant_ME = self.ME_supply > self._ME_required
        abundant_MP = self.MP_supply > self._MP_required

        balance_ME = tolerance_ME >= diff_ME >= 0
        balance_MP = tolerance_MP >= diff_MP >= 0

        # balance_ME = abs(self._ME_required - self.ME_supply) <= tolerance_ME
        # balance_MP = abs(self._MP_required - self.MP_supply) <= tolerance_MP

        shortage_ME = self.ME_supply < self._ME_required
        shortage_MP = self.MP_supply < self._MP_required

        if (balance_ME and abundant_MP) or (balance_MP and abundant_ME):
            return 'balance'
        else:
            if abundant_ME and abundant_MP:
                return 'abundance'
            elif shortage_ME or shortage_MP:
                return 'shortage'
            else:
                if self._dead:
                    return 'shortage'
                else:
                    raise ValueError('Unpredicted result from check_balance()')

    def update_supply_and_requirements(self, bw_estimate, bw_change_estimate, bw_loss=False):
        ''' This method gets updated values for the supply of ME and MP to the
        animal and the nutrititive requirements of the animal, based on the
        estimated values for body weight loss and body weight.

        If the animal loses weight the energy and protein mobilized from body 
        tissues is added to the supply from feed.
        '''
        self._animal.reset_requirements()

        if bw_loss:
            self.set_supply(change=bw_change_estimate)        
        self.set_requirements(bw=bw_estimate, bw_change=bw_change_estimate)

    def set_supply(self, change):
        ''' When the animal loses weight, the energy and protein mobilized from
        body tissues are added to the total supply of energy and protein from 
        the feed.
        '''
        intake_feed_ME = self._animal.intake_feed_ME
        intake_feed_MP = self._animal.intake_feed_MP

        ME_supply_weight_loss = self._animal.energy_content_tissue * change
        MP_supply_weight_loss = self._animal.protein_content_tissue * change

        self._ME_supply_weight_loss = ME_supply_weight_loss
        self._MP_supply_weight_loss = MP_supply_weight_loss

        self._ME_supply = intake_feed_ME + ME_supply_weight_loss
        self._MP_supply = intake_feed_MP + MP_supply_weight_loss

    def set_requirements(self, bw, bw_change):
        ''' Get the updated requirements based on the estimated new values for 
        the body weight and body weight_gain.
        ''' 
        f = self.bw_change_modifier

        self._body_weight_estimate = bw
        self._body_weight_change_estimate = bw_change * f

        self._ME_required_ = self._animal.ME_required
        self._MP_required_ = self._animal.MP_required

    # endregion - actual body weight change
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # region - production of urine and faeces

    @property
    def N_mobilized_weight_loss(self):
        ''' Amount of N freed by weight loss (g/month).

            Nitrogen, in the form of amino acids, may be mobilized if the
            animal loses weight. In these calculations a value of 138 g of
            protein per kg of body weight is used - see 'MP_value_weight_loss'.

            See [1] page 40, equation 115
        '''
        if self._animal.will_grow:
            res = 0
        else:
            f = self.bw_change_modifier
            res = self._animal.protein_content_tissue * self._body_weight_change  * f

        return self.protein_to_N(res)

    @property
    def faecal_DM(self):
        ''' Manure dry-matter production (kg/month).

            See the associated Feed class for more information.
        '''
        sources = [
            self._animal.feed_concentrate,
            self._animal.feed_roughage
        ]

        return sum([x.amount * x.faecal_DM for x in sources if x is not None])

    @property
    def faecal_C(self):
        ''' Manure C production (kg/month).

            See the associated Feed class for more information.
        '''
        return self._animal.parameters['miscellaneous']['manure_C_fraction']['value'] * self.faecal_DM

    @property
    def faecal_N(self):
        ''' Manure N production (g/month).

            See [1] page 20, the flow diagram and the associated Feed class for
            more information.
        '''
        sources = [
            self._animal.feed_concentrate,
            self._animal.feed_roughage
        ]

        return sum([x.amount * x.faecal_N for x in sources if x is not None])

    @property
    def faecal_P(self):
        ''' Manure P production (g/month).

            Reference
            ---------
            Efde, 1996 page 131
        '''
        sources = [
            self._animal.feed_concentrate,
            self._animal.feed_roughage
        ]

        return sum([x.amount * x.faecal_P for x in sources if x is not None])

    @property
    def faecal_K(self):
        ''' Manure N production (g/month).

            Reference
            ---------
            Efde, 1996 page 133-4 Table 9.5
        '''
        sources = [
            self._animal.feed_concentrate,
            self._animal.feed_roughage
        ]

        return sum([x.amount * x.faecal_K for x in sources if x is not None])

    @property
    def urinary_N(self):
        ''' Urinary N excretion rate (g/month)

            We use a mass balance:

            N_urine = N_feed + N_weight_loss - N_calf - N_milk - N_faeces
        '''
        N_in_total = self._animal.feed_intake_N + self.N_mobilized_weight_loss
        N_out_non_urine = self._animal.sex_specific_N_excretion + self.faecal_N

        N_out_urine = N_in_total - N_out_non_urine

        try:
            assert N_out_urine >= 0
        except:
            if self._animal.production_level == 'actual':
                raise ValueError("We're leaking nitrogen again!")
        return N_out_urine

    @property
    def urinary_P(self):
        ''' Urinary P excretion rate (g/month).

        NOTE: Taking into account losses of P through urine is a deviation from
        the AFRC system, it is in accordance with ARC 1980.

        See [3] page 130.
        '''
        # note: this parameter 2 might be nice to put as an actual parameter value
        return 2.0 * 10**-6 * self._animal.body_weight

    @property
    def urinary_K(self):
        ''' Urinary K excretion rate (g/month).

        See [3] page 133-4 Table 9.5.
        '''
        return self._animal.feed_intake_K * 0.95

    # endregion - production of urine and faeces


class GrowthProductionCow(GrowthProductionCattle):

    def __init__(self, animal):

        super().__init__(animal)

    def get_body_weight_loss(self):
        ''' Body weight loss for a cow differs from that of a bull in that more
        processes, that require energy and protein, may be active. If an animal
        has a shortage of ME and/or MP some of these processes may be sacrificed
        to meet maintenance requirements.
        '''
        bw_loss_estimate, bw_estimate = super().get_body_weight_loss()

        if bw_loss_estimate > self._animal.threshold_lactation:

            self._animal._milk_yield = 0

            bw_loss_estimate, bw_estimate = super().get_body_weight_loss()
            
            if bw_loss_estimate > self._animal.threshold_lactation:
                self._animal.stop_lactation()
            else:
                bw_loss_estimate = self._animal.threshold_lactation
                bw_estimate = self.get_body_weight_estimate(bw_loss_estimate)

                self.update_supply_and_requirements(bw_estimate, bw_loss_estimate)
                self._animal._milk_yield = self.reduce_lactation()

            self.update_supply_and_requirements(bw_estimate, bw_loss_estimate)
            
            # check balance between intake and requirements
            res = self.check_balance()
            
        return bw_loss_estimate, bw_estimate

    def reduce_lactation(self):
        ''' If weight loss exceeds a threshold value the animal will reduce, or
            stop, lactation in an attempt to meet maintenance requirements with
            the given diet.

            When some ME and MP is still available after maintenance require-
            ments have been met, this method will calculate how much milk can
            still be produced using the surplus over maintenance requirements.
        '''
        abundance_ME = self.ME_supply - self._ME_required
        abundance_MP = self.MP_supply - self._MP_required

        milk_ME = self._animal.milk_energy_content
        milk_MP = self._animal.milk_crude_protein_content

        k_ME = self._animal._efficiency_ME_lactation
        k_MP = self._animal._efficiency_MP_lactation

        milk_yield_ME = abundance_ME / (milk_ME / k_ME)
        milk_yield_MP = abundance_MP / (milk_MP / k_MP)

        return min(milk_yield_ME, milk_yield_MP)


class GrowthProductionBull(GrowthProductionCattle):

    def __init__(self, animal):

        super().__init__(animal)
