"""The helpers-module contains a number of functions that may assist the user
in creating, running, and analyzing LIVSIM-simulations in Jupyter-notebooks.
"""
import sys
import os
import re
import inspect

import yaml

from matplotlib import pyplot as plt

from IPython.core.display import display
from IPython.core.display import HTML

path = os.path.abspath(__file__)
notebook_dir = os.path.dirname(path)
example_dir = os.path.dirname(notebook_dir)
main_dir = os.path.dirname(example_dir)


def get_list_of_feeds():
    """This function prints the names of the breeds currently available in the
    breed library.
    """
    library_path = os.path.join(main_dir, 'data', 'feed_library.yaml')

    with open(library_path) as f:
        feed_data = yaml.load(f, Loader=yaml.SafeLoader)
    
    categories = [i for i in feed_data.keys() if i != 'cr_to_feed']
    for c in categories:
        feeds = list(feed_data[c].keys())
        
        print(f"Feeds available in categorie '{c}':")
        print("-------------------------------------------")
        print("\n".join((*feeds,)))
        print("")

def get_list_of_breeds():
    """This function prints the names of the breeds currently available in the
    breed library.
    """
    library_path = os.path.join(main_dir, 'data', 'breed_library.yaml')

    with open(library_path) as f:
        breed_library = yaml.load(f, Loader=yaml.SafeLoader)

    breed_names = list(breed_library.keys())

    print("List of breeds currently available:")
    print("-----------------------------------")
    print("\n".join((*breed_names,)))

def get_full_var_name(df, var_name: str) -> str:

    list_of_objects = ["Cow", "Bull", "FI", "GrPr", "ReqAFRC"]

    substring = "|".join(list_of_objects)

    string = f"^({substring})_{var_name}$"
    pattern = re.compile(string)

    var = [i for i in df if pattern.search(i)]

    if len(var) == 0:
        raise ValueError(f"Cannot find variable {var_name} in the DataFrame.")
    elif len(var) > 1:
        m = f"Variable {var_name} not uniquely defined; found: {var}."
        raise ValueError(m)
    else:
        return var[0]

def list_of_animal_vars(df, herd=False):
    """This functions inspects the dataframe [df] and returns an alphabetical
    list of all animal-related variables stored in the data-frame.
    
    The variable names in this list can be used in other functions. In the 
    function plot_animals_var(), e.g., the entries in the list can be used as
    the argument of the [var]-parameter.

    :param df: Data frame containing the results from a simulation. 
    :type df: DataFrame
    :return: An alphabetically sorted list of animal-related variables.
    :rtype: list
    """
    # The information stored for each cow is divided over several nested
    # objects.
    list_of_objects = ["Cow", "Bull", "FI", "GrPr", "ReqAFRC"]

    substring = "|".join(list_of_objects)
    if herd:
        string = f"^({substring})_([0-9]|[1-9][0-9])_"
    else:
        string = f"^({substring})_"
    pattern = re.compile(string)
    
    raw_list = [re.sub(string, '', i) for i in df if pattern.search(i)]
    set_list = list(set(raw_list))
    set_list.sort()
    
    print("\n".join(set_list))
    
def plot_animals_var(var: str, df, daily: bool=False):
    """ This function plots the data for the variable passed in as the function
    argument for all animals in the herd.
    
    :param var:
    :type var: str
    :param df:
    :type df: DataFrame
    :param daily: Should the data be plotted on a per day basis?
    :type daily: bool
    """
    fig = plt.figure()
    ax = fig.gca()

    # The information stored for each cow is divided over several nested
    # objects.
    list_of_objects = ["Cow", "Bull", "FI", "GrPr", "ReqAFRC"]

    substring = "|".join(list_of_objects)
    search_string = f"^({substring})_([0-9]|[1-9][0-9])_{var}$"
    pattern = re.compile(search_string)
    cs = [i for i in df if pattern.search(i)]

    if daily:
        df[cs].divide(30).plot(ax=ax)
        ylabel = f"{var} per day"
    else:
        df[cs].plot(ax=ax)
        ylabel = f"{var} per month"

    ax.set_xlabel("time (months)")
    ax.set_ylabel(ylabel)

    plt.show()

def get_animal_dict(df, animal_id: int):
    """This function extracts all information pertaining to the animal with the
    provided id from the Pandas DataFrame [df] and returns a new DataFrame with
    the requested information.

    :param df: DataFrame from which to extract information.
    :type df: DataFrame
    :param animal_id: Id of the animal for which to extract information.
    :type animal_id: int
    :return: DataFrame containing information on one particular animal.
    :rtype: DataFrame
    """
    # The information stored for each cow is divided over several nested
    # objects.
    list_of_objects = ["Cow", "Bull", "FI", "GrPr", "ReqAFRC"]

    substring = "|".join(list_of_objects)
    search_string = f"^({substring})_{animal_id}_"
    pattern = re.compile(search_string)
    cs = [i for i in df if pattern.search(i)]
    
    return df[cs]

def display_function_parameters(f):
    """This function displays a html-table showing the arguments of the 
    supplied function in the order in which the arguments should be supplied to
    the function. If the function has been defined with default values, or type 
    hints, for any of its parameters these are included in the table as well.

    :param f: The function for which we'd like to know the arguments.
    :type f: function
    """
    sig = inspect.signature(f)
    args = [p for p in sig.parameters if p != 'self']

    header = f"List of parameters for method: {f.__name__}"

    bg_colour = '#98D6F0'

    table = '<table cellpadding="10" style="font-family: Consolas">'
    table += f' \
        <tr> \
            <td style="text-align: center "colspan="3">{header}</td> \
        </tr> \
        <tr style="background-color: {bg_colour};"> \
            <th style="width: 100px; text-align: left">argument</th> \
                <th style="width: 100px; text-align: left">default value</th> \
                    <th style="width: 100px; text-align: left">data type</th> \
                        </tr>'
        
    for p in args:
        arg_txt = str(sig.parameters[p])
        
        txt_lst = arg_txt.split('=')
        
        arg_tmp = txt_lst[0]
        arg_lst = arg_tmp.split(':')
        arg_name = arg_lst[0]
        
        try:
            arg_type = arg_lst[1]
        except IndexError:
            arg_type = '-'
        
        try:
            def_val = txt_lst[1]
        except IndexError:
            def_val = '-'
        
        table += f' \
            <tr> \
                <td style="text-align: left; font-style: normal">{arg_name}</td> \
                    <td style="text-align: left">{def_val}</td> \
                        <td style="text-align: left">{arg_type}</td> \
                            </tr>'
            
    table += '</table>'
    display(HTML(table))

def list_of_available_functions():
    current_module = sys.modules[__name__]
    functions_list = inspect.getmembers(current_module, inspect.isfunction)

    print("List of functions currently available:")
    print("--------------------------------------")
    print("\n".join([f[0] for f in functions_list]))
    print("--------------------------------------")

if __name__ == '__main__':

    get_list_of_breeds()