from .cattle import Cow
from .cattle import Bull

from .requirements import RequirementsCowAFRC
from .requirements import RequirementsBullAFRC