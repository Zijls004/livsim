import sys

from .lib import Cow, Bull
from .lib import RequirementsCowAFRC 
from .lib import RequirementsBullAFRC
from .lib import Herd
from .lib import Feed
from .lib import mix
from .lib import FeedStorage

# These functions should only be imported if the code is not run from a .exe.
if not getattr(sys, 'frozen', False):
    from .lib import html_single_value_parameters
    from .lib import html_multi_value_parameters
    from .lib import flatten_parameter_dict
    from .lib import plot_parameters
    from .lib import plot_body_weight_curves