from matplotlib import pyplot as plt


def get_data(d: dict, i: int, var_name: str):
    ''' Simple function to return data from a nested dictionary. 
    
        parameters:
        -----------
        d       : dictionary
        i       : timestep
        var_name: name of the variable
    '''
    try:
        return d[i][var_name]
    except KeyError:
        return None

def extract_output(d: dict, var_name: str):
    ''' Returns a list of the specified variable from the provided nested
        dictionary. 

        To handle missing values in the list comprehension, this function uses
        the auxilliary function get_data().

        parameters:
        -----------
        d       : dictionary
        var_name: name of the variable
    '''
    return [get_data(d, i, var_name) for i in d]

def flatten_parameter_dict(d: dict) -> dict:    
    """ This function takes the nested dictionary containing the parameters
    for a specific breed and returns two new dictionaries: one containing the
    single value parameters and one containing the multi value parameters.

    :param d: dictionary containing the breed parameters
    :returns: flattened dictionaries containing either single or multi value 
              parameters
    :rtype: dict
    """
    res_single = {}
    res_multi = {}
    
    sub_list = list(d)
    
    for s in sub_list:
        
        for k in d[s]:
            
            if isinstance(d[s][k]['value'], list):
                res_multi[k] = d[s][k]
                
            else:
                res_single[k] = d[s][k]
            
    return res_single, res_multi

def display_single_value_parameters(parameters):
    
    lines = ['{:<50.50} {:<9} {:<12}'.format('Property', 'Value', 'Unit')]
    lines += [70 * '-']
        
    for key in parameters:

        unit = parameters[key]['unit']
        value = parameters[key]['value']

        if isinstance(value, int):
            lines += [f'{key:<50.50} {value:<9.0f} {unit:<12}']
        elif isinstance(value, float):
            lines += [f'{key:<50.50} {value:<9.4f} {unit:<12}']
        else:
            pass

    return '\n'.join(lines)

def display_multi_value_parameters(parameters):
    
    p = 'Property'
    v = 'Values'
    u = 'Unit'

    lines = [f'{p:<40.40} {v:<50} {u:<12}']
    lines += [102 * '-']
        
    for key in parameters:

        empty = ''        
        value_x = [i[0] for i in parameters[key]['value']]
        value_y = [i[1] for i in parameters[key]['value']]
        unit_x = parameters[key]['unit'][0]
        unit_y = parameters[key]['unit'][1]

        string_x = ''
        for i in value_x:
            if isinstance(i, int):
                string_x += f'{ i:<8.0f}'
            elif isinstance(i, float):
                string_x += f'{ i:<8.2f}'
        
        string_y = ''
        for i in value_y:
            if isinstance(i, int):
                string_y += f'{ i:<8.0f}'
            elif isinstance(i, float):
                string_y += f'{ i:<8.2f}'

        lines += [f'{key:<40.40} {string_x:<50} {unit_x:<12}']
        lines += [f'{empty:<40.40} {string_y:<50} {unit_y:<12}']
        lines += [102 * '-']

    return '\n'.join(lines)

def html_single_value_parameters(parameters):
    
    bg_colour = '#98D6F0'

    table = '<table cellpadding="10">'
    table += f' \
        <tr style="background-color: {bg_colour};"> \
            <th style="text-align: left">Property</th> \
                <th style="width: 75px; text-align: left">Values</th> \
                    <th style="width: 100px; text-align: left">Unit</th> \
                        <th style="width: 512px; text-align: left">Description</th> \
                            </tr>'

    var_nr = 0
    for key in parameters:

        if var_nr % 2 == 0:
            bg_colour = 'white'
        else:
            bg_colour = '#f2f2f2'

        value = parameters[key]['value']
        unit = parameters[key]['unit']
        info = parameters[key]['info']

        table += f' \
            <tr style="background-color:{bg_colour}"> \
                <td style="text-align: left">{key}</td> \
                    <td style="text-align: left">{value}</td> \
                        <td style="text-align: left">{unit}</td> \
                            <td style="text-align: left">{info}</td> \
                                </tr>'
        
        var_nr += 1

    table += '</table>'

    return table

def html_multi_value_parameters(parameters):
            
    data_cols = max([len(parameters[i]['value']) for i in list(parameters)])
    bg_colour = '#98D6F0'

    table = '<table cellpadding="10">'
    table += f' \
        <tr style="background-color: {bg_colour}"> \
            <th style="text-align: left">Property</th> \
                <th style="width: 128px; text-align: left">Variable</th> \
                    <th style="text-align: left" colspan="{data_cols}">Values</th> \
                        <th style="width: 96px; text-align: left">Unit</th> \
                            <th style="text-align: left">Description</th> \
                                </tr>'

    var_nr = 0
    for key in parameters:

        if var_nr % 2 == 0:
            bg_colour = 'white'
        else:
            bg_colour = '#f2f2f2'

        value_x = [i[0] for i in parameters[key]['value']]
        value_y = [i[1] for i in parameters[key]['value']]
        unit_x = parameters[key]['unit'][0]
        unit_y = parameters[key]['unit'][1]
        var_x = parameters[key]['independent']
        var_y = parameters[key]['dependent']
        info = parameters[key]['info']

        padding_cells = '<td></td>' * (data_cols - len(value_x))

        tmp_x = '</td><td style="text-align: left">'.join(f'{_:2.2f}' for _ in value_x)
        data_x = f'<td style="text-align: left">{tmp_x}</td>{padding_cells}'

        tmp_y = '</td><td style="text-align: left">'.join(f'{_:2.2f}' for _ in value_y)
        data_y = f'<td style="text-align: left">{tmp_y}</td>{padding_cells}'

        table += f' \
            <tr style="background-color:{bg_colour}; text-align: left;"> \
                <td rowspan="2" style="text-align: left">{key}</td> \
                    <td style="text-align: left">{var_x}</td> \
                        {data_x} \
                            <td style="text-align: left">{unit_x}</td> \
                                <td rowspan="2" style="text-align: left">{info}</td> \
                                    </tr>'
        
        table += f' \
            <tr style="background-color:{bg_colour}"> \
                <td style="text-align: left">{var_y}</td>\
                    {data_y} \
                        <td style="text-align: left">{unit_y}</td> \
                            </tr>'

        var_nr += 1

    table += '</table>'

    return table


def plot_parameters(data, pars):

    fig, axes = plt.subplots(nrows=1, ncols=len(pars), figsize=(len(pars)*4, 4))
    
    sub_plot = 0
    for par in pars:
        
        if len(pars) > 1:
            ax = axes[sub_plot]
        else:
            ax = axes
        
        par_data = data[par]
        
        xs = [i[0] for i in par_data['value']]
        ys = [i[1] for i in par_data['value']]

        x_label = par_data['independent']
        y_label = par_data['dependent']
        x_unit = par_data['unit'][0]
        y_unit = par_data['unit'][1]

        ax.plot(xs, ys)
        ax.set_xlabel(f'{x_label} ({x_unit})')
        ax.set_ylabel(f'{y_label} ({y_unit})')
        ax.set_title(par)
        
        sub_plot += 1
        
    plt.tight_layout()

def plot_body_weight_curves(data, pars):
    
    fig = plt.figure()
    ax = fig.gca()
    
    for par in pars:
        
        par_data = data[par]
        
        xs = [i[0] for i in par_data['value']]
        ys = [i[1] for i in par_data['value']]
        ax.plot(xs, ys, label=par)
    
    x_label = par_data['independent']
    y_label = par_data['dependent']
    x_unit = par_data['unit'][0]
    y_unit = par_data['unit'][1]
    
    ax.set_xlabel(f'{x_label} ({x_unit})')
    ax.set_ylabel(f'{y_label} ({y_unit})')
    
    ax.legend()
    
    plt.tight_layout()