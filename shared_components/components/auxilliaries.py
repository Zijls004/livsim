def interpolate(data, x):
    xs = [i[0] for i in data]
    ys = [i[1] for i in data]
    
    if not min(xs) <= x <= max(xs):
        raise ValueError('x-value used for interpolation outside data-range')
    
    check_xs = [i for i in xs]
    check_xs.sort()
    if xs != check_xs:
        raise ValueError('The x-values in data are not in ascending order.')
    
    
    x_up = [i for i in xs if i >= x][0]
    i = xs.index(x_up)
    
    slope = (ys[i] - ys[i-1]) / (xs[i] - xs[i-1])
    
    return ys[i-1] + (x - xs[i-1]) * slope