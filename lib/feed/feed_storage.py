from copy import deepcopy

from .feed import mix, Feed

# days per month
DPM = 30


class FeedStorage(object):
    """Stores information about allocation of feed from external sources and 
    stores and allocates feed produced on-farm.

    External sources of feed
    ------------------------
    For external sources of feed, both roughages and concentrates, the feeding
    regimes are determined at the start of the simulation. For each kind of 
    feed used in the simulation the user specifies the amount of that feed to
    give to each animal and in what life stage the animal should receive the
    specified amount of the given feed.

    On-farm produced feed
    ---------------------
    Upon storage of on-farm produced roughage the roughage is split into
    monthly rations. These monthly rations are individual feed-objects 
    initialized with the set of feed-parameters that are related to the crop 
    harvested. The amount of feed for each ration depends on the amount of
    roughage that is stored and the number of months until the next harvest.

    The information on on-farm produced feed is stored in a dictionary where 
    the 'key: value' pairs are 'feed_name: list_of_rations'.

    Example:
        'maize stover': [Obj, Obj, Obj, ...],
        'bean stover': [Obj, Obj, Obj, ...],
        ...

    The montly ration of roughage to each animal in the herd is derived by 
    selecting the amount for the current month for each type of roughage in 
    the storage and blending the feeds into a tasty roughage cocktail.
    """
    _name = 'FeedStorage'
    _prefix = 'FS'
    units = {}
    life_stages = ['default', 
                   'calf', 
                   'gestating', 
                   'lactating', 
                   'lactating and gestating',
                   'yearling']

    def __init__(self, library=None, farm=None):

        self._library = library

        self._farm = farm
        self._herd = None
        self._animal = None

        self.roughages = {}
        self.new_roughages = {}
        self.pasture_from_common_land = {}

        self.external_feed = {'roughage': {}, 'concentrate': {}}

        # Used to ration feed (months)
        self._season_nr = {}
        self._nmonths_rationing = 6
        self._storage_is_new = True

    @property
    def library(self):
        return self._library

    @property
    def _farm(self):
        return self._farm_

    @_farm.setter
    def _farm(self, farm):
        self._farm_ = farm

    @property
    def season_nr(self):
        return self._season_nr

    @property
    def _herd(self):
        """Returns a reference to the herd that is supplied with feed from
        this FeedStorage-object.

        :return: A reference to a herd-object.
        :rtype: Herd
        """
        if self._farm:
            return self._farm.herd
        else:
            return self._herd_

    @_herd.setter
    def _herd(self, value):
        self._herd_ = value

    @property
    def herd_size(self):
        """Returns the current size of the herd. This information is used to
        determine the ration of feed produced on-farm for each individual
        animal.

        :return: The number of animals in the herd.
        :rtype: int
        """
        if self._herd:
            return self._herd.nanimals

    @property
    def _animal(self):
        return self._animal_

    @_animal.setter
    def _animal(self, value):
        self._animal_ = value

    @property
    def _month_index(self):
        ''' The month of the year. 
        
            Simulation starts at month = 1. To index properly, therefore, we
            should index with (self._farm.month - 1).
        '''
        if self._farm:
            return self._farm.month - 1
        elif self._animal:
            return self._animal.months_in_simulation
        else:
            return 0

    def split_into_monthly_rations(self, feed, nmonths):
        ''' Splits feed into monthly rations (list) - equal amount per month.

            The rations are the amount of DM available of the specified feed
            type to the herd per month.
        '''

        total_amount = feed.amount
        ration_amount = total_amount / nmonths

        rations = []

        for i in range(nmonths):

            ration = feed.__copy__()
            ration.set_amount(ration_amount)
            rations.append(ration)

        return rations

    def create_empty_feed_dict(self, category: str):

        assert category in ('concentrate', 'roughage'), \
            "parameter category should be either 'concentrate' or 'roughage'"
        
        if category == 'concentrate':
            feed_types = ['concentrate']
        else:
            feed_types = ['pasture', 'forage']

        if self.library is not None:
            library = self.library
        else:
            library = Feed.library

        feed_dict = {}

        for feed_type in feed_types:
            
            feed_dict.update({
                feed_name: [0 for i in range(12)]
                for feed_name in library[feed_type]
            })
        
        return feed_dict

    def add_external_roughage(self, amount, feed, life_stage='default'):
        """Adds roughage to the feeding regime for the animals in the given 
        life stage.
        
        Care should be taken that the specified kind of external feed does
        exist in the feed library.

        The amount may either be given as a single value or as a list of 12
        values. In the first case the same amount of the specified feed type
        will be offered to animals in the specified life stage, in the latter
        case an amount of feed can be given for each month of the year.

        :param amount: amount of feed in kg DM per day to add
        :type amount: list, int, float
        :param feed: feed type e.g. 'maize stover'
        :type feed: str, optional
        :param life_stage: life stage of the animal for which the external feed
                           is defined, defaults to 'default'
        :type life_stage: str, optional
        :raises ValueError: raises an error if the specified life stage is not
                            recognized
        """
        if life_stage not in self.life_stages:
            raise ValueError(
                f"The defined life stage -{life_stage}- is not \n \
                recognized. Pick an option from the list: {[i for i in self.life_stages]}"
            )

        if life_stage not in self.external_feed['roughage']:
            d = self.external_feed['roughage'] 
            d[life_stage] = self.create_empty_feed_dict('roughage')

        try:
            d = self.external_feed['roughage'][life_stage]
            d[feed] = self.external_feed_addition(d[feed], amount) 
            
        except KeyError:
            print(f'Error setting feed {feed} for life stage {life_stage}')

    def add_external_concentrate(self, amount, kind=None,
                                 life_stage='default'):
        """ Adds concentrate to the feeding regime for the animals in the given
        life stage.

        see also: add_external_roughage().

        :param amount: amount of feed in kg DM per day to add
        :type amount: list, int, float
        :param feed: feed type e.g. 'maize stover', defaults to None
        :type feed: str, optional
        :param life_stage: life stage of the animal for which the external feed
                           is defined, defaults to 'default'
        :type life_stage: str, optional
        :raises ValueError: raises an error if the specified life stage is not
                            recognized
        """

        if life_stage not in self.life_stages:
            raise ValueError(
                f"The defined life stage -{life_stage}- is not \n \
                recognized. Pick an option from the list: {[i for i in self.life_stages]}"
            )

        if life_stage not in self.external_feed['concentrate']:
            self.external_feed['concentrate'][
                life_stage] = self.create_empty_feed_dict('concentrate')

        try:
            d = self.external_feed['concentrate'][life_stage] 
            d[kind] = self.external_feed_addition(d[kind], amount)

        except KeyError:
            print(f'Error setting feed {kind} for life stage {life_stage}')

    @staticmethod
    def external_feed_addition(current, amount):
        if isinstance(amount, list):
            try:
                assert len(amount) == 12
            except AssertionError:
                raise AssertionError(
                    "External feed should be defined for all months.")

            return [current[i] + amount[i] for i in range(len(current))]

        elif isinstance(amount, (int, float)):
            return [current[i] + amount for i in range(len(current))]

        else:
            print(f'Type of amount is not recognized: {type(amount)}')

    def switch_seasons(self):
        """Switch from the old rations of feed to the newly harvested ones.
        
        In the sequence of calculations in FARMSIM the crops are harvested, and
        the feed storage updated, before LIVSIM runs. To make sure that the 
        proper rations of crop residues are used as cattle feed, therefore, the
        new crop residues are first stored in a different dictionary, before
        being 'promoted' to the dictionary from which the rations of cattle 
        feed are extracted.
        """
        self.roughages = deepcopy(self.new_roughages)
        self.new_roughages = {}

    def add_crop_residues(self, amount, kind=None, nmonths_rationing=None):
        ''' Add crop residues to the storage.

            Args:
                kind: feed type e.g. 'soy stover' or 'napier grass'
                        - key in the feed library
                amount: amount in kg DM
                nmonths_rationing : number of months to ration this feed.

            Here the feed (crop residues) is divided into portions and stored
            in a list which is popped each month to get the monthly ration for
            the entire herd.
        '''
        if kind in self.new_roughages:

            if self.season_nr[kind] != self._farm.season_nr:

                self.new_roughages[kind] = self.create_rations(kind=kind, 
                                                               category='roughage', 
                                                               amount=amount, 
                                                               nmonths_rationing=nmonths_rationing)

                self._season_nr[kind] += 1
            else:
                per_month = amount / nmonths_rationing

                for ration in self.new_roughages[kind]:
                    ration.set_amount(ration.amount + per_month)

        else:
            self.new_roughages[kind] = self.create_rations(kind=kind, 
                                                           category='roughage', 
                                                           amount=amount, 
                                                           nmonths_rationing=nmonths_rationing)

            if self._farm is not None:
                self._season_nr[kind] = self._farm.season_nr - 1
            else:
                self._season_nr[kind] = -1

    def create_rations(self, kind, category, amount, nmonths_rationing=None):

        feed = Feed(library=self.library, 
                    parameters=kind, 
                    category=category, 
                    amount=amount, 
                    crop_residues=True)

        if nmonths_rationing is None:
            nmonths_rationing = self._nmonths_rationing

        return self.split_into_monthly_rations(feed, nmonths_rationing)

    def roughage_to_give(self, animal):
        ''' Returns the roughage to give to an animal.

            The amounts and kinds of roughages an animal receives may depend
            on the life stage of the animal (see: get_life_stage_of_animal).
        '''
        if animal.dead:
            return None

        on_farm = self.roughage_from_on_farm_to_give(animal)
        off_farm = self.roughage_from_off_farm_to_give(animal)

        sources = (on_farm, off_farm)

        both_sources_empty = all(f is None for f in sources)
        from_both_sources = all(f is not None for f in sources)

        if both_sources_empty:
            return None

        elif from_both_sources:
            animal.fraction_external_roughage = off_farm.amount / \
                (off_farm.amount + on_farm.amount)

        elif off_farm is not None:
            animal.fraction_external_roughage = 1

        else:
            animal.fraction_external_roughage = 0

        to_give = [on_farm, off_farm]

        return mix(to_give, animal)

    def concentrate_to_give(self, animal):
        ''' Returns the concentrate to give to an animal.

            The amounts and kinds of concentrates an animal receives may depend
            on the life stage of the animal (see: get_life_stage_of_animal).
        '''
        if animal.dead:
            return None

        off_farm = self.concentrate_from_off_farm_to_give(animal)

        if not off_farm:
            return None
        else:
            return off_farm

    def get_life_stage_of_animal(self, animal, feed_type):
        """Inspect the current animal to see which feeding strategy to use for
        this animal.

        Currently the following life stages are distinguished:
        - 'calf'                   : animals that have not yet reached the
                                     weaning age
        - 'gestating'              : animals that are in gestation
        - 'lactating'              : animals that are in lactation
        - 'lactating and gestating': animals that are simulataneously giving
                                     milk and have a bun in the oven
        - 'yearling'               : animals that are post-weaning but younger
                                     than 24 months
        - 'default'                : any animal that does not fit into any of
                                     the specific categories specified will be
                                     given the default feed

        The user is free so specify any of these categories, or no specific
        feeding management at all. Only the 'default'-scenario should always
        be specified.

        :param animal: Reference to the animal that needs feeding.
        :type animal: Cow or Bull
        :param feed_type: The feed is either 'roughage' or 'concentrate'.
        :type feed_type: str
        :raises ValueError: In the simplified reality of LIVSIM everyone is 
                            either male or female.
        :return: The life stage the animal is currently in.
        :rtype: str
        """
        if animal.age < animal.weaning_age:
            return 'calf'

        if animal._sex == 'f':

            if animal.is_lactating and animal.is_gestating:
                return self.check_lactating_and_gestating(
                    'lactating and gestating', feed_type)

            elif animal.is_gestating:
                return 'gestating'

            elif animal.is_lactating:
                return 'lactating'

            elif 12 <= animal.age <= 24:
                return 'yearling'

            else:
                return 'default'

        elif animal._sex == 'm':
            if 12 <= animal.age <= 24:
                return 'yearling'
            else:
                return 'default'

        else:
            raise ValueError(f'Sex: {animal._sex} is not known.')

    def check_lactating_and_gestating(self, life_stage, feed_type):
        """For animals that are both in lactation and gestation the model
        checks whether a feeding strategy has been defined for that particular
        case. If no such strategy is available, the model looks for feeding
        strategies for lactating and gestating animals in that order. If no
        feeding management for productive animals can be found, the default
        feeding will be used.

        :param life_stage: [description]
        :type life_stage: str
        :param feed_type: [description]
        :type feed_type: str
        :return: [description]
        :rtype: str
        """
        if life_stage in self.external_feed[feed_type]:
            return life_stage

        elif 'lactating' in self.external_feed[feed_type]:
            return 'lactating'

        elif 'gestating' in self.external_feed[feed_type]:
            return 'gestating'

        else:
            return 'default'

    def get_external_concentrate_for_animal(self, animal):
        """Returns the feed ration for this animal based on its current life
        stage.

        For life stages, see also self.get_life_stage_of_animal().

        :param animal: [description]
        :type animal: [type]
        :raises KeyError: [description]
        :return: [description]
        :rtype: dict
        """
        life_stage = self.get_life_stage_of_animal(animal, 'concentrate')

        feed_dict = self.external_feed['concentrate']
        feed_dict_is_empty = not feed_dict.items()

        if feed_dict_is_empty:
            return {}
        else:
            if life_stage in self.external_feed['concentrate']:
                return self.external_feed['concentrate'][life_stage]
            else:
                try:
                    return self.external_feed['concentrate']['default']
                except KeyError:
                    return {}

    def get_external_roughage_for_animal(self, animal):
        ''' Returns the feed ration for this animal based on its current life
            stage.

            For life stages, see also self.get_life_stage_of_animal().
        '''
        life_stage = self.get_life_stage_of_animal(animal, 'roughage')

        if life_stage in self.external_feed['roughage']:
            return self.external_feed['roughage'][life_stage]
        else:
            return self.external_feed['roughage']['default']

    def roughage_from_on_farm_to_give(self, animal):
        """The feed, from on farm production, to give to each animal in the
        herd.

        NOTE: here we for the moment do not inspect the animal to see what
        to give. Each animal gets the same ration of roughage from on farm
        production.

        :param animal: [description]
        :type animal: [type]
        :return: [description]
        :rtype: [type]
        """
        if self._storage_is_new:
            self.switch_seasons()
            self._storage_is_new = False
        elif not all(self.roughages.values()):
            self.switch_seasons()
        
        to_give = []

        for _, monthly_rations in self.roughages.items():

            if monthly_rations:
                feed = monthly_rations[-1].__copy__()
                feed.amount = feed.amount / self.herd_size
                to_give.append(feed)

        if to_give:
            res = mix(to_give, animal)
            return res
        else:
            return None

    def concentrate_from_off_farm_to_give(self, animal):
        ''' Returns the concentrate to give to an animal.

            The amounts and kinds of concentrates an animal receives may depend
            on the life stage of the animal (see: get_life_stage_of_animal).
        '''
        external_feed = self.get_external_concentrate_for_animal(animal)
        external_feed_is_empty = not external_feed.items()

        month = self._month_index

        to_give = []

        if not external_feed_is_empty:
            feeds = {k: v[month % 12] for k, v in external_feed.items() if v[month % 12] > 0}
        
            for name, amount in feeds.items():
                feed = Feed(library=self.library,
                            parameters=name,
                            category='concentrate',
                            amount=DPM * amount,
                            animal=animal)

                to_give.append(feed)

        if to_give:
            return mix(to_give, animal)
        else:
            return None

    def roughage_from_off_farm_to_give(self, animal):
        ''' Returns the roughage to give to an animal.

            The amounts and kinds of roughages an animal receives may depend
            on the life stage of the animal (see: get_life_stage_of_animal).
        '''
        external_feed = self.get_external_roughage_for_animal(animal)

        month = self._month_index

        to_give = []

        feeds = {k: v[month % 12] for k, v in external_feed.items() if v[month % 12] > 0}

        for name, amount in feeds.items():
            feed = Feed(library=self.library,
                        parameters=name,
                        category='roughage',
                        amount=DPM * amount,
                        animal=animal)

            to_give.append(feed)

        if to_give:
            return mix(to_give, animal)
        else:
            return None

    def remove_roughage_rations(self):
        ''' Removes (pops) this months roughage rations, out of the storage.
        '''
        for kind in self.roughages:
            try:
                del self.roughages[kind][-1]
            except IndexError:
                continue

    @property
    def total_roughage(self):
        """The total amount of roughage in storage (kg).

        :return: The total amount of roughage left.
        :rtype: float
        """
        total = 0
        for _, monthly_rations in self.roughages.items():
            if monthly_rations:
                total += sum([x.amount for x in monthly_rations])

        return total

    def update(self):
        ''' Updates the state of storage - removes this months rations. 
        '''
        self.remove_roughage_rations()

    def summary(self):
        ''' A summary (string) of the state of storage.
        '''
        fs = 'Roughage: {:>6.0f} kg'
        return fs.format(self.total_roughage)

    def __repr__(self):
        ''' The python representation of the storage.
        '''
        lines = []

        for key in self.roughages:
            val = sum([x.amount for x in self.roughages[key]])
            line = '{:<20} {:>6.1f} kg (DM)'.format(key + ':', val)
            lines.append(line)

        return '\n'.join(lines)
