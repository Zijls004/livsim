
import sys

from file_paths import nuances_folder

sys.path.append(nuances_folder)

from matplotlib import pyplot as plt

from livsim import Herd
from livsim import Cow, Bull
from livsim import Feed, FeedStorage

# let us make our feed storage model-element
feed_storage = FeedStorage()

# Feed sources from off-farm: here an amount of grass to feed to the animals is specified for each month of the year.
external_grass = [15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4]
feed_storage.add_external_roughage(external_grass, 'Heteropogon contortus', 'default')
feed_storage.add_external_roughage(0, 'maize stover', 'default')
feed_storage.add_external_roughage(external_grass, 'Heteropogon contortus', 'gestating')

# Some additional concentrates to keep our cows from dying on the diet of only grass...
feed_storage.add_external_concentrate(2, 'concentrate 1', 'default')
feed_storage.add_external_concentrate(2, 'concentrate 1', 'gestating')

# Start with two cows; aged 48 months and 6 months.
cow_1 = Cow(age=48, breed='Friesian x Holstein')
cow_2 = Cow(age=6, breed='Friesian x Holstein')
cow_1._main_variables = None
cow_2._main_variables = None

# The two cows we specified above form our initial herd.
initial_cattle = [cow_1, cow_2]
herd = Herd(animals=initial_cattle)

# Set the maximum number of cows in our herd
herd.parameters['max_nfemales']['value'] = 2
herd.parameters['max_nmales']['value'] = 0
herd.parameters['female_threshold_open']['value'] = 36       
herd.parameters['female_threshold_age']['value'] = 96        
herd.parameters['female_threshold_lactations']['value'] = 5

# Now let us link the feed supply to the herd; this established a two-way connection between the herd and the feed supply.
herd.feed_supply = feed_storage

res, df = herd.run(months=240)
df.to_csv('test_run.csv')