"""The LIVSIM package.

Allows one to model cattle using the provided Herd, Cow, Bull, Feed and Feed
storage classes.
"""
import sys

from .animal import Cow, Bull
from .animal import RequirementsCowAFRC 
from .animal import RequirementsBullAFRC

from .herd import Herd

from .feed import Feed
from .feed import FeedStorage
from .feed import mix

# These functions should only be imported if the code is not run from a .exe.
if not getattr(sys, 'frozen', False):
    from .helper_functions import html_single_value_parameters
    from .helper_functions import html_multi_value_parameters
    from .helper_functions import flatten_parameter_dict
    from .helper_functions import plot_parameters
    from .helper_functions import plot_body_weight_curves
