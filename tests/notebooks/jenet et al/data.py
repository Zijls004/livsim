data_cows = {
    'Boran': {
        'cycle 1': {
            'low': {
                'dm_intake': 4.73,
                'om_intake': 4.36,
                'bw': 318,
                'omd': 0.623
            },
            'medium': {
                'dm_intake': 5.66,
                'om_intake': 5.23,
                'bw': 322,
                'omd': 0.647
            },
            'high': {
                'dm_intake': 7.08,
                'om_intake': 6.52,
                'bw': 374,
                'omd': 0.639
            }
        },
        'cycle 2': {
            'low': {
                'dm_intake': 4.86,
                'om_intake': 4.44,
                'bw': 330,
                'omd': 0.623
            },
            'medium': {
                'dm_intake': 5.80,
                'om_intake': 5.29,
                'bw': 328,
                'omd': 0.647
            },
            'high': {
                'dm_intake': 7.48,
                'om_intake': 6.85,
                'bw': 382,
                'omd': 0.639
            }
        }      

    },
    'Boran x Friesian': {
        'cycle 1': {
            'low': {
                'dm_intake': 4.87,
                'om_intake': 4.48,
                'bw': 336,
                'omd': 0.616
            },
            'medium': {
                'dm_intake': 6.04,
                'om_intake': 5.58,
                'bw': 347,
                'omd': 0.615
            },
            'high': {
                'dm_intake': 7.88,
                'om_intake': 7.29,
                'bw': 410,
                'omd': 0.546
            }
        },
        'cycle 2': {
            'low': {
                'dm_intake': 5.10,
                'om_intake': 4.65,
                'bw': 340,
                'omd': 0.616
            },
            'medium': {
                'dm_intake': 5.90,
                'om_intake': 5.39,
                'bw': 339,
                'omd': 0.615
            },
            'high': {
                'dm_intake': 7.82,
                'om_intake': 7.15,
                'bw': 400,
                'omd': 0.615
            }
        }      

    }
}

roughage_ME = 9.6
concentrate_ME = 11

fraction_roughage = 0.65
fraction_concentrate = 0.35

average_ME = fraction_roughage * roughage_ME + fraction_concentrate * concentrate_ME

res = {}
for breed in data_cows:
    
    breed_data = data_cows[breed]
    res[breed] = {}
    
    for cycle in breed_data:
        
        cycle_data = breed_data[cycle]
        res[breed][cycle] = {}
        
        for feeding_level in cycle_data:
            
            if feeding_level == 'low':
                k = 1
            elif feeding_level == 'medium':
                k = 1.2
            elif feeding_level == 'high':
                k = 1.4
            else:
                raise ValueError(f'Unknow feeding level: {feeding_level}')
            
            feeding_data = cycle_data[feeding_level]
            res[breed][cycle][feeding_level] = {}
            
            bw = feeding_data['bw']
            intake = feeding_data['intake']
            omd = feeding_data['omd']
            
            requirement = 8.3 + 0.091 * (bw / 1.08)
            
            a_priori = requirement * k
            a_posteriori = omd * intake * average_ME
            
            res[breed][cycle][feeding_level]['a_priori'] = a_priori / requirement
            res[breed][cycle][feeding_level]['a_posteriori'] = a_posteriori / requirement
                        

def my_function(a):
    return a**2

b = 3
c = my_function(a=b)
